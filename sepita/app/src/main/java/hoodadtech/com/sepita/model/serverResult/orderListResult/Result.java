package hoodadtech.com.sepita.model.serverResult.orderListResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("serviceTitle")
    @Expose
    private String serviceTitle;
    @SerializedName("orderCode")
    @Expose
    private String orderCode;
    @SerializedName("address")
    @Expose
    private Object address;
    @SerializedName("statusTitle")
    @Expose
    private String statusTitle;
    @SerializedName("statusCode")
    @Expose
    private String statusCode;
    @SerializedName("statusColor")
    @Expose
    private String statusColor;
    @SerializedName("orderDate")
    @Expose
    private String orderDate;
    @SerializedName("phone")
    @Expose
    private Object phone;
    @SerializedName("employeeId")
    @Expose
    private String employeeId;
    @SerializedName("visitDateTime")
    @Expose
    private String visitDateTime;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("orderPaymentType")
    @Expose
    private String orderPaymentType;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("cityArea")
    @Expose
    private String cityArea;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(String statusColor) {
        this.statusColor = statusColor;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getVisitDateTime() {
        return visitDateTime;
    }

    public void setVisitDateTime(String visitDateTime) {
        this.visitDateTime = visitDateTime;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getOrderPaymentType() {
        return orderPaymentType;
    }

    public void setOrderPaymentType(String orderPaymentType) {
        this.orderPaymentType = orderPaymentType;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCityArea() {
        return cityArea;
    }

    public void setCityArea(String cityArea) {
        this.cityArea = cityArea;
    }

}
