package hoodadtech.com.sepita.view.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.view.fragment.worker.FrgStaredWorkers;

public class StaredWorkersActivity extends BaseActivity {

    Toolbar toolbar;
    TextView toolBar_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stared_workers);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //toolBar_title = findViewById(R.id.toolbar_text);
        //toolBar_title.setText(getString(R.string.workers));
        FragmentHandler fragmentHandler=new FragmentHandler(this,getSupportFragmentManager());
        if(getIntent().getExtras()==null){
            fragmentHandler.loadFragment(new FrgStaredWorkers(),false);
        }else if(getIntent().getExtras()!=null&&!getIntent().getExtras().getBoolean("fromNav",true)){
            Bundle bundle=new Bundle();
            bundle.putString("id",getIntent().getExtras().getString("id"));
        }
    }

}
