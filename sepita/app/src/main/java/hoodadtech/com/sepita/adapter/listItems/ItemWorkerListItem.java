package hoodadtech.com.sepita.adapter.listItems;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.viewHolders.ItemCommentEmplyoeeHolder;
import hoodadtech.com.sepita.adapter.viewHolders.ItemWorkerListItemHolder;
import hoodadtech.com.sepita.model.Reason;
import hoodadtech.com.sepita.model.serverResult.EmployeeCommentItem;
import hoodadtech.com.sepita.model.serverResult.Worker;
import hoodadtech.com.sepita.utils.StringUtility;

/**
 * Created by Milad on 1/30/2018.
 */

public class ItemWorkerListItem implements MultiAdapterItem {
    ItemWorkerListItemHolder itemHolder;
    Worker item;

    @Override
    public int getTypeId() {
        return 8;
    }

    @Override
    public void setup(RecyclerView.ViewHolder holder){
        itemHolder = (ItemWorkerListItemHolder) holder;
        if (itemHolder != null) {
            itemHolder.TV_assistanName.setText(item.getFullName());
            itemHolder.TV_assistanScore.setText(item.getAvrageScore());
            itemHolder.layout_favorites.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(itemClickListener!=null){
                        itemClickListener.onClick(item,view);
                    }
                }
            });

            itemHolder.layout_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(itemClickListener!=null){
                        itemClickListener.onClick(item,view);
                    }
                }
            });


        }

    }

    public void setItem(Worker item) {
        this.item = item;
    }

    @Override
    public Worker getItem() {
        return item;
    }


    ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onClick(Worker item, View view);

    }


}
