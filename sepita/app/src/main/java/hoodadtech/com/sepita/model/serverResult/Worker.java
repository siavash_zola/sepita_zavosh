
package hoodadtech.com.sepita.model.serverResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Worker {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("avrageScore")
    @Expose
    private String avrageScore;
    @SerializedName("imageUrl")
    @Expose
    private Object imageUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAvrageScore() {
        return avrageScore;
    }

    public void setAvrageScore(String avrageScore) {
        this.avrageScore = avrageScore;
    }

    public Object getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Object imageUrl) {
        this.imageUrl = imageUrl;
    }

}
