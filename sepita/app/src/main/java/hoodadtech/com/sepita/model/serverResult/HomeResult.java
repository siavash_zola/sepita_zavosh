
package hoodadtech.com.sepita.model.serverResult;

import java.util.List;

import hoodadtech.com.sepita.model.HomeItem;


public class HomeResult {


    private List<HomeItem> result = null;

    public List<HomeItem> getResult() {
        return result;
    }

    public void setResult(List<HomeItem> result) {
        this.result = result;
    }

}
