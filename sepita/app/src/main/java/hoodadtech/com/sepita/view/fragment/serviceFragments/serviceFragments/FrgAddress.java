package hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.newAdapters.AddressAdapter;
import hoodadtech.com.sepita.model.AddressNew;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.DeletResult.DeleteResult;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.addAddress.AddAddressResult;
import hoodadtech.com.sepita.model.serverResult.getAddress.GetAddressResult;
import hoodadtech.com.sepita.model.serverResult.getAddress.Result;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.AddressManager;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.IransansEditText;
import hoodadtech.com.sepita.utils.OnItemLatLonClickListener;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import hoodadtech.com.sepita.view.activity.BaseActivity;
import hoodadtech.com.sepita.view.activity.HistoryActivity;
import hoodadtech.com.sepita.view.activity.MapsActivity;
import hoodadtech.com.sepita.view.fragment.entrancefragments.FrgLogin;

public class FrgAddress extends Fragment implements View.OnClickListener ,OnItemLatLonClickListener ,AddressManager {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private RelativeLayout relativeLayout_box;
    LinearLayout mainLayout,addNewAddress,btn_submit;
    EditText ET_address, ETPelak, Et_vahed, Et_phoneNumber, Et_details;
    PostOrderObject postOrderObject;
    FileUtility fileUtility;
    ImageView iv_level;



    private RecyclerView recyclerView_address;
    private List<AddressNew> list;
    private AddressAdapter adapter;

    public static AddressNew addressSelected = null;
    private Server server;
    private AlertDialog show;

    public FrgAddress() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        addressSelected = null;
        for (int i = 0; i < list.size(); i++) {
            AddressNew addressNew = list.get(i);
            if (addressNew.isSelected()){
                addressSelected = addressNew;
                break;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_address, container, false);
        fileUtility = new FileUtility(getContext());
        postOrderObject = fileUtility.getPostOrderObject();
        //TextView title = getActivity().findViewById(R.id.toolbar_text);
        //title.setText(getResources().getString(R.string.address));
        server=Server.getInstance(getContext());
        initView(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadList(server);
    }

    private void initView(View view) {
        relativeLayout_box = view.findViewById(R.id.RelativeLayout_box);
        btn_submit = view.findViewById(R.id.btn_submit);
        ET_address = view.findViewById(R.id.input_name);
        addNewAddress = view.findViewById(R.id.addAddress);
        btn_submit.setOnClickListener(this);
        addNewAddress.setOnClickListener(this);
        relativeLayout_box.setOnClickListener(this);

        recyclerView_address = view.findViewById(R.id.recyclerView_address);
        list = new ArrayList<>();

        adapter = new AddressAdapter(list,getActivity());
        adapter.addressManager = this;
        adapter.listener = this;
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView_address.setLayoutManager(layoutManager);
        recyclerView_address.setAdapter(adapter);

        iv_level = view.findViewById(R.id.iv_level);
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey("serviceCode")){
            iv_level.setImageResource(R.drawable.level_two);
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                hideKeyboard();


                if (addressSelected != null){
                    postOrderObject.getAddress().setAddressDescription(addressSelected.getAddress());
                    postOrderObject.getAddress().setPhone(addressSelected.getPhone());
                    postOrderObject.getAddress().setUnit(addressSelected.getUnit());
                    postOrderObject.getAddress().setNumber(addressSelected.getPlaque());
                    //------- ezafe shod----
                    postOrderObject.getAddress().setCity(addressSelected.getCity());
                    postOrderObject.getAddress().setNeighborhood(addressSelected.getNeighborhood());


                    fileUtility.writePostOrderObject(postOrderObject);
                    FragmentHandler fragmentHandler = new FragmentHandler(getActivity(), getFragmentManager());
                    FrgPayment frgPayment = new FrgPayment();
                    Bundle arguments = getArguments();
                    if (arguments != null && arguments.containsKey("serviceCode")){
                        Bundle bundle = new Bundle();
                        bundle.putString("serviceCode",arguments.getString("serviceCode"));
                        frgPayment.setArguments(bundle);
                    }
                    fragmentHandler.loadFragment(frgPayment, true);
                }else {
                    Toast.makeText(getContext(),getActivity().getString(R.string.inputAddressPlz) ,Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.mainLayout:
                hideKeyboard();
                break;
            case R.id.addAddress:
                createDialog();
                break;
            case R.id.RelativeLayout_box:
                createDialog();
                break;
        }

    }

    private void createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_add_address, null);
        builder.setView(rootView);
        show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        IransansEditText titleEditText = rootView.findViewById(R.id.stateEditText);
        IransansEditText neighborhoodEditText = rootView.findViewById(R.id.neighborhoodEditText);
        IransansEditText plaqueEditText = rootView.findViewById(R.id.plaqueEditText);
        IransansEditText unitEditText = rootView.findViewById(R.id.unitEditText);
        IransansEditText addressEditText = rootView.findViewById(R.id.addressEditText);
        IransansEditText phoneEditText = rootView.findViewById(R.id.phoneEditText);
        LinearLayout addAddress = rootView.findViewById(R.id.addAddress);

        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                if (titleEditText.getText().length()>0 &&
                        neighborhoodEditText.getText().length()>0 &&
                        plaqueEditText.getText().length()>0 &&
                        unitEditText.getText().length()>0 &&
                        addressEditText.getText().length()>0 &&
                        phoneEditText.getText().length()>0){


                    if (StringUtility.isValidPhoneNumber(phoneEditText.getText().toString())) {
                        AddressNew addressNew = new AddressNew();
                        addressNew.setCity("تهران");
                        addressNew.setTitle(titleEditText.getMyText());
                        addressNew.setNeighborhood(neighborhoodEditText.getMyText());
                        addressNew.setPlaque(plaqueEditText.getMyText());
                        addressNew.setUnit(unitEditText.getMyText());
                        addressNew.setAddress(addressEditText.getMyText());
                        addressNew.setPhone(phoneEditText.getMyText());
                        //Log.i("kk",MapsActivity.selectedLatLong.latitude+"");
                        //Log.i("kk",MapsActivity.selectedLatLong.longitude+"");
                        addressNew.setLat(MapsActivity.selectedLatLong.latitude +"");
                        addressNew.setLon(MapsActivity.selectedLatLong.longitude +"");
                        addressNew.setSelected(false);
                        //Log.i("kk","address ok shod");
                        //send to server---------------


                        addAddressToServer(server,addressNew);


                    }else {
                        Toast.makeText(getContext(), "شماره وارد شده معتبر نمی باشد", Toast.LENGTH_SHORT).show();
                    }

                }else {
                    Toast.makeText(getContext(),getActivity().getString(R.string.inputDataPlz) ,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void addAddressToServer(Server server,AddressNew addressNew) {
        show.setCancelable(false);
        server.addAddressToServer(addressNew, new FutureCallback<Response<AddAddressResult>>() {
            @Override
            public void onCompleted(Exception e, Response<AddAddressResult> result) {
                show.setCancelable(true);
                if (result != null) {
                    if (result.getHeaders().code() == 401) {
                        getToken(2, addressNew);
                    } else if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().getStatusCode() == 16) {
                            getToken(2, addressNew);
                        } else if (result.getResult().getStatus().getIsSuccess()) {
                            list.clear();
                            adapter.notifyDataSetChanged();
                            loadList(server);
                            show.dismiss();
                        } else {
                            Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.plzCheckNetwork), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    void hideKeyboard(){
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    @Override
    public void itemLatLonClicked(int position) {
        for (int i = 0; i < list.size(); i++) {
            AddressNew addressNew = list.get(i);
            if (i == position){
                addressNew.setSelected(true);
                addressSelected = addressNew;

            }else {
                addressNew.setSelected(false);
            }
        }

        adapter.notifyDataSetChanged();
    }

    public void loadList( Server server){
        server.getAllAddress(new FutureCallback<Response<GetAddressResult>>() {
            @Override
            public void onCompleted(Exception e, Response<GetAddressResult> result) {
                if (result != null) {
                    if (result.getHeaders().code() == 401) {
                        getToken(1, null);
                    } else if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().getStatusCode() == 16) {
                            getToken(1, null);
                        } else if (result.getResult().getStatus().getIsSuccess()) {
                            List<Result> serverList = result.getResult().getResult();
                            list.clear();
                            for (int i = 0; i < result.getResult().getResult().size(); i++) {
                                Result result1 = serverList.get(i);
                                AddressNew addressNew = new AddressNew();
                                addressNew.setPhone(result1.getPhone());
                                addressNew.setNeighborhood(result1.getCityArea());
                                addressNew.setCity(result1.getCity());
                                addressNew.setUnit(result1.getUnitNumber());
                                addressNew.setPlaque(result1.getNumber());
                                addressNew.setAddress(result1.getBody());
                                addressNew.setId(result1.getId());
                                addressNew.setTitle(result1.getTitle());
                                //addressNew.setSelected(true);
                                if (MapsActivity.selectedId != null) {
                                    if (MapsActivity.selectedId.equals(result1.getId())) {
                                        addressNew.setSelected(true);
                                        addressSelected = addressNew;
                                    } else {
                                        addressNew.setSelected(false);
                                    }
                                } else {
                                    addressNew.setSelected(false);
                                }
                                list.add(addressNew);
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.plzCheckNetwork), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void getToken(int b,AddressNew addressNew){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getActivity()), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(getActivity(), result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(getActivity(), sharedPreference.getLoginTokenValue(getActivity()));
                        if (b == 1) {
                            loadList(server1);
                        }else if (b == 2){
                            addAddressToServer(server1,addressNew);
                        }else if (b == 3){
                            deleteAddress(server1,addressNew);
                        }else  if (b == 4){
                            editAddress(server1,addressNew);
                        }

                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //isSendRequest = false;
                }
            }
        });
    }

    @Override
    public void delete(AddressNew addressNew) {
        showDialogForDelete(addressNew);
    }

    @Override
    public void edit(AddressNew addressNew) {
        showDialogForEdit(addressNew);
    }

    private void showDialogForEdit(AddressNew addressNew) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_add_address, null);
        builder.setView(rootView);
        show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        IransansEditText titleEditText = rootView.findViewById(R.id.stateEditText);
        IransansEditText neighborhoodEditText = rootView.findViewById(R.id.neighborhoodEditText);
        IransansEditText plaqueEditText = rootView.findViewById(R.id.plaqueEditText);
        IransansEditText unitEditText = rootView.findViewById(R.id.unitEditText);
        IransansEditText addressEditText = rootView.findViewById(R.id.addressEditText);
        IransansEditText phoneEditText = rootView.findViewById(R.id.phoneEditText);
        LinearLayout addAddress = rootView.findViewById(R.id.addAddress);


        titleEditText.setText(addressNew.getTitle());
        neighborhoodEditText.setText(addressNew.getNeighborhood());
        plaqueEditText.setText(addressNew.getPlaque());
        unitEditText.setText(addressNew.getUnit());
        addressEditText.setText(addressNew.getAddress());
        phoneEditText.setText(addressNew.getPhone());

        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();

                if (titleEditText.getText().length()>0 &&
                        neighborhoodEditText.getText().length()>0 &&
                        plaqueEditText.getText().length()>0 &&
                        unitEditText.getText().length()>0 &&
                        addressEditText.getText().length()>0 &&
                        phoneEditText.getText().length()>0){


                    if (StringUtility.isValidPhoneNumber(phoneEditText.getText().toString())) {
                        AddressNew address = new AddressNew();
                        address.setCity("تهران");
                        address.setTitle(titleEditText.getMyText());
                        address.setNeighborhood(neighborhoodEditText.getMyText());
                        address.setPlaque(plaqueEditText.getMyText());
                        address.setUnit(unitEditText.getMyText());
                        address.setAddress(addressEditText.getMyText());
                        address.setPhone(phoneEditText.getMyText());
                        address.setSelected(false);
                        address.setId(addressNew.getId());

                        //send to server---------------
                        editAddress(server,address);


                    }else {
                        Toast.makeText(getContext(), "شماره وارد شده معتبر نمی باشد", Toast.LENGTH_SHORT).show();
                    }

                }else {
                    Toast.makeText(getContext(),getActivity().getString(R.string.inputDataPlz) ,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showDialogForDelete(AddressNew addressNew) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_confirmation, null);
        builder.setView(rootView);
        builder.create();
        AlertDialog show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        IransansButton yesBtn = rootView.findViewById(R.id.yesBtn);
        IranSansTextView textView = rootView.findViewById(R.id.text);
        IransansButton noBtn = rootView.findViewById(R.id.noBtn);
        textView.setText(R.string.doUWantToDeleteAddress);

        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteAddress(server,addressNew);
            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
    }

    private void deleteAddress(Server server,AddressNew addressNew) {
        server.deleteAddress(addressNew.getId(), new FutureCallback<Response<DeleteResult>>() {
            @Override
            public void onCompleted(Exception e, Response<DeleteResult> result) {
                if (result != null){
                    if (result.getHeaders().code() == 401){
                        getToken(3,addressNew);
                    }else if (result.getHeaders().code() == 200){
                        if (result.getResult().getStatus().getStatusCode() == 16){
                            getToken(3,addressNew);
                        }else if (result.getResult().getStatus().getIsSuccess()){
                            list.clear();
                            loadList(server);
                            Toast.makeText(getActivity(),result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getActivity(),result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(getActivity(),getString(R.string.error),Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(),getString(R.string.checkConnection),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void editAddress(Server server, AddressNew addressNew) {
        server.updateAddress(addressNew, new FutureCallback<Response<DeleteResult>>() {
            @Override
            public void onCompleted(Exception e, Response<DeleteResult> result) {
                if (result != null){
                    if (result.getHeaders().code() == 401){
                        getToken(4,addressNew);
                    }else if (result.getHeaders().code() == 200){
                        if (result.getResult().getStatus().getStatusCode() == 16){
                            getToken(4,addressNew);
                        }else if (result.getResult().getStatus().getIsSuccess()){
                            list.clear();
                            loadList(server);
                            show.dismiss();
                            Toast.makeText(getActivity(),result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getActivity(),result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(getActivity(),getString(R.string.error),Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(),getString(R.string.checkConnection),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
