package hoodadtech.com.sepita.model.serverResult.GetPrice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("duration")
    @Expose
    private String duration;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
