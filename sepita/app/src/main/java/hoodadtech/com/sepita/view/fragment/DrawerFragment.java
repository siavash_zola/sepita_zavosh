package hoodadtech.com.sepita.view.fragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;


import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.BaseInflaterAdapter;
import hoodadtech.com.sepita.adapter.DrawerItemInflater;
import hoodadtech.com.sepita.model.DrawerItem;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.menuInfoResult.MenuInfoResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.PublicMethod;
import hoodadtech.com.sepita.utils.SeterInfo;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import hoodadtech.com.sepita.view.activity.AboutUsActivity;
import hoodadtech.com.sepita.view.activity.BaseActivity;
import hoodadtech.com.sepita.view.activity.DetailsActivity;
import hoodadtech.com.sepita.view.activity.HistoryActivity;
import hoodadtech.com.sepita.view.activity.HomeActivity;
import hoodadtech.com.sepita.view.activity.IncreaseCreditActivity;
import hoodadtech.com.sepita.view.activity.MainActivity;
import hoodadtech.com.sepita.view.activity.MapsActivity;
import hoodadtech.com.sepita.view.activity.OrderDetailsActivity;
import hoodadtech.com.sepita.view.activity.ProfileActivity;
import hoodadtech.com.sepita.view.activity.SupportActivity;
import hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments.FrgHomeList;
import hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments.FrgPendingOrdersList;


/**
 * https://www.getpostman.com/collections/2f6a9bcc179e404cf659
 * A simple {@link Fragment} subclass.
 */
public class DrawerFragment extends Fragment implements SeterInfo{

    LinearLayout layout_credit;
    TextView txt_credit,tv_fullName,txt_version;
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerWrapper;
    private ListView mDrawerLV;
    private ImageView iv_profile;
    private boolean viewCreated;
    private Server server;
    private PublicMethod publicMethod;
    private Context context;

    public void setmDrawerLayout(DrawerLayout mDrawerLayout) {
        this.mDrawerLayout = mDrawerLayout;
    }

    public void setmDrawerWrapper(LinearLayout mDrawerWrapper) {
        this.mDrawerWrapper = mDrawerWrapper;
    }

    public DrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FrgHomeList.seterInfo = this;
        SupportActivity.seterInfo = this;
        server=Server.getInstance(getActivity());
        View view = inflater.inflate(R.layout.fragment_drawer, container, false);
        mDrawerLV = (ListView) view.findViewById(R.id.drawerLV);
        layout_credit =  view.findViewById(R.id.layout_credit);
        txt_credit =  view.findViewById(R.id.txt_credit);
        tv_fullName = view.findViewById(R.id.tv_fullName);
        iv_profile = view.findViewById(R.id.iv_profile);
        txt_version = view.findViewById(R.id.txt_version);
        mDrawerLV.setOnItemClickListener(new DrawerItemClickListener());
        viewCreated = true;
        fillNavigationDrawerItems();
        if(new SharedPreference().getCodeTekhfif(getActivity())!=null){
            //txt_credit.setText(StringUtility.numberEn2Fa(new SharedPreference().getCodeTekhfif(getActivity())) + "  تومان  ");
            txt_credit.setText("0");
            layout_credit.setVisibility(View.VISIBLE);

        }
        context = getActivity();

        publicMethod = new PublicMethod(getActivity());
        tv_fullName.setText(publicMethod.getFullName());
        txt_credit.setText(publicMethod.getCredit());
        Ion.with(getContext()).load(publicMethod.getImage()).intoImageView(iv_profile);

        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            txt_version.setText("V " + pInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }



        listener();
        return view;
    }

    private void listener() {
        layout_credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getActivity(),getActivity().getString(R.string.increaseCredit),Toast.LENGTH_SHORT).show();
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawer(mDrawerWrapper);
                }
                getActivity().startActivity(new Intent(getActivity(), IncreaseCreditActivity.class));
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mDrawerLayout = null;
        mDrawerWrapper = null;
        mDrawerLV = null;

        viewCreated = false;
    }

    public void fillNavigationDrawerItems() {
        if (!viewCreated) return;
        BaseInflaterAdapter<DrawerItem> adapter = new BaseInflaterAdapter<>(new DrawerItemInflater());
        DrawerItem drawerItem = null;
        if (getActivity() instanceof HomeActivity) {
            adapter.addItem(new DrawerItem(getString(R.string.title_activity_home), R.drawable.credit_icon), false);
            drawerItem = (DrawerItem) adapter.getItem(0);
            drawerItem.setId(0);
            drawerItem.setSelected(false);
        } else {
            adapter.addItem(new DrawerItem(getString(R.string.title_activity_home), R.drawable.credit_icon), false);
            drawerItem = (DrawerItem) adapter.getItem(0);
            drawerItem.setId(0);
            drawerItem.setSelected(false);
        }

        Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (getActivity() instanceof DetailsActivity && currentFragment instanceof FrgPendingOrdersList) {
            adapter.addItem(new DrawerItem(getString(R.string.title_activity_details), R.drawable.requests_icon), false);
            drawerItem = (DrawerItem) adapter.getItem(1);
            drawerItem.setId(1);
            drawerItem.setSelected(true);
        } else {
            adapter.addItem(new DrawerItem(getString(R.string.title_activity_details), R.drawable.requests_icon), false);
            drawerItem = (DrawerItem) adapter.getItem(1);
            drawerItem.setId(1);
            drawerItem.setSelected(false);
        }



        if (getActivity() instanceof ProfileActivity) {
            adapter.addItem(new DrawerItem(getString(R.string.title_activity_profile), R.drawable.profile_icon), false);
            drawerItem = (DrawerItem) adapter.getItem(2);
            drawerItem.setId(2);
            drawerItem.setSelected(true);
        } else {
            adapter.addItem(new DrawerItem(getString(R.string.title_activity_profile), R.drawable.profile_icon), false);
            drawerItem = (DrawerItem) adapter.getItem(2);
            drawerItem.setId(2);
            drawerItem.setSelected(false);
        }
        //if (getActivity() instanceof StaredWorkersActivity && currentFragment instanceof FrgStaredWorkers) {
        //    adapter.addItem(new DrawerItem(getString(R.string.workers), R.drawable.credit_icon ), false);
        //    drawerItem = (DrawerItem) adapter.getItem(3);
        //    drawerItem.setId(3);
        //    drawerItem.setSelected(true);
        //} else {
        //    adapter.addItem(new DrawerItem(getString(R.string.workers),  R.drawable.credit_icon), false);
        //    drawerItem = (DrawerItem) adapter.getItem(3);
        //    drawerItem.setId(3);
        //    drawerItem.setSelected(false);
        //}

        if (getActivity() instanceof AboutUsActivity) {

            adapter.addItem(new DrawerItem(getString(R.string.about_us), R.drawable.about_us_icon), false);
            drawerItem = (DrawerItem) adapter.getItem(3);
            drawerItem.setId(3);
            drawerItem.setSelected(true);
        } else {
            adapter.addItem(new DrawerItem(getString(R.string.about_us),  R.drawable.about_us_icon), false);
            drawerItem = (DrawerItem) adapter.getItem(3);
            drawerItem.setId(3);
            drawerItem.setSelected(false);
        }

        adapter.addItem(new DrawerItem(getString(R.string.introduce),  R.drawable.credit_icon), false);
        drawerItem = (DrawerItem) adapter.getItem(4);
        drawerItem.setId(4);
        drawerItem.setSelected(false);

        if (getActivity() instanceof SupportActivity) {

            adapter.addItem(new DrawerItem(getString(R.string.support),  R.drawable.call_center_icon), false);
            drawerItem = (DrawerItem) adapter.getItem(5);
            drawerItem.setId(5);
            drawerItem.setSelected(true);
        } else {
            adapter.addItem(new DrawerItem(getString(R.string.support), R.drawable.call_center_icon), false);
            drawerItem = (DrawerItem) adapter.getItem(5);
            drawerItem.setId(5);
            drawerItem.setSelected(false);
        }


        // add exit item
        adapter.addItem(new DrawerItem(getString(R.string.exit_app), R.drawable.exite_icon), false);
        drawerItem = (DrawerItem) adapter.getItem(6);
        drawerItem.setId(6);
        drawerItem.setSelected(false);

        mDrawerLV.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    @Override
    public void setInfo(Context context) {

        tv_fullName.setText(publicMethod.getFullName());
        txt_credit.setText(publicMethod.getCredit());
        if (publicMethod.getImage() != null){
            Ion.with(context).load(publicMethod.getImage()).intoImageView(iv_profile);
        }else {
            iv_profile.setImageResource(R.drawable.user);
        }
        //Toast.makeText(context,publicMethod.getCredit(),Toast.LENGTH_SHORT).show();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //view.setBackgroundColor(getResources().getColor(R.color.gray_light0));
            selectItem(position);
        }
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void selectItem(int position) {
        if (!viewCreated) return;
        mDrawerLV.setItemChecked(position, true);
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mDrawerWrapper);
        }
        DrawerItem item = (DrawerItem) mDrawerLV.getAdapter().getItem(position);
        Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (item.getTitle().equals(getString(R.string.title_activity_home))) {
            if (getActivity() instanceof HomeActivity) {


            } else {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        }

        if (item.getTitle().equals(getString(R.string.title_activity_profile))) {
            if (getActivity() instanceof ProfileActivity) {
//                mDrawerLayout.closeDrawer(mDrawerWrapper);

            } else {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        }

        //if (item.getTitle().equals(getString(R.string.workers))) {
        //    if (getActivity() instanceof StaredWorkersActivity ) {
        //             new FragmentHandler(getActivity(),getFragmentManager()).loadFragment(new FrgStaredWorkers(),false);
        //        }
//
//
        //    } else {
        //        Intent intent = new Intent(getActivity(), StaredWorkersActivity.class);
        //        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //        startActivity(intent);
        //    }
        //}

        if (item.getTitle().equals(getString(R.string.about_us))) {
            if (getActivity() instanceof AboutUsActivity) {
//                mDrawerLayout.closeDrawer(mDrawerWrapper);

            } else {
                Intent intent = new Intent(getActivity(), AboutUsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        }

        if (item.getTitle().equals(getString(R.string.support))) {
            if (getActivity() instanceof SupportActivity) {
//                mDrawerLayout.closeDrawer(mDrawerWrapper);

            } else {
                Intent intent = new Intent(getActivity(), SupportActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        }


        if (item.getTitle().equals(getString(R.string.title_activity_details))) {

            if (getActivity() instanceof DetailsActivity) {


                if (currentFragment instanceof FrgPendingOrdersList) {
//                mDrawerLayout.closeDrawer(mDrawerWrapper);
                } else {
                    //new FragmentHandler(getActivity(), getFragmentManager()).loadFragment(new FrgPendingOrdersList(), true);
                    startActivity(new Intent(getActivity(),HistoryActivity.class));
                }


            } else {
                Intent intent = new Intent(getActivity(), DetailsActivity.class);
                intent.putExtra("drawer", true);
                startActivity(intent);
            }
        }

        if (item.getTitle().equals(getString(R.string.exit_app))) {
            showExitDialog();
        }

        if (item.getTitle().equals(getString(R.string.introduce))) {
            share();

        }



    }

    private void share() {
        Server server=Server.getInstance(getActivity());

        server.getInviteText((e, result) -> {
            if (result != null){
                if (result.getHeaders().code() == 401){
                    getToken(2);
                }else if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().getStatusCode() == 16){
                        getToken(2);
                    }else if (result.getResult().getStatus().isSuccess()){
                        String message=result.getResult().getResult();
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT,
                                message);
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                    }else {
                        Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(getActivity(), getString(R.string.checkConnection), Toast.LENGTH_SHORT).show();
            }


        });
    }

    void showExitDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_confirmation, null);
        builder.setView(rootView);
        builder.create();
        AlertDialog show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        IransansButton yesBtn = rootView.findViewById(R.id.yesBtn);
        IranSansTextView textView = rootView.findViewById(R.id.text);
        IransansButton noBtn = rootView.findViewById(R.id.noBtn);
        textView.setText(R.string.areYouWantToExit);
        yesBtn.setOnClickListener(view -> {

            exitFromApp(server);
            show.dismiss();


        });
        noBtn.setOnClickListener(view -> {
                    show.dismiss();
                }
        );
    }

    private void exitFromApp(Server server) {
        server.accountLogout((e, result) -> {
            if (result != null) {
                if (result.getHeaders().code() == 401) {
                    getToken(1);
                } else if (result.getHeaders().code() == 200) {
                    if (result.getResult().getStatus().getStatusCode() == 16){
                        getToken(1);
                    }else if (result.getResult().getStatus().isSuccess()){
                        Toast.makeText(getContext(),result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                        new SharedPreference().clearSharedPreference(getActivity());
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        //    //getActivity().finishAffinity();
                        //
                        //}
                    }else {
                        Toast.makeText(getContext(),result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(getContext(),getString(R.string.checkConnection),Toast.LENGTH_SHORT).show();
            }
        });

    }

    //public void getInfo(){
    //    Server server = Server.getInstance(getActivity());
    //    server.getMenuInfo(new FutureCallback<MenuInfoResult>() {
    //        @Override
    //        public void onCompleted(Exception e, MenuInfoResult result) {
    //            refresh.setRefreshing(false);
    //            if (result != null){
    //                if (result.getStatus() != null) {
    //                    if (result.getStatus().getIsSuccess()) {
    //                        tv_fullName.setText(result.getResult().getFullname());
    //                        //txt_credit.setText(result.getResult().getUserCredit());
    //                        txt_credit.setText(StringUtility.numberEn2Fa(String.valueOf(result.getResult().getUserCredit())));
    //                        if (result.getResult().getImageUrl() == null || result.getResult().getImageUrl().length() == 0){
    //                            iv_profile.setImageResource(R.drawable.user);
    //                        }else {
    //                            Ion.with(getActivity()).load(result.getResult().getImageUrl()).intoImageView(iv_profile);
    //                        }
    //                        //Log.i("kk",result.getResult().getImageUrl());
    //                        //Log.i("kk",result.getResult().getFullname());
    //                    }
    //                }
    //            }
    //        }
    //    });
    //}

    public void getToken(int b){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getContext()), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(getActivity(), result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(getActivity(), sharedPreference.getLoginTokenValue(getActivity()));
                        if (b == 1) {
                            exitFromApp(server1);
                        }else if (b == 2){
                            share();
                        }

                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //isSendRequest = false;
                }
            }
        });
    }
}
