package hoodadtech.com.sepita.view.fragment.entrancefragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.IransansEditText;
import hoodadtech.com.sepita.utils.StringUtility;

public class FrgForgetPassword extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    RelativeLayout mainLayout;
    IransansButton btn_signup;
    ImageView IV_bg;
    IransansEditText ET_phoneNumer;
    ProgressBar progressBar;
    String phoneNumber;
    Server server;
    IranSansTextView btn_login;

    String osVersion, deviceId, deviceModel;
    final String osType = "Android";


    public FrgForgetPassword() {

    }

    public static FrgForgetPassword newInstance(String param1, String param2) {
        FrgForgetPassword fragment = new FrgForgetPassword();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_forgetpassword, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });
        server = Server.getInstance(getContext());
        IV_bg = view.findViewById(R.id.IV_bg);
        ET_phoneNumer = view.findViewById(R.id.input_name);
        btn_login = view.findViewById(R.id.btn_login);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.gray_light0), android.graphics.PorterDuff.Mode.SRC_ATOP);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getActivity().getWindowManager().getDefaultDisplay().getWidth(),
                getActivity().getWindowManager().getDefaultDisplay().getHeight());
        IV_bg.setLayoutParams(layoutParams);
        btn_signup = view.findViewById(R.id.btn_signup);
        mainLayout = view.findViewById(R.id.mainLayout);
        btn_signup.setOnClickListener(this);
        mainLayout.setOnClickListener(this);
        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        FragmentHandler fragmentHandler = new FragmentHandler(getActivity(), getFragmentManager());
        switch (view.getId()) {
            case R.id.btn_signup:
                hideKeyboard();
                phoneNumber = ET_phoneNumer.getMyText();
                if (phoneNumber.length() == 0)
                    Toast.makeText(getContext(), "لطفا شماره تلفن همراه خود را وارد کنید", Toast.LENGTH_SHORT).show();
                else {
                    forgotPassword(fragmentHandler);
                }
                break;
            case R.id.mainLayout:
                hideKeyboard();
                break;
            case R.id.btn_login:
                hideKeyboard();
                fragmentHandler.loadFragment(new FrgLogin(), false);
                break;
        }
    }

    @SuppressLint("HardwareIds")
    private void forgotPassword(FragmentHandler fragmentHandler) {
        progressBar.setVisibility(View.VISIBLE);
        btn_signup.setText(getString(R.string.plzWait));
        btn_signup.setEnabled(false);
        deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
        deviceModel = server.GetDeviceString();
        osVersion = String.valueOf(android.os.Build.VERSION.SDK_INT);
        server.accountForgetPassword(StringUtility.numberFa2En(phoneNumber), deviceId, deviceModel, osType, osVersion,
                (e, result) -> {
                    progressBar.setVisibility(View.INVISIBLE);
                    btn_signup.setText("دریافت کد فعاسازی");
                    btn_signup.setEnabled(true);
                  if(result!=null&&result.getStatus()!=null){
                      if (result.getStatus().getStatusCode()==0){
                          fragmentHandler.loadFragment(new FrgLogin(), true);
                      }
                      else Toast.makeText(getContext(), result.getStatus().getMessage(), Toast.LENGTH_LONG).show();
                  }
                  else Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                });
    }

    void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }


}
