package hoodadtech.com.sepita.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.IranSansTextView;

/**
 * Created by Milad on 1/30/2018.
 */

public class ItemCommentAssistHolder extends RecyclerView.ViewHolder   {
    public IranSansTextView txt_title;
    public RelativeLayout layout_bg;
    public ItemCommentAssistHolder(View itemView) {
        super(itemView);
        txt_title=itemView.findViewById(R.id.txt_title);
        layout_bg=itemView.findViewById(R.id.layout_bg);


    }
}
