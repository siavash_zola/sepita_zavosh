package hoodadtech.com.sepita.model.serverResult.menuInfoResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("userCredit")
    @Expose
    private String userCredit;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUserCredit() {
        return userCredit;
    }

    public void setUserCredit(String userCredit) {
        this.userCredit = userCredit;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
