package hoodadtech.com.sepita.view.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;


import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.newAdapters.LatLonAdapter;
import hoodadtech.com.sepita.model.Address;
import hoodadtech.com.sepita.model.AddressSaved;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.getAddressOnMap.GetAddressOnMapResult;
import hoodadtech.com.sepita.model.serverResult.getAddressOnMap.Result;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.NetworkUtil;
import hoodadtech.com.sepita.utils.OnItemLatLonClickListener;
import hoodadtech.com.sepita.utils.SharedPreference;

public class MapsActivity extends BaseActivity implements GoogleMap.OnCameraMoveListener, OnMapReadyCallback, View.OnClickListener, LocationSource.OnLocationChangedListener ,OnItemLatLonClickListener{

    private GoogleMap mMap;
    private Marker marker;
    IranSansTextView btn_submit;
    Location mLastLocation;
    public static LatLng selectedLatLong;
    public static String selectedId = null;

    LatLng previous;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    String itemString;
    ProgressBar progressBar;
    Toolbar toolbar;
    TextView toolBar_title;

    private RecyclerView recyclerView_latLon;
    private LinearLayoutManager layoutManager;
    private LatLonAdapter adapter;
    private List<AddressSaved> list;
    public static Address address;
    public Boolean isSelected = false;
    private Location myLocation;
    private FusedLocationProviderClient mFusedLocationClient;
    private GoogleApiClient mGoogleApiClient;
    private LatLng myLatLng = null;
    public boolean isSendRequest = false;
    private Server server;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        checkLocationPermission();
        itemString=getIntent().getExtras().getString("service",null);
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //toolBar_title=findViewById(R.id.toolbar_text);
        //toolBar_title.setText(getString(R.string.submit_address));
        btn_submit = findViewById(R.id.btn_submit);
        progressBar = findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.gray_light0), android.graphics.PorterDuff.Mode.SRC_ATOP);
        if(!NetworkUtil.isOnline(this)){
            CustomSnackbar.showSnackBar(btn_submit);
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        btn_submit.setOnClickListener(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        recyclerView_latLon = findViewById(R.id.recyclerView_latLon);
        layoutManager = new LinearLayoutManager(MapsActivity.this,LinearLayoutManager.HORIZONTAL, false);
        list = new ArrayList<>();

        adapter = new LatLonAdapter(list,MapsActivity.this);
        adapter.listener = this;
        recyclerView_latLon.setLayoutManager(layoutManager);
        recyclerView_latLon.setAdapter(adapter);
        server = Server.getInstance(this);
        loadList(server);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        buildGoogleApiClient();
        progressBar.setVisibility(View.GONE);
        mMap = googleMap;
        mMap.setOnCameraMoveListener(this);

        selectedLatLong=mMap.getCameraPosition().target;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        //mLastLocation = null;





        // Add a marker in tehran and move the camera
        LatLng tehran = new LatLng(35.6892, 51.3890);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tehran, 14.0f));

        previous=mMap.getCameraPosition().target;
//        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
//            @Override
//            public void onMapLongClick(LatLng latLng) {
//
//                Geocoder geocoder =
//                        new Geocoder(MapsActivity.this);
//                List<Address> list;
//                try {
//                    list = geocoder.getFromLocation(latLng.latitude,
//                            latLng.longitude, 1);
//                } catch (IOException e) {
//                    return;
//                }
//                Address address = list.get(0);
//                if (marker != null) {
//                    marker.remove();
//                }
//
//                MarkerOptions options = new MarkerOptions()
//                        .title(address.getLocality())
//                        .position(new LatLng(latLng.latitude,
//                                latLng.longitude));
//
//                marker = mMap.addMarker(options);
//            }
//        });

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                if (!isSelected) {
                    selectedId = null;
                }
                isSelected = false;
            }
        });
        mMap.setOnCameraMoveCanceledListener(new GoogleMap.OnCameraMoveCanceledListener() {
            @Override
            public void onCameraMoveCanceled() {
                selectedId = null;
            }
        });

    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        finder(mMap);
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .build();
        mGoogleApiClient.connect();
    }
    public void finder(GoogleMap mMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation!= null) {
            myLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }
        //mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
        //    @Override
        //    public void onMyLocationChange(Location location) {
        //        myLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        //    }
        //});
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    // Logic to handle location object
                    myLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                    CameraUpdate lcn = CameraUpdateFactory.newLatLngZoom(myLatLng, 14.0f);
                    mMap.animateCamera(lcn);
                }else {
                    //Toast.makeText(MapsActivity.this,"nashod",Toast.LENGTH_SHORT).show();
                }
            }
        });


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                if (!isSendRequest) {
                    if (!NetworkUtil.isOnline(this)) {
                        CustomSnackbar.showSnackBar(btn_submit);
                        break;
                    }


                    Intent intent = new Intent(this, DetailsActivity.class);
                    FileUtility fileUtility = new FileUtility(getBaseContext());


                    selectedLatLong = mMap.getCameraPosition().target;

                    address = new Address();
                    address.setLat(String.valueOf(selectedLatLong.latitude));
                    address.setLong(String.valueOf(selectedLatLong.longitude));
                    PostOrderObject postOrderObject = fileUtility.getPostOrderObject();
                    postOrderObject.setAddress(address);
                    fileUtility.writePostOrderObject(postOrderObject);

                    intent.putExtra("service", itemString);
                    //Log.i("kk",selectedLatLong.latitude+"");
                    //Log.i("kk",selectedLatLong.longitude+"");
                    startActivity(intent);
                }
                break;

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (marker != null) {
            marker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        marker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

    }

    @Override
    public void onCameraMove() {
        selectedLatLong=mMap.getCameraPosition().target;
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)

                        .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapsActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {


                        mMap.setOnCameraMoveListener(this);

                        selectedLatLong=mMap.getCameraPosition().target;
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mMap.setMyLocationEnabled(true);
                        LatLng tehran = new LatLng(35.6892, 51.3890);
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tehran, 14.0f));
                        previous=mMap.getCameraPosition().target;

                    }

                } else {

                }
                return;
            }

        }
    }

    @Override
    public void itemLatLonClicked(int position) {
        //Toast.makeText(this,position+"",Toast.LENGTH_SHORT).show();
        if (mMap != null) {
            isSelected = true;
            selectedId = list.get(position).getId();
            CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                    list.get(position).getLatLng(), 14.0f);
            mMap.animateCamera(location);

            LatLng latLng = list.get(position).getLatLng();
            //Log.i("kk",latLng.latitude+"");
            //Log.i("kk",latLng.longitude+"");
        }
    }

    @Override
    public void onBackPressed() {
        if (!isSendRequest) {
            super.onBackPressed();
        }
    }

    void loadList(Server server){
        isSendRequest = true;

        server.getAllAddressOnMap(new FutureCallback<Response<GetAddressOnMapResult>>() {
            @Override
            public void onCompleted(Exception e, Response<GetAddressOnMapResult> result) {
                if (result != null) {
                    if (result.getHeaders().code() == 401) {
                        //getToken
                        getToken();
                    } else if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().getStatusCode() == 16) {
                            //grtToken
                            getToken();
                        } else if (result.getResult().getStatus().getIsSuccess()) {
                            List<Result> result1 = result.getResult().getResult();
                            list.clear();
                            for (int i = 0; i < result1.size(); i++) {
                                AddressSaved addressSaved = new AddressSaved();
                                Result addressServer = result1.get(i);
                                LatLng latLng = null;
                                try {
                                    latLng = new LatLng(Double.parseDouble(addressServer.getLat()), Double.parseDouble(addressServer.getLong()));
                                } catch (Exception e1) {

                                }

                                addressSaved.setLatLng(latLng);
                                addressSaved.setTitle(addressServer.getTitle());
                                addressSaved.setId(addressServer.getId());

                                list.add(addressSaved);

                            }

                            adapter.notifyDataSetChanged();
                            isSendRequest = false;
                        } else {
                            Toast.makeText(MapsActivity.this, result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                            isSendRequest = false;
                        }
                    } else {
                        Toast.makeText(MapsActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                        isSendRequest = false;
                    }
                }else {
                    Toast.makeText(MapsActivity.this, getString(R.string.plzCheckNetwork), Toast.LENGTH_SHORT).show();
                    isSendRequest = false;
                }
            }
        });

    }

    public void getToken(){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(this), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(MapsActivity.this, result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(MapsActivity.this, sharedPreference.getLoginTokenValue(MapsActivity.this));
                        loadList(server1);
                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(MapsActivity.this, result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(MapsActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    isSendRequest = false;
                }
            }
        });
    }
}
