package hoodadtech.com.sepita.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.IranSansTextView;

/**
 * Created by Milad on 1/30/2018.
 */

public class ItemRequestDetailHolder extends RecyclerView.ViewHolder   {
    public IranSansTextView txt_title;
    public ItemRequestDetailHolder(View itemView) {
        super(itemView);
        txt_title=itemView.findViewById(R.id.txt_title);


    }
}
