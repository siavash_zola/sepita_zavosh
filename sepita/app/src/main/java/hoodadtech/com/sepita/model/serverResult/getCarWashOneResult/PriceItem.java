package hoodadtech.com.sepita.model.serverResult.getCarWashOneResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceItem {
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("serviceDetailId")
    @Expose
    private String serviceDetailId;
    @SerializedName("carwashServiceId")
    @Expose
    private String carwashServiceId;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getServiceDetailId() {
        return serviceDetailId;
    }

    public void setServiceDetailId(String serviceDetailId) {
        this.serviceDetailId = serviceDetailId;
    }

    public String getCarwashServiceId() {
        return carwashServiceId;
    }

    public void setCarwashServiceId(String carwashServiceId) {
        this.carwashServiceId = carwashServiceId;
    }

}
