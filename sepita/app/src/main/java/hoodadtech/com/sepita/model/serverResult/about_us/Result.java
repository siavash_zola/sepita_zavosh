package hoodadtech.com.sepita.model.serverResult.about_us;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("about")
    @Expose
    private String about;

    @SerializedName("contact")
    @Expose
    private String contact;

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
