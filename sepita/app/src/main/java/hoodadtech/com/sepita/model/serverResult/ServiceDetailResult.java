
package hoodadtech.com.sepita.model.serverResult;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.ServiceDetail;
import hoodadtech.com.sepita.model.serverResult.serviceDetailResult.ServiceDetailsInfo;

public class ServiceDetailResult {

    @SerializedName("serviceTitle")
    @Expose
    private String serviceTitle;
    @SerializedName("baseDuration")
    @Expose
    private Integer baseDuration;
    @SerializedName("baseAmount")
    @Expose
    private Integer baseAmount;
    @SerializedName("serviceDetails")
    @Expose
    private List<ServiceDetail> serviceDetails = null;




    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public Integer getBaseDuration() {
        return baseDuration;
    }

    public void setBaseDuration(Integer baseDuration) {
        this.baseDuration = baseDuration;
    }

    public Integer getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(Integer baseAmount) {
        this.baseAmount = baseAmount;
    }

    public List<ServiceDetail> getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(List<ServiceDetail> serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

}
