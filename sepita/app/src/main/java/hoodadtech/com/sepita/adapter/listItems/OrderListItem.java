package hoodadtech.com.sepita.adapter.listItems;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.viewHolders.OrderListItemHolder;
import hoodadtech.com.sepita.model.Order;
import hoodadtech.com.sepita.utils.StringUtility;

/**
 * Created by Milad on 1/30/2018.
 */

public class OrderListItem implements MultiAdapterItem {
    OrderListItemHolder itemHolder;
    Order item;

    @Override
    public int getTypeId() {
        return 4;
    }

    @Override
    public void setup(RecyclerView.ViewHolder holder ) {
        itemHolder = (OrderListItemHolder) holder;
        if (itemHolder != null) {
            itemHolder.txt_service_title.setText(item.getServiceTitle());
            itemHolder.TV_status.setText(item.getStatusTitle());
            itemHolder.TV_phone.setText(StringUtility.numberEn2Fa(item.getPhone()));
            itemHolder.TV_date.setText(StringUtility.numberEn2Fa(item.getOrderDate()));
            itemHolder.TV_address.setText(item.getAddress());
            itemHolder.TV_Order_Code.setText(StringUtility.numberEn2Fa(item.getOrderCode()));
            itemHolder.layout_status.setBackgroundColor(Color.parseColor("#"+item.getStatusColor()));
            switch (item.getStatusCode()){
                    //searching
                case "1":
                    //itemHolder.Icon_Status.setText(itemHolder.Icon_Status.getResources().getString(R.string.Icon_Searching));
                    break;
                    //accepted
                case "2":
                    //itemHolder.Icon_Status.setText(itemHolder.Icon_Status.getResources().getString(R.string.Icon_Accepted));
                    break;
                    //doing
                case "3":
                    itemHolder.Icon_cancel.setVisibility(View.GONE);
                    //itemHolder.Icon_Status.setText(itemHolder.Icon_Status.getResources().getString(R.string.Icon_Doing));
                    break;
                    //done
                case "4":
                    itemHolder.Icon_cancel.setVisibility(View.GONE);
                    //itemHolder.Icon_Status.setText(itemHolder.Icon_Status.getResources().getString(R.string.Icon_Done));
                    break;
            }

            itemHolder.layout_details.setOnClickListener(view -> {
                if(itemClickListener!=null){
                    itemClickListener.onClick(item,view);
                }
            });

            itemHolder.Icon_cancel.setOnClickListener(view -> {
                if(itemClickListener!=null){
                    itemClickListener.onClick(item,view);
                }
            });

        }


    }

    public void setItem(Order item) {
        this.item = item;
    }

    @Override
    public Object getItem() {
        return item;
    }

    ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onClick(Order item, View view);

    }
}
