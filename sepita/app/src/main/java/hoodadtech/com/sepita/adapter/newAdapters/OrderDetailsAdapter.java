package hoodadtech.com.sepita.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.koushikdutta.ion.Ion;

import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.RateText;
import hoodadtech.com.sepita.model.serverResult.orderDetailResult.ServiceDetail;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.MyClickListener;
import hoodadtech.com.sepita.view.activity.OrderDetailsActivity;

public class OrderDetailsAdapter extends RecyclerView.Adapter {
    private List<ServiceDetail> list;
    private Activity activity;
    private final int TYPE_HEADER = 0;
    private final int TYPE_FOOTER = 1;
    private final int TYPE_ITEM = 2;
    public MyClickListener listener;

    public OrderDetailsAdapter(List<ServiceDetail> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_detail, parent, false);
            return new ItemViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        ServiceDetail serviceDetail = list.get(position);
        itemViewHolder.tv_title.setText(serviceDetail.getQuantity()+"");
        Ion.with(activity).load(serviceDetail.getIconUrl()).intoImageView(itemViewHolder.iv_icon);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        IranSansTextView tv_title;
        ImageView iv_icon;
        public ItemViewHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            iv_icon = itemView.findViewById(R.id.iv_icon);
        }
    }
}
