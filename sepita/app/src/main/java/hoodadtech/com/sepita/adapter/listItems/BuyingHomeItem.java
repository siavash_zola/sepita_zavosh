package hoodadtech.com.sepita.adapter.listItems;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CompoundButton;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.viewHolders.BuyingHomeItemHolder;
import hoodadtech.com.sepita.model.ServiceDetail;

/**
 * Created by Milad on 1/30/2018.
 */

public class BuyingHomeItem implements MultiAdapterItem {
    BuyingHomeItemHolder itemHolder;
    ServiceDetail item;

    @Override
    public int getTypeId() {
        return 10;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void setup(RecyclerView.ViewHolder holder) {
        itemHolder = (BuyingHomeItemHolder) holder;
        if (itemHolder != null) {
            itemHolder.title.setText(item.getTitle());
            itemHolder.checkBox.setOnCheckedChangeListener(null);
            itemHolder.checkBox.setChecked(item.isSelected());
//            if(item.isSelected())
//                itemHolder.checkBox.setChecked(true);
//            else itemHolder.checkBox.setChecked(false);

            itemHolder.checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                if (checkChangedListener != null) {
                    if(b)
                        item.setSelected(true);
                    else item.setSelected(false);
                    checkChangedListener.onItemClick(item,compoundButton,b);
                }
            });



        }


    }

    public void setItem(ServiceDetail item) {
        this.item = item;
    }

    @Override
    public Object getItem() {
        return item;
    }


    CheckChangedListener checkChangedListener;

    public void setCheckChangedListener(CheckChangedListener checkChangedListener) {
        this.checkChangedListener = checkChangedListener;
    }

    public interface CheckChangedListener {
        void onItemClick(ServiceDetail item, View view, boolean b);
    }
}
