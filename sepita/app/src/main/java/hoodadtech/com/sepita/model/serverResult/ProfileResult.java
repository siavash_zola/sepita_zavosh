
package hoodadtech.com.sepita.model.serverResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileResult {

    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("cellNum")
    @Expose
    private String cellNum;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("introducerCode")
    @Expose
    private String introducerCode;
    @SerializedName("birthDate")
    @Expose
    private String birthDate;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCellNum() {
        return cellNum;
    }

    public void setCellNum(String cellNum) {
        this.cellNum = cellNum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String  getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIntroducerCode() {
        return introducerCode;
    }

    public void setIntroducerCode(String introducerCode) {
        this.introducerCode = introducerCode;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "ProfileResult{" +
                "fullName='" + fullName + '\'' +
                ", code='" + code + '\'' +
                ", cellNum='" + cellNum + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", introducerCode='" + introducerCode + '\'' +
                ", birthDate='" + birthDate + '\'' +
                '}';
    }
}
