
package hoodadtech.com.sepita.model.serverResult;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.ServiceDetail2;

public class OrderDetailResult {

    @SerializedName("serviceTitle")
    @Expose
    private String serviceTitle;
    @SerializedName("employeeId")
    @Expose
    private String employeeId;
    @SerializedName("orderCode")
    @Expose
    private String orderCode;
    @SerializedName("employeeTitle")
    @Expose
    private String employeeTitle;
    @SerializedName("employeeStar")
    @Expose
    private Integer employeeStar;
    @SerializedName("employeeImageUrl")
    @Expose
    private Object employeeImageUrl;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("finalAmount")
    @Expose
    private float finalAmount;
    @SerializedName("paymentTypeTitle")
    @Expose
    private String paymentTypeTitle;
    @SerializedName("visitDate")
    @Expose
    private String visitDate;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("employeeGender")
    @Expose
    private String employeeGender;
    @SerializedName("serviceType")
    @Expose
    private String serviceType;

    @SerializedName("orderRemain")
    @Expose
    private float orderRemain;


    @SerializedName("serviceDetails")
    @Expose
    private List<ServiceDetail2> serviceDetails = null;

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getEmployeeTitle() {
        return employeeTitle;
    }

    public void setEmployeeTitle(String employeeTitle) {
        this.employeeTitle = employeeTitle;
    }

    public Integer getEmployeeStar() {
        return employeeStar;
    }

    public void setEmployeeStar(Integer employeeStar) {
        this.employeeStar = employeeStar;
    }

    public Object getEmployeeImageUrl() {
        return employeeImageUrl;
    }

    public void setEmployeeImageUrl(Object employeeImageUrl) {
        this.employeeImageUrl = employeeImageUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public float getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(float finalAmount) {
        this.finalAmount = finalAmount;
    }

    public String getPaymentTypeTitle() {
        return paymentTypeTitle;
    }

    public void setPaymentTypeTitle(String paymentTypeTitle) {
        this.paymentTypeTitle = paymentTypeTitle;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getEmployeeGender() {
        return employeeGender;
    }

    public void setEmployeeGender(String employeeGender) {
        this.employeeGender = employeeGender;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public List<ServiceDetail2> getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(List<ServiceDetail2> serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public float getOrderRemain() {
        return orderRemain;
    }

    public void setOrderRemain(float orderRemain) {
        this.orderRemain = orderRemain;
    }
}
