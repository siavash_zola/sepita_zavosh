package hoodadtech.com.sepita.view.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;


import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.MultiRecyclerAdapter;
import hoodadtech.com.sepita.adapter.listItems.ItemCommentAssist;
import hoodadtech.com.sepita.adapter.newAdapters.RateAdapter;
import hoodadtech.com.sepita.model.RateText;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.OrderCommentResult;
import hoodadtech.com.sepita.model.PostOrderCommentRequst;
import hoodadtech.com.sepita.model.Reason;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.IransansEditText;
import hoodadtech.com.sepita.utils.MyClickListener;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments.FrgHomeList;

public class HomeActivity extends BaseActivity implements View.OnClickListener, ItemCommentAssist.ItemClickListener ,MyClickListener {
    List<Fragment> fragments = new ArrayList<>();
    MyPagerAdapter adapter;
    ViewPager pager;
    ImageView nav_icon;
    int i = 0;
    String starNumber = "1";
    List<String> selectedCommentsIds;
    PostOrderCommentRequst postOrderCommentRequst ;

    Dialog dialog_first_comment, dialog_second_comment;
    RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    private MultiRecyclerAdapter multiRecyclerAdapter;
    private List<MultiAdapterItem> homeItems;
    Toolbar toolbar;
    private List<RateText> list;
    private RateAdapter rateAdapter;
    private LinearLayout toolbar_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        pager = findViewById(R.id.pager);
        postOrderCommentRequst = new PostOrderCommentRequst();
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        toolbar_back = findViewById(R.id.toolbar_back);
        listener();


        FrgHomeList frgHomeList = new FrgHomeList();
        fragments.add(frgHomeList);
        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(0);
        list = new ArrayList<>();

    }

    private void listener() {
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Server server = Server.getInstance(getApplicationContext(), new SharedPreference().getLoginTokenValue(getApplicationContext()));
        server.getOrderComment((e, result) -> {
            if(e!=null){

            }
            else if (OperationResult.isOk(result.getResult(), e)) {
                //showFirstCommentDialog(result.getResult().getResult());
                showNewDialog(result);
            }
        });
        //showNewDialog();
    }

    private void showNewDialog(Response<OperationResult<OrderCommentResult>> result) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_set_rate, null);
        builder.setView(rootView);
        builder.create();
        AlertDialog show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        List<Reason> badReason = result.getResult().getResult().getBadReason();
        List<Reason> goodReason = result.getResult().getResult().getGoodReason();

        convertToMyList(badReason);

        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView);

        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);
        rateAdapter = new RateAdapter(list,this);
        rateAdapter.listener = this;
        recyclerView.setAdapter(rateAdapter);

        LinearLayout back_one = rootView.findViewById(R.id.back_one);
        LinearLayout back_two = rootView.findViewById(R.id.back_two);
        LinearLayout back_three = rootView.findViewById(R.id.back_three);
        LinearLayout back_four = rootView.findViewById(R.id.back_four);
        LinearLayout back_five = rootView.findViewById(R.id.back_five);

        IranSansTextView tv_one = rootView.findViewById(R.id.tv_one);
        IranSansTextView tv_two = rootView.findViewById(R.id.tv_two);
        IranSansTextView tv_three = rootView.findViewById(R.id.tv_three);
        IranSansTextView tv_four = rootView.findViewById(R.id.tv_four);
        IranSansTextView tv_five = rootView.findViewById(R.id.tv_five);

        IranSansTextView tv_fullName = rootView.findViewById(R.id.tv_fullName);
        IranSansTextView tv_numberOrder = rootView.findViewById(R.id.tv_numberOrder);
        IranSansTextView tv_title = rootView.findViewById(R.id.tv_title);
        IransansEditText ev_description = rootView.findViewById(R.id.ev_description);
        ImageView iv_employee = rootView.findViewById(R.id.iv_employee);
        IransansButton btn_send = rootView.findViewById(R.id.btn_send);
        ProgressBar progressBar = rootView.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);

        tv_fullName.setText(result.getResult().getResult().getEmployeeName());
        tv_numberOrder.setText(result.getResult().getResult().getOrderCode()+"");
        tv_title.setText(result.getResult().getResult().getServiceType());
        if (result.getResult().getResult().getEmployeeImageUrl().length() == 0 || result.getResult().getResult().getEmployeeImageUrl() == null || result.getResult().getResult().getEmployeeImageUrl().equals("")){

        }else {
            Ion.with(HomeActivity.this).load(result.getResult().getResult().getEmployeeImageUrl()).intoImageView(iv_employee);
        }




        tv_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                convertToMyList(badReason);
                starNumber ="1";
                back_one.setBackgroundResource(R.drawable.bg_button_blue);
                back_two.setBackgroundResource(R.drawable.bg_button_silver);
                back_three.setBackgroundResource(R.drawable.bg_button_silver);
                back_four.setBackgroundResource(R.drawable.bg_button_silver);
                back_five.setBackgroundResource(R.drawable.bg_button_silver);

                tv_one.setTextColor(getResources().getColor(R.color.white));
                tv_two.setTextColor(getResources().getColor(R.color.text_color));
                tv_three.setTextColor(getResources().getColor(R.color.text_color));
                tv_four.setTextColor(getResources().getColor(R.color.text_color));
                tv_five.setTextColor(getResources().getColor(R.color.text_color));

                rateAdapter.notifyDataSetChanged();
            }
        });
        tv_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                convertToMyList(badReason);
                starNumber ="2";
                back_one.setBackgroundResource(R.drawable.bg_button_blue);
                back_two.setBackgroundResource(R.drawable.bg_button_blue);
                back_three.setBackgroundResource(R.drawable.bg_button_silver);
                back_four.setBackgroundResource(R.drawable.bg_button_silver);
                back_five.setBackgroundResource(R.drawable.bg_button_silver);

                tv_one.setTextColor(getResources().getColor(R.color.white));
                tv_two.setTextColor(getResources().getColor(R.color.white));
                tv_three.setTextColor(getResources().getColor(R.color.text_color));
                tv_four.setTextColor(getResources().getColor(R.color.text_color));
                tv_five.setTextColor(getResources().getColor(R.color.text_color));

                rateAdapter.notifyDataSetChanged();
            }
        });
        tv_three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                convertToMyList(badReason);
                starNumber ="3";
                back_one.setBackgroundResource(R.drawable.bg_button_blue);
                back_two.setBackgroundResource(R.drawable.bg_button_blue);
                back_three.setBackgroundResource(R.drawable.bg_button_blue);
                back_four.setBackgroundResource(R.drawable.bg_button_silver);
                back_five.setBackgroundResource(R.drawable.bg_button_silver);

                tv_one.setTextColor(getResources().getColor(R.color.white));
                tv_two.setTextColor(getResources().getColor(R.color.white));
                tv_three.setTextColor(getResources().getColor(R.color.white));
                tv_four.setTextColor(getResources().getColor(R.color.text_color));
                tv_five.setTextColor(getResources().getColor(R.color.text_color));

                rateAdapter.notifyDataSetChanged();
            }
        });
        tv_four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                convertToMyList(badReason);
                starNumber ="4";

                back_one.setBackgroundResource(R.drawable.bg_button_blue);
                back_two.setBackgroundResource(R.drawable.bg_button_blue);
                back_three.setBackgroundResource(R.drawable.bg_button_blue);
                back_four.setBackgroundResource(R.drawable.bg_button_blue);
                back_five.setBackgroundResource(R.drawable.bg_button_silver);

                tv_one.setTextColor(getResources().getColor(R.color.white));
                tv_two.setTextColor(getResources().getColor(R.color.white));
                tv_three.setTextColor(getResources().getColor(R.color.white));
                tv_four.setTextColor(getResources().getColor(R.color.white));
                tv_five.setTextColor(getResources().getColor(R.color.text_color));

                rateAdapter.notifyDataSetChanged();
            }
        });
        tv_five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                convertToMyList(goodReason);
                starNumber ="5";
                back_one.setBackgroundResource(R.drawable.bg_button_blue);
                back_two.setBackgroundResource(R.drawable.bg_button_blue);
                back_three.setBackgroundResource(R.drawable.bg_button_blue);
                back_four.setBackgroundResource(R.drawable.bg_button_blue);
                back_five.setBackgroundResource(R.drawable.bg_button_blue);

                tv_one.setTextColor(getResources().getColor(R.color.white));
                tv_two.setTextColor(getResources().getColor(R.color.white));
                tv_three.setTextColor(getResources().getColor(R.color.white));
                tv_four.setTextColor(getResources().getColor(R.color.white));
                tv_five.setTextColor(getResources().getColor(R.color.white));

                rateAdapter.notifyDataSetChanged();
            }
        });

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                btn_send.setVisibility(View.GONE);
                PostOrderCommentRequst postOrderCommentRequst = new PostOrderCommentRequst();
                postOrderCommentRequst.setCommentDesc(ev_description.getMyText());
                postOrderCommentRequst.setOrderCode(result.getResult().getResult().getOrderCode()+"");
                postOrderCommentRequst.setStarNumber(starNumber);
                List<String> strings = new ArrayList<>();
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).isSelected()){
                        strings.add(list.get(j).getId());
                    }
                }
                postOrderCommentRequst.setCommentItems(strings);
                Server server = Server.getInstance(HomeActivity.this, new SharedPreference().getLoginTokenValue(HomeActivity.this));
                server.postOrderCommetn(postOrderCommentRequst, (e, result1) -> {
                    progressBar.setVisibility(View.GONE);
                    btn_send.setVisibility(View.VISIBLE);
                    try{
                        if (result1 != null) {
                            if (result1 != null && result1.getStatus() != null && result1.getStatus().isSuccess()) {
                                Toast.makeText(HomeActivity.this, result1.getStatus().getMessage(), Toast.LENGTH_LONG).show();
                                show.dismiss();

                            } else if (result1 != null && result1.getStatus() != null && !result1.getStatus().isSuccess()) {
                                Toast.makeText(HomeActivity.this, result1.getStatus().getMessage(), Toast.LENGTH_LONG).show();


                            }
                        }else {
                            Toast.makeText(HomeActivity.this, getString(R.string.error), Toast.LENGTH_LONG).show();
                        }


                    }
                    catch (Exception e1){

                    }
                });
            }
        });
    }

    private void convertToMyList(List<Reason> badReason) {
        list.clear();
        for (int j = 0; j < badReason.size(); j++) {
            RateText rateText = new RateText();
            rateText.setTitle(badReason.get(j).getTitle());
            rateText.setId(badReason.get(j).getId());
            rateText.setSelected(false);
            list.add(rateText);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.nav_icon:
//                if(i==0){
//                pager.setCurrentItem(1);
//                i=1;
//                }else {
//                    pager.setCurrentItem(0);
//                    i=0;
//                }
                //mDrawerLayout.openDrawer(Gravity.RIGHT);

                break;
        }
    }


    @Override
    public void onClick(Reason item, View view) {
        if (item.isSelected()) {
            item.setSelected(false);
            for(Iterator<String> iterator = selectedCommentsIds.iterator(); iterator.hasNext();){
                String reason=iterator.next();
                if(reason.equals(item.getId())){
                    iterator.remove();
                }

            }


        } else {
            item.setSelected(true);
            selectedCommentsIds.add(item.getId());
        }

    }

    @Override
    public void itemClicked(int position) {
        //for (int i = 0; i < list.size(); i++) {
        //    RateText rateText = list.get(i);
        //    if (i == position){
        //        rateText.setSelected(true);
        //    }else {
        //        rateText.setSelected(false);
        //    }
        //}
        //rateAdapter.notifyDataSetChanged();

        RateText rateText = list.get(position);
        if (rateText.isSelected()){
            rateText.setSelected(false);
        }else {
            rateText.setSelected(true);
        }
        rateAdapter.notifyDataSetChanged();
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        public MyPagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "";
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {

            return fragments.get(position);
        }

        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
            if (observer != null) {
                super.unregisterDataSetObserver(observer);
            }
        }
    }



}
