
package hoodadtech.com.sepita.model.serverResult;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.Reason;

public class OrderCommentResult {

    @SerializedName("orderCode")
    @Expose
    private Integer orderCode;
    @SerializedName("employeeCode")
    @Expose
    private Integer employeeCode;
    @SerializedName("employeeName")
    @Expose
    private String employeeName;
    @SerializedName("employeeImageUrl")
    @Expose
    private String employeeImageUrl;
    @SerializedName("goodReason")
    @Expose
    private List<Reason> goodReason = null;
    @SerializedName("badReason")
    @Expose
    private List<Reason> badReason = null;

    @SerializedName("serviceType")
    @Expose
    private String serviceType;

    public Integer getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(Integer orderCode) {
        this.orderCode = orderCode;
    }

    public Integer getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(Integer employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeImageUrl() {
        return employeeImageUrl;
    }

    public void setEmployeeImageUrl(String employeeImageUrl) {
        this.employeeImageUrl = employeeImageUrl;
    }

    public List<Reason> getGoodReason() {
        return goodReason;
    }

    public void setGoodReason(List<Reason> goodReason) {
        this.goodReason = goodReason;
    }

    public List<Reason> getBadReason() {
        return badReason;
    }

    public void setBadReason(List<Reason> badReason) {
        this.badReason = badReason;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
