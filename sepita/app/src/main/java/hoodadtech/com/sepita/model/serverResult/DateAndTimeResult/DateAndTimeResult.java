
package hoodadtech.com.sepita.model.serverResult.DateAndTimeResult;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.Date;
import hoodadtech.com.sepita.model.serverResult.addAddress.Status;

public class DateAndTimeResult {

    @SerializedName("result")
    @Expose
    private ResultDT result ;
    @SerializedName("status")
    @Expose
    private Status status;

    public ResultDT getResult() {
        return result;
    }

    public void setResult(ResultDT result) {
        this.result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
