
package hoodadtech.com.sepita.model.serverResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


import hoodadtech.com.sepita.model.ServiceDetail;

public class CarWashServiceDetailsResult {


    @SerializedName("serviceDesc")
    @Expose
    private String serviceDesc;

    @SerializedName("serviceTitle")
    @Expose
    private String serviceTitle;
    @SerializedName("carTypes")
    @Expose
    private List<CarBrand> carTypes = null;
    @SerializedName("serviceDetails")
    @Expose
    private List<ServiceDetailCar> serviceDetails = null;

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public List<CarBrand> getCarTypes() {
        return carTypes;
    }

    public void setCarTypes(List<CarBrand> carBrands) {
        this.carTypes = carBrands;
    }

    public List<ServiceDetailCar> getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(List<ServiceDetailCar> serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }
}
