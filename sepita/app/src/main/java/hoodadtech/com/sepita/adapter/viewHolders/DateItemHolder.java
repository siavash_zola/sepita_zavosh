package hoodadtech.com.sepita.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import hoodadtech.com.sepita.R;

/**
 * Created by Milad on 1/5/2018.
 */

public class DateItemHolder extends RecyclerView.ViewHolder  {

    public TextView title;

    public DateItemHolder(View itemView) {
        super(itemView);
        title=itemView.findViewById(R.id.title);
    }
}
