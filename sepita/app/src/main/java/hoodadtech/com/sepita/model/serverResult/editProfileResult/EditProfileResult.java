package hoodadtech.com.sepita.model.serverResult.editProfileResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.serverResult.addAddress.Status;

public class EditProfileResult {
    @SerializedName("result")
    @Expose
    private EditProfileResult result;
    @SerializedName("status")
    @Expose
    private Status status;

    public EditProfileResult getResult() {
        return result;
    }

    public void setResult(EditProfileResult result) {
        this.result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
