package hoodadtech.com.sepita.model.serverResult.getCarWashOneResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarwashService {
    @SerializedName("carwashServiceTitle")
    @Expose
    private String carwashServiceTitle;
    @SerializedName("carwashServiceId")
    @Expose
    private String carwashServiceId;

    public String getCarwashServiceTitle() {
        return carwashServiceTitle;
    }

    public void setCarwashServiceTitle(String carwashServiceTitle) {
        this.carwashServiceTitle = carwashServiceTitle;
    }

    public String getCarwashServiceId() {
        return carwashServiceId;
    }

    public void setCarwashServiceId(String carwashServiceId) {
        this.carwashServiceId = carwashServiceId;
    }
}
