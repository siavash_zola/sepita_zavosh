package hoodadtech.com.sepita.view.fragment.entrancefragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.FragmentHandler;

public class FrgEntrance extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    Button btn_login;
    Button btn_register;


    public FrgEntrance() {

    }

    public static FrgEntrance newInstance(String param1, String param2) {
        FrgEntrance fragment = new FrgEntrance();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.frg_entrance, container, false);

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });
        initView(view);
        return view;
    }

    private void initView(View view) {
        btn_login=view.findViewById(R.id.btn_login);
        btn_register=view.findViewById(R.id.btn_signup);
        btn_register.setOnClickListener(this);
        btn_login.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        FragmentHandler fragmentHandler=new FragmentHandler(getActivity(),getFragmentManager());
        switch (view.getId()){
            case R.id.btn_login:
                fragmentHandler.loadFragment(new FrgLogin(),true);
                break;
            case R.id.btn_signup:
                fragmentHandler.loadFragment(new FrgRegister(),true);
                break;
        }


    }
}
