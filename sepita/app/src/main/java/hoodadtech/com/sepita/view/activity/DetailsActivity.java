package hoodadtech.com.sepita.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.Service;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.ServiceType;
import hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments.FrgBuyingHome;
import hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments.FrgFirstDetail;
import hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments.FrgUsedWorkers;

public class DetailsActivity extends BaseActivity {
    String serviceItemString;
    Service service;
    Toolbar toolbar;
    TextView toolBar_title;
    private LinearLayout toolbar_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //toolBar_title = findViewById(R.id.toolbar_text);
        //toolBar_title.setText(getString(R.string.title_activity_details));
        serviceItemString = getIntent().getExtras().getString("service", null);
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBoolean("drawer", false)) {
            loadPendingOrderList();
        } else if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getBoolean("workers", false)) {
            FragmentHandler fragmentHandler = new FragmentHandler(this, getSupportFragmentManager());
            fragmentHandler.loadFragment(new FrgUsedWorkers(), false);
        } else
            loadFragment();

        toolbar_back = findViewById(R.id.toolbar_back);
        listener();
    }

    private void loadPendingOrderList() {
        //FragmentHandler fragmentHandler = new FragmentHandler(this, getSupportFragmentManager());
        //fragmentHandler.loadFragment(new FrgPendingOrdersList(), false);
        finish();
        startActivity(new Intent(DetailsActivity.this,HistoryActivity.class));
    }

    private void loadFragment() {
        service = new Gson().fromJson(serviceItemString, Service.class);
        FragmentHandler fragmentHandler = new FragmentHandler(this, getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("service", serviceItemString);
        //switch (service.getServiceTypeId()) {
        switch (service.getServiceTypeCode()) {
            case ServiceType.HAUSE_CLEANING_code:
            //case ServiceType.HAUSE_CLEANING_code:
                FrgFirstDetail frgFirstDetail = new FrgFirstDetail();
                frgFirstDetail.setArguments(bundle);
                fragmentHandler.loadFragment(frgFirstDetail, false);
                break;

            case ServiceType.CARWASH_code:
                FrgFirstDetail frgFirstDetail1 = new FrgFirstDetail();
                frgFirstDetail1.setArguments(bundle);
                fragmentHandler.loadFragment(frgFirstDetail1, false);
                break;

            case ServiceType.HOUSEBUYING_code:
                FrgBuyingHome frgBuyingHome = new FrgBuyingHome();
                frgBuyingHome.setArguments(bundle);
                fragmentHandler.loadFragment(frgBuyingHome, false);
                break;
            case ServiceType.COMPLEX_code:
                FrgFirstDetail frgFirstDetailN = new FrgFirstDetail();
                frgFirstDetailN.setArguments(bundle);
                fragmentHandler.loadFragment(frgFirstDetailN, false);
                break;


        }

    }

    public void updateDrawerFragmentUi(){
        this.filldraweritems();
    }
    private void listener() {
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
