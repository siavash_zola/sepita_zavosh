package hoodadtech.com.sepita.model.serverResult.serviceDetailResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import hoodadtech.com.sepita.model.ServiceDetailInfoItem;

public class ServiceDetailsInfo {
    @SerializedName("serviceDetailId")
    @Expose
    private String serviceDetailId;

    @SerializedName("serviceDetailInfoItems")
    @Expose
    private List<ServiceDetailInfoItem> serviceDetailInfoItems = null;

    public String getServiceDetailId() {
        return serviceDetailId;
    }

    public void setServiceDetailId(String serviceDetailId) {
        this.serviceDetailId = serviceDetailId;
    }

    public List<ServiceDetailInfoItem> getServiceDetailInfoItems() {
        return serviceDetailInfoItems;
    }

    public void setServiceDetailInfoItems(List<ServiceDetailInfoItem> serviceDetailInfoItems) {
        this.serviceDetailInfoItems = serviceDetailInfoItems;
    }
}
