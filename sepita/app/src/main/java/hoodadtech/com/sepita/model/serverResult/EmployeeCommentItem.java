
package hoodadtech.com.sepita.model.serverResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EmployeeCommentItem {

    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("previousDays")
    @Expose
    private String previousDays;
    @SerializedName("commentBody")
    @Expose
    private String commentBody;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPreviousDays() {
        return previousDays;
    }

    public void setPreviousDays(String previousDays) {
        this.previousDays = previousDays;
    }

    public String getCommentBody() {
        return commentBody;
    }

    public void setCommentBody(String commentBody) {
        this.commentBody = commentBody;
    }

}
