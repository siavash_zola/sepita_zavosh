package hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.AddressNew;
import hoodadtech.com.sepita.model.PostOrderNew;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.Service;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.PostOrderResult;
import hoodadtech.com.sepita.model.serverResult.PostOrderResultNew;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.IransansEditText;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import hoodadtech.com.sepita.view.activity.AboutUsActivity;
import hoodadtech.com.sepita.view.activity.OrderDetailsActivity;

public class FrgPayment extends Fragment implements View.OnClickListener {

    Button btn_submit;
    IranSansTextView price, takhfif, finalPrice, TV_takhfif, TV_message, txt_credit;
    IranSansTextView   btn_textView;
    LinearLayout mainLayout,btn_login;
    Server server;
    String discountCode;
    IransansEditText et_discountCode;
    ProgressBar progressBar, progressBar_post;
    PostOrderObject postOrderObject;
    FileUtility fileUtility;
    RelativeLayout layout_discount, layout_payment_price;
    IransansEditText et_description;
    private AlertDialog show;
    private RelativeLayout relativeLayout;
    ImageView iv_level;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        View view = inflater.inflate(R.layout.frg_payment, container, false);
        fileUtility = new FileUtility(getContext());
        postOrderObject = fileUtility.getPostOrderObject();
        postOrderObject.setIsUseCredit("false");
        server = Server.getInstance(getContext(), new SharedPreference().getLoginTokenValue(getActivity()));
        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(() -> {

        });
        initView(view);
        return view;
    }

    private void initView(View view) {
        //TextView title = getActivity().findViewById(R.id.toolbar_text);
        //title.setText(getResources().getString(R.string.payment));

        price = view.findViewById(R.id.TV_price);
        //Log.i("kk",postOrderObject.getNetAmount());
        price.setText(StringUtility.numberEn2Fa(postOrderObject.getNetAmount() + " تومان "));
        postOrderObject.setFinalAmunt(postOrderObject.getNetAmount());
        postOrderObject.setPhone(postOrderObject.getAddress().getPhone());
        postOrderObject.setIsCredit("false");
        postOrderObject.setDiscountCodeId("");
        progressBar_post = view.findViewById(R.id.progressBar_post);
        progressBar_post.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);



        TV_takhfif = view.findViewById(R.id.TV_takhfif);
        btn_textView = view.findViewById(R.id.btn_textView);
        mainLayout = view.findViewById(R.id.mainLayout);
        txt_credit = view.findViewById(R.id.txt_credit);
        et_description = view.findViewById(R.id.ET_description);
        price.setText(StringUtility.numberEn2Fa(price.getText().toString()));
        takhfif = view.findViewById(R.id.takhfif);
        layout_payment_price = view.findViewById(R.id.layout_payment_price);
        layout_discount = view.findViewById(R.id.layout_discount);

        takhfif.setText(StringUtility.numberEn2Fa(String.valueOf(0) + " تومان "));


        txt_credit.setText(StringUtility.numberEn2Fa(new SharedPreference().getCodeTekhfif(getContext()) + " تومان "));
        finalPrice = view.findViewById(R.id.finalprice);
        finalPrice.setText(StringUtility.numberEn2Fa(postOrderObject.getFinalAmunt() + " تومان "));

        mainLayout.setOnClickListener(this);
        btn_submit = view.findViewById(R.id.btn_submit);
        btn_login = view.findViewById(R.id.btn_login);
        btn_submit.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        TV_takhfif.setOnClickListener(this);

        iv_level = view.findViewById(R.id.iv_level);
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey("serviceCode")){
            iv_level.setImageResource(R.drawable.level_three);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mainLayout:
                hideKeyboard();
                break;
            case R.id.btn_submit:
                hideKeyboard();
                discountCodeValidate(server);
                break;
            //case R.id.pay_cash:
            //    pay_cash.setBackgroundResource(R.drawable.bg_activated);
            //    pay_cash.setTextColor(getActivity().getResources().getColor(R.color.white));
            //    btn_textView.setText("ارسال درخواست");
            //    pay_credit.setBackgroundResource(R.drawable.bg_deactivated);
            //    pay_credit.setTextColor(getActivity().getResources().getColor(R.color.accent));
            //    pay_online.setBackgroundResource(R.drawable.bg_deactivated);
            //    pay_online.setTextColor(getActivity().getResources().getColor(R.color.accent));
            //    break;
            //case R.id.pay_credit:
            //    pay_credit.setBackgroundResource(R.drawable.bg_activated);
            //    pay_credit.setTextColor(getActivity().getResources().getColor(R.color.white));
            //    btn_textView.setText("پرداخت");
            //    pay_cash.setBackgroundResource(R.drawable.bg_deactivated);
            //    pay_cash.setTextColor(getActivity().getResources().getColor(R.color.accent));
//
            //    pay_online.setBackgroundResource(R.drawable.bg_deactivated);
            //    pay_online.setTextColor(getActivity().getResources().getColor(R.color.accent));
            //    break;
            //case R.id.pay_online:
            //    btn_textView.setText("پرداخت");
            //    pay_online.setBackgroundResource(R.drawable.bg_activated);
            //    pay_online.setTextColor(getActivity().getResources().getColor(R.color.white));
//
            //    pay_cash.setBackgroundResource(R.drawable.bg_deactivated);
            //    pay_cash.setTextColor(getActivity().getResources().getColor(R.color.accent));
//
            //    pay_credit.setBackgroundResource(R.drawable.bg_deactivated);
            //    pay_credit.setTextColor(getActivity().getResources().getColor(R.color.accent));
            //    break;
            case R.id.btn_login:
                progressBar_post.setVisibility(View.VISIBLE);
                postOrderObject.setDescription(et_description.getMyText());
                btn_login.setEnabled(false);
                btn_textView.setText(getString(R.string.plzWait));
                //Log.i("kk",postOrderObject.toString());
                //server.postOrder(new Gson().toJson(postOrderObject), (e, result) -> {
                 //  if (isAdded()) {
                //        progressBar_post.setVisibility(View.INVISIBLE);
                //        btn_login.setEnabled(true);
                //        btn_textView.setText("ارسال درخواست");
//
                //        if (e != null) {
                //            CustomSnackbar.showSnackBar(view);
                //        } else if (result != null &&result.getResult()!=null&& result.getStatus() != null && result.getStatus().isSuccess()) {
                //            FragmentHandler fragmentHandler = new FragmentHandler(getActivity(), getFragmentManager());
                //            fragmentHandler.loadFragment(new FrgPendingOrdersList(), true);
                //            Toast.makeText(getContext(), result.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                //        } else if (result != null && result.getStatus() != null) {
                //            Toast.makeText(getActivity(), result.getStatus().getMessage(), Toast.LENGTH_LONG).show();
                //        } else
                //            Toast.makeText(getActivity(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_LONG).show();
                //    }
                //});

                //--------------------------------------------------------

                PostOrderNew postOrderNew = convertData(postOrderObject);
                postOrder(server,postOrderNew);

                break;
            case R.id.TV_takhfif:
                showDialog();
                break;

        }
    }

    private void postOrder(Server server, PostOrderNew postOrderNew) {
        server.postOrderNew(new Gson().toJson(postOrderNew), new FutureCallback<Response<PostOrderResultNew>>() {
            @Override
            public void onCompleted(Exception e, Response<PostOrderResultNew> result) {
                progressBar_post.setVisibility(View.INVISIBLE);
                btn_login.setEnabled(true);
                btn_textView.setText(getString(R.string.sendRequest));

                if (result != null) {
                    if (result.getHeaders().code() == 401) {
                        getToken(1, postOrderNew, null);
                    } else if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().getStatusCode() == 16) {
                            getToken(1, postOrderNew, null);
                        } else if (result.getResult().getStatus().getIsSuccess()) {
                            Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);
                            intent.putExtra("id", result.getResult().getResult().getOrderId());
                            intent.putExtra("checkFirst", "1");
                            getActivity().startActivity(intent);

                        } else {
                            Toast.makeText(getContext(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.checkConnection), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private PostOrderNew convertData(PostOrderObject pOld) {
        PostOrderNew pNew = new PostOrderNew();
        pNew.setServiceId(pOld.getServiceId());
        Log.i("json",pNew.getServiceId());
        pNew.setVisitDate(pOld.getVisitDate());
        Log.i("json",pNew.getVisitDate());
        pNew.setVisitTime(pOld.getVisitTime());
        Log.i("json",pNew.getVisitTime());
        pNew.setEmployeeGender(pOld.getEmployeeGender());
        Log.i("json",pNew.getEmployeeGender());
        pNew.setNetAmount(pOld.getNetAmount());
        Log.i("json",pNew.getNetAmount());
        pNew.setFinalAmunt(pOld.getFinalAmunt());
        Log.i("json",pNew.getFinalAmunt());
        pNew.setServiceDetails(pOld.getServiceDetails());

        for (int i = 0; i <pNew.getServiceDetails().size() ; i++) {
            Log.i("json",pNew.getServiceDetails().get(i).getQuantity());
            Log.i("json",pNew.getServiceDetails().get(i).getServiceDetailId());
        }


        pNew.setDuration(pOld.getDuration());
        Log.i("json",pNew.getDuration()+"");
        pNew.setNumber(pOld.getNumber());
        Log.i("json",pNew.getNumber()+"");
        pNew.setDiscountCodeId(pOld.getDiscountCodeId());
        Log.i("json","DiscountCodeId  "+pNew.getDiscountCodeId());
        pNew.setDescription(pOld.getDescription());
        Log.i("json",pNew.getDescription());
        pNew.setAddressId(FrgAddress.addressSelected.getId());
        Log.i("json",FrgAddress.addressSelected.getId());
        pNew.setServiceTypeCodeId(pOld.getServiceTypeCodeId());
        Log.i("json",pNew.getServiceTypeCodeId());
        pNew.setCarwashServiceId(pOld.getCarwashServiceId());

        //Log.i("json",pNew.getCarwashServiceId());

        return pNew;
    }

    private void discountCodeValidate(Server server) {
        discountCode = StringUtility.numberFa2En(et_discountCode.getMyText());
        if (discountCode.length() == 0) {
            Toast.makeText(getContext(), "لطفا کد تخفیف خود را وارد کنید", Toast.LENGTH_SHORT).show();
        } else {
            TV_message.setVisibility(View.GONE);
            relativeLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            btn_submit.setText("لطفا منتظر بمانید");
            btn_submit.setEnabled(false);

            sendDiscountCode(server,discountCode);


        }
    }

    private void sendDiscountCode(Server server, String discountCode) {
        server.discountCodeValidation(discountCode, (e, result) -> {
            progressBar.setVisibility(View.INVISIBLE);
            btn_submit.setText("اعمال کد تخفیف");
            TV_message.setVisibility(View.VISIBLE);
            btn_submit.setEnabled(true);

            if (result != null) {
                if (result.getHeaders().code() == 401) {
                    getToken(2, null, discountCode);
                } else if (result.getHeaders().code() == 200) {
                    if (result.getResult().getStatus().getStatusCode() == 16) {
                        getToken(2, null, discountCode);
                    } else if (result.getResult().getStatus().isSuccess()) {
                        show.dismiss();
                        hideKeyboard();
                        //Toast.makeText(getContext(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        TV_message.setText(result.getResult().getStatus().getMessage());
                        takhfif.setText(StringUtility.numberEn2Fa(String.valueOf(result.getResult().getResult().getAmount()) + " تومان "));
                        Integer finalAmount = Integer.valueOf(postOrderObject.getNetAmount()) - result.getResult().getResult().getAmount();
                        finalPrice.setText(StringUtility.numberEn2Fa(String.valueOf(finalAmount) + " تومان "));
                        layout_discount.setVisibility(View.VISIBLE);
                        layout_payment_price.setVisibility(View.VISIBLE);
                        postOrderObject.setDiscountCodeId(result.getResult().getResult().getDiscountCodeId());
                        postOrderObject.setFinalAmunt(StringUtility.numberFa2En(String.valueOf(finalAmount)));
                    } else {
                        Toast.makeText(getContext(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_LONG).show();
            }

        });
    }

    void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_discount, null);
        builder.setView(rootView);
        builder.create();
        show = builder.show();

        //dialog = new Dialog(getContext());
        //dialog.setCancelable(true);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setContentView(R.layout.dialog_discount);

        progressBar = rootView.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);

        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        IranSansTextView confirmBtn = rootView.findViewById(R.id.btn_submit);
        TV_message = rootView.findViewById(R.id.TV_message);
        et_discountCode = rootView.findViewById(R.id.et_discountCode);
        relativeLayout = rootView.findViewById(R.id.RelativeLayout);

//
        confirmBtn.setOnClickListener(view -> {

            discountCodeValidate(server);


        });
    }

    public void getToken(int i,PostOrderNew postOrderNew,String discountCode){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getActivity()), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(getActivity(), result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(getActivity(), sharedPreference.getLoginTokenValue(getActivity()));
                        if (i == 1){
                            postOrder(server1,postOrderNew);
                        }
                        if (i == 2){
                            sendDiscountCode(server1,discountCode);
                        }

                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //isSendRequest = false;
                }
            }
        });
    }

}
