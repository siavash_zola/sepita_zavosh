
package hoodadtech.com.sepita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {

    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("serviceTitle")
    @Expose
    private String serviceTitle;
    @SerializedName("orderCode")
    @Expose
    private String orderCode;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("statusTitle")
    @Expose
    private String statusTitle;

    @SerializedName("statusCode")
    @Expose
    private String statusCode;

    @SerializedName("statusColor")
    @Expose
    private String statusColor;

    @SerializedName("orderDate")
    @Expose
    private String orderDate;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("duration")
    @Expose
    private String duration;

    @SerializedName("cityArea")
    @Expose
    private String cityArea;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(String statusColor) {
        this.statusColor = statusColor;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
