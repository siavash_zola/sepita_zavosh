package hoodadtech.com.sepita.adapter.listItems;

import android.support.v7.widget.RecyclerView;

import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.viewHolders.DateItemHolder;
import hoodadtech.com.sepita.model.HomeItem;

/**
 * Created by Milad on 1/6/2018.
 */

public class DateListItem implements MultiAdapterItem {
    HomeItem item;
    DateItemHolder itemHolder;
    @Override
    public int getTypeId() {
        return 2;
    }

    @Override
    public void setup(RecyclerView.ViewHolder holder ) {
        itemHolder= (DateItemHolder) holder;
        if(itemHolder!=null){
            itemHolder.title.setText(item.getTitle());
        }

    }

    public void setItem(HomeItem item) {
        this.item = item;
    }

    @Override
    public Object getItem() {
        return item;
    }
}
