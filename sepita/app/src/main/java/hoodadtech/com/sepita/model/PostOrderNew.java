package hoodadtech.com.sepita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostOrderNew {
    @SerializedName("serviceId")
    @Expose
    private String serviceId;
    @SerializedName("visitDate")
    @Expose
    private String visitDate;
    @SerializedName("visitTime")
    @Expose
    private String visitTime;
    @SerializedName("employeeGender")
    @Expose
    private String employeeGender;
    @SerializedName("netAmount")
    @Expose
    private String netAmount;
    @SerializedName("finalAmunt")
    @Expose
    private String finalAmunt;
    @SerializedName("serviceDetails")
    @Expose
    private List<ServiceDetails> serviceDetails = null;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("number")
    @Expose
    private Integer number;
    @SerializedName("discountCodeId")
    @Expose
    private String discountCodeId;
    @SerializedName("serviceTypeCodeId")
    @Expose
    private String serviceTypeCodeId;
    @SerializedName("addressId")
    @Expose
    private String addressId;
    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("carwashServiceId")
    @Expose
    private String carwashServiceId;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(String visitTime) {
        this.visitTime = visitTime;
    }

    public String getEmployeeGender() {
        return employeeGender;
    }

    public void setEmployeeGender(String employeeGender) {
        this.employeeGender = employeeGender;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getFinalAmunt() {
        return finalAmunt;
    }

    public void setFinalAmunt(String finalAmunt) {
        this.finalAmunt = finalAmunt;
    }

    public List<ServiceDetails> getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(List<ServiceDetails> serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getDiscountCodeId() {
        return discountCodeId;
    }

    public void setDiscountCodeId(String discountCodeId) {
        this.discountCodeId = discountCodeId;
    }

    public String getServiceTypeCodeId() {
        return serviceTypeCodeId;
    }

    public void setServiceTypeCodeId(String serviceTypeCodeId) {
        this.serviceTypeCodeId = serviceTypeCodeId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCarwashServiceId() {
        return carwashServiceId;
    }

    public void setCarwashServiceId(String carwashServiceId) {
        this.carwashServiceId = carwashServiceId;
    }

    @Override
    public String toString() {
        return "PostOrderNew{" +
                "serviceId='" + serviceId + '\'' +
                ", visitDate='" + visitDate + '\'' +
                ", visitTime='" + visitTime + '\'' +
                ", employeeGender='" + employeeGender + '\'' +
                ", netAmount='" + netAmount + '\'' +
                ", finalAmunt='" + finalAmunt + '\'' +
                ", serviceDetails=" + serviceDetails +
                ", duration=" + duration +
                ", number=" + number +
                ", discountCodeId='" + discountCodeId + '\'' +
                ", serviceTypeCodeId='" + serviceTypeCodeId + '\'' +
                ", addressId='" + addressId + '\'' +
                ", description='" + description + '\'' +
                ", carwashServiceId='" + carwashServiceId + '\'' +
                '}';
    }
}
