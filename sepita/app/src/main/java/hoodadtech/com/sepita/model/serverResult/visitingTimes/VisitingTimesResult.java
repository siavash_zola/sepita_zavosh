package hoodadtech.com.sepita.model.serverResult.visitingTimes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.VisitingDatesTimes;
import hoodadtech.com.sepita.model.serverResult.addAddress.Status;

public class VisitingTimesResult {
    @SerializedName("result")
    @Expose
    private VisitingDatesTimes result;
    @SerializedName("status")
    @Expose
    private Status status;

    public VisitingDatesTimes getResult() {
        return result;
    }

    public void setResult(VisitingDatesTimes result) {
        this.result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
