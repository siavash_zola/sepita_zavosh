package hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.CalculatePriceObject;
import hoodadtech.com.sepita.model.Date;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.ServiceDetails;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.CalculatePriceResult;
import hoodadtech.com.sepita.model.serverResult.DateAndTimeResult.DateAndTimeResult;
import hoodadtech.com.sepita.model.serverResult.GetPrice.GetPriceResult;
import hoodadtech.com.sepita.model.serverResult.GetPrice.GetPrice_sender;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.visitingTimes.VisitingTimesResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.FontIconTextView;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.ServiceType;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import io.blackbox_vision.wheelview.view.WheelView;

public class FrgSecondDetail extends Fragment implements View.OnClickListener {

    LinearLayout btn_submit,help;
    TextView first;
    LinearLayout  plus_one, minus_one;
    TextView time, price;
    //int firstCounter = 0;
    int Ttime = 0, baseDuration;
    int Pprice = 0,holderPprice=0, basePrice;
    String title;
    int increamentValue = 0;
    int minTimeValue = 0;
    String selectedDateTime = "";
    Spinner datespinner, timespinner;
    String vipText;
    int femalePercent, vipPercent;
    //Dialog dialog;
    String selectedPrice;
    IranSansTextView btn_date, btn_check_price;
    String selectedtime = null;
    int selectedTimePos = 3;
    IranSansTextView txt_male, txt_female, txt_male_female, TV_num_of_workers;
    ImageView img_male_female,img_male,img_female;
    LinearLayout layout_male, layout_female, layout_male_female;
    String selectedDatee = null;
    int SelectedDatePos = 3;
    LinearLayout date_layout,linearLayout_progressBar;
    int vipFinalPrice = 0;
    int femalPrice = 0;
    boolean isFemalSelected = false;
    View view;
    String gender = "notimportant";
    int maxHourAvailable = 10;
    ProgressBar loadDate;
    Switch switch_btn;
    List<Date> dateList;
    List<String> dateArrayList, timeArrayList;
    PostOrderObject postOrderObject;
    FileUtility fileUtility;
    boolean isFirstTime = true;
    boolean isVip = false;
    int num_of_workers = 1;
    String maxHourMessageText="";
    private AlertDialog show;
    private Server server;
    private int extraHourPrice;
    private int femalePrice;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDateANDTimes();
        //Log.i("kk","service id :"+getArguments().getString("id"));


    }

    private void getDateANDTimes() {
        Server server = Server.getInstance(getContext());
        dateArrayList = new ArrayList<>();
        timeArrayList = new ArrayList<>();
        dateList = new ArrayList<>();
        server.getVisitingTimesNewNewNew(getArguments().getString("id"), new FutureCallback<Response<DateAndTimeResult>>() {
            @Override
            public void onCompleted(Exception e, Response<DateAndTimeResult> result) {
                if (result.getHeaders().code() == 200){
                    if (result != null){
                        timeArrayList = result.getResult().getResult().getHoures();
                        dateList = result.getResult().getResult().getDates();
                        for (int i = 0; i < result.getResult().getResult().getDates().size(); i++) {
                            dateArrayList.add(result.getResult().getResult().getDates().get(i).getShowDate());

                            //meghdari ke saat taghsim baresh mishe va tedade hamyaro mige
                            maxHourAvailable = Integer.valueOf(result.getResult().getResult().getMaxHourAvailable());
                            //baraye har saat bishtar in meghdar be gheymat afzoode mishe
                            extraHourPrice = Integer.parseInt(result.getResult().getResult().getExtraHourPrice());
                            //age khanoom bekhad in gheymat afzoode mishe
                            femalePrice = Integer.valueOf(result.getResult().getResult().getFemalePrice());

                            maxHourMessageText=result.getResult().getResult().getMaxHourMessageText();


                            String serviceTypeCode = getArguments().getString("serviceTypeCode");
                            if (serviceTypeCode.equals(ServiceType.COMPLEX_code)){
                                getPrice(server);
                            }else {
                                int number = Ttime/maxHourAvailable;
                                int baghimande = Ttime % maxHourAvailable;
                                if (baghimande>0){
                                    number++;
                                }

                                TV_num_of_workers.setText(number+"");
                            }
                        }
                        date_layout.setVisibility(View.VISIBLE);
                        linearLayout_progressBar.setVisibility(View.GONE);
                    }
                }
            }
        });
    }

    private void getPrice(Server server) {
        GetPrice_sender sender = new GetPrice_sender();
        sender.setServiceDetails(postOrderObject.getServiceDetails());
        server.getPrice(sender, new FutureCallback<Response<GetPriceResult>>() {
            @Override
            public void onCompleted(Exception e, Response<GetPriceResult> result) {
                try {
                    if (result != null){
                        if (result.getHeaders().code() == 200){
                            if (result.getResult().getStatus().getIsSuccess()){
                                Pprice = Integer.parseInt(result.getResult().getResult().getPrice());
                                Ttime = Integer.parseInt(result.getResult().getResult().getDuration());
                                price.setText(Pprice+"");
                                time.setText(Ttime+"");

                                int number = Ttime/maxHourAvailable;
                                int baghimande = Ttime % maxHourAvailable;
                                if (baghimande>0){
                                    number++;
                                }

                                TV_num_of_workers.setText(number+"");
                                minTimeValue = Ttime;

                            }else {
                                Toast.makeText(getActivity(),result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(getActivity(),getString(R.string.error),Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(getActivity(),getString(R.string.checkConnection),Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e1){

                }

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null){
            //TextView titleTv = getActivity().findViewById(R.id.toolbar_text);
            //titleTv.setText(title);
            return view;
        }
        view = inflater.inflate(R.layout.frg_details_second, container, false);
        fileUtility = new FileUtility(getContext());
        postOrderObject = fileUtility.getPostOrderObject();
        postOrderObject.setEmployeeGender("notimportant");
        postOrderObject.setIsVip("false");
        postOrderObject.setNumber(num_of_workers);


        if (getArguments() != null) {
            basePrice = getArguments().getInt("price", 0);
            baseDuration = getArguments().getInt("time", 0);
            title= getArguments().getString("title","");
            Ttime = baseDuration;
            Pprice = basePrice;
            holderPprice = basePrice;
            minTimeValue = Ttime;

        }
        initView(view);
        return view;
    }

    private void initView(View view) {
        switch_btn = view.findViewById(R.id.switch_btn);
        help = view.findViewById(R.id.help);
        TV_num_of_workers = view.findViewById(R.id.num_of_workers);
        date_layout = view.findViewById(R.id.date_layout);
        linearLayout_progressBar = view.findViewById(R.id.linearLayout_progressBar);
        txt_female = view.findViewById(R.id.txt_femail);
        txt_male = view.findViewById(R.id.txt_male);
        txt_male_female = view.findViewById(R.id.txt_male_female);
        img_female = view.findViewById(R.id.img_female);
        img_male = view.findViewById(R.id.img_male);
        img_male_female = view.findViewById(R.id.img_male_female);
        layout_female = view.findViewById(R.id.layout_female);
        layout_male = view.findViewById(R.id.layout_male);
        layout_male_female = view.findViewById(R.id.layout_male_female);
        loadDate = view.findViewById(R.id.loadDate);
        loadDate.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);

        TV_num_of_workers.setText(StringUtility.numberEn2Fa(String.valueOf(num_of_workers)));
        layout_male_female.setOnClickListener(this);
        layout_male.setOnClickListener(this);
        layout_female.setOnClickListener(this);
        date_layout.setOnClickListener(this);


        btn_date = view.findViewById(R.id.btn_date);
        btn_check_price = view.findViewById(R.id.btn_check_price);
        btn_date.setOnClickListener(this);
        btn_check_price.setOnClickListener(this);
        IranSansTextView title = view.findViewById(R.id.header_title);
        title.setText("نظافت منزل");
        btn_submit = view.findViewById(R.id.btn_submit);
        time = view.findViewById(R.id.time);
        price = view.findViewById(R.id.price);
        time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
        price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
        btn_submit.setOnClickListener(this);
        first = view.findViewById(R.id.first);
        //first.setText(StringUtility.numberEn2Fa(String.valueOf(firstCounter)));
        plus_one = view.findViewById(R.id.plus_one);
        minus_one = view.findViewById(R.id.minus_one);
        plus_one.setOnClickListener(this);
        minus_one.setOnClickListener(this);
        help.setOnClickListener(this);

        first.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));


    }

    void showDialog() {
        //dialog = new Dialog(getContext());
        //dialog.setCancelable(false);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setContentView(R.layout.dialog_date_time);

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_date_time, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(rootView);
        builder.setCancelable(false);
        builder.create();
        show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final WheelView wheelView = rootView.findViewById(R.id.loop_view);
        final WheelView time_loop_view = rootView.findViewById(R.id.time_loop_view);
        wheelView.setInitialPosition(SelectedDatePos);
        wheelView.setIsLoopEnabled(false);


        wheelView.addOnLoopScrollListener((item, position) -> {
            selectedDatee = item.toString();
            SelectedDatePos = position;
            postOrderObject.setVisitDate(dateList.get(SelectedDatePos).getDate());
            selectedDateTime = selectedDatee + " " + selectedtime;

        });
        if (dateArrayList.size() > 0)
            wheelView.setItems(dateArrayList);

        time_loop_view.setInitialPosition(selectedTimePos);
        time_loop_view.setIsLoopEnabled(false);
        time_loop_view.addOnLoopScrollListener((item, position) -> {
            selectedtime = item.toString();
            selectedTimePos = position;
            postOrderObject.setVisitTime(StringUtility.numberFa2En(selectedtime));
            selectedDateTime = selectedDatee + " " + selectedtime;


        });
        if (timeArrayList.size() > 0)
            time_loop_view.setItems(timeArrayList);


        IransansButton cancelBtn = rootView.findViewById(R.id.canel_Btn);
        IransansButton confirmBtn = rootView.findViewById(R.id.confirmBtn);


//
        cancelBtn.setOnClickListener(view -> show.dismiss());

        confirmBtn.setOnClickListener(view -> {
            if (selectedDatee == null)
                selectedDatee = dateList.get(SelectedDatePos).getShowDate();
            if (selectedtime == null)
                selectedtime = timeArrayList.get(selectedTimePos);
            postOrderObject.setVisitDate(dateList.get(SelectedDatePos).getDate());
            postOrderObject.setVisitTime(StringUtility.numberFa2En(selectedtime));
            selectedDateTime = selectedDatee + " " + selectedtime;
            btn_date.setText(StringUtility.numberEn2Fa(selectedDateTime.toString()));
            show.dismiss();
        });

        //dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_female:
                img_female.setImageResource(R.drawable.female_blue);
                txt_female.setTextColor(getResources().getColor(R.color.app_blue1));

                img_male.setImageResource(R.drawable.male_icon);
                txt_male.setTextColor(getResources().getColor(R.color.text_color));

                img_male_female.setImageResource(R.drawable.female_male);
                txt_male_female.setTextColor(getResources().getColor(R.color.text_color));

                isFemalSelected = true;
                if (gender.equals("female")){

                }else {
                    int num = Integer.parseInt(TV_num_of_workers.getText().toString());
                    Pprice = Pprice + (num * femalePrice);
                }

                gender = "female";
                postOrderObject.setEmployeeGender(gender);
                price.setText(Pprice+"");
                break;
            case R.id.layout_male:

//                if (isFemalSelected) {
//                    Pprice = Pprice - incrementalPriceForFemale;
//                    price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
//
//                }

                img_male.setImageResource(R.drawable.male_icon_blue);
                txt_male.setTextColor(getResources().getColor(R.color.app_blue1));

                img_female.setImageResource(R.drawable.female);
                txt_female.setTextColor(getResources().getColor(R.color.text_color));

                img_male_female.setImageResource(R.drawable.female_male);
                txt_male_female.setTextColor(getResources().getColor(R.color.text_color));

                isFemalSelected = false;

                if (gender.equals("female")){
                    int num = Integer.parseInt(TV_num_of_workers.getText().toString());
                    Pprice = Pprice - (num * femalePrice);
                }

                gender = "male";
                postOrderObject.setEmployeeGender(gender);
                price.setText(Pprice+"");
                break;
            case R.id.layout_male_female:

//                if (isFemalSelected) {
//                    Pprice = Pprice - incrementalPriceForFemale;
//                    price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
//                }

                img_male_female.setImageResource(R.drawable.female_male_blue);
                txt_male_female.setTextColor(getResources().getColor(R.color.app_blue1));

                img_female.setImageResource(R.drawable.female);
                txt_female.setTextColor(getResources().getColor(R.color.text_color));

                img_male.setImageResource(R.drawable.male_icon);
                txt_male.setTextColor(getResources().getColor(R.color.text_color));

                isFemalSelected = false;

                if (gender.equals("female")){
                    int num = Integer.parseInt(TV_num_of_workers.getText().toString());
                    Pprice = Pprice - (num * femalePrice);
                }
                gender = "notimportant";
                postOrderObject.setEmployeeGender(gender);
                price.setText(Pprice+"");
                break;
            case R.id.date_layout:
                showDialog();
                break;
            case R.id.btn_date:
                showDialog();
                break;

            case R.id.btn_submit:
                if (selectedDatee == null || selectedtime == null) {
                    if (selectedDatee == null)
                        Toast.makeText(getContext(), getActivity().getString(R.string.selectDate), Toast.LENGTH_SHORT).show();

                    else if (selectedtime == null)
                        Toast.makeText(getContext(), "لطفا ساعت مورد نظر خود را مشخص کنید", Toast.LENGTH_SHORT).show();
                    return;

                }
                postOrderObject.setNetAmount(StringUtility.numberFa2En(String.valueOf(Pprice)));
                postOrderObject.setDuration(Integer.valueOf(StringUtility.numberFa2En(String.valueOf(Ttime))));
                postOrderObject.setNumber(Integer.parseInt(TV_num_of_workers.getText().toString()));
                Log.i("kk",TV_num_of_workers.getText().toString());
                fileUtility.writePostOrderObject(postOrderObject);
                FragmentHandler fragmentHandler = new FragmentHandler(getActivity(), getFragmentManager());
                fragmentHandler.loadFragment(new FrgAddress(), true);


                break;

            case R.id.btn_check_price:
                //showPriceCalculationDialog();
                break;

            case R.id.plus_one:
                //firstCounter = firstCounter + 1;
                Ttime = Ttime + 1;
                Pprice = Pprice + extraHourPrice;
                time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));


                int number = Ttime/maxHourAvailable;
                int baghimande = Ttime % maxHourAvailable;
                if (baghimande>0){
                    number++;
                }
                int oldNum = Integer.parseInt(TV_num_of_workers.getText().toString());
                if (oldNum<number){
                    if (gender.equals("female")){
                        Pprice += femalePrice;
                    }
                }
                TV_num_of_workers.setText(number+"");

                price.setText(Pprice+"");

                break;

            case R.id.minus_one:

                if (Ttime - 1 < minTimeValue)
                    return;
                else {

                    Ttime = Ttime - 1;
                    time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
                    Pprice = Pprice - extraHourPrice;

                    int num = Ttime/maxHourAvailable;
                    int baghi = Ttime % maxHourAvailable;
                    if (baghi>0){
                        num++;
                    }
                    int oldNumN = Integer.parseInt(TV_num_of_workers.getText().toString());
                    if (oldNumN>num){
                        if (gender.equals("female")){
                            Pprice -= femalePrice;
                        }
                    }
                    TV_num_of_workers.setText(num+"");
                    price.setText(Pprice+"");
                }

                break;
            case R.id.help:
                Toast.makeText(getActivity(),"Clicked",Toast.LENGTH_SHORT).show();
                break;

        }

    }



    public void getToken(CalculatePriceObject calculatePriceObject){
        server = Server.getInstance(getActivity());
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getActivity()), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(getActivity(), result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(getActivity(), sharedPreference.getLoginTokenValue(getActivity()));
                        //calculatePrice(server1,calculatePriceObject);
                    }else {
                        Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //isSendRequest = false;
                }
            }
        });
    }



}
