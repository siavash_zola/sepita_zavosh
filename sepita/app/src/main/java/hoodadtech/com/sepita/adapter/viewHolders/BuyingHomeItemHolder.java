package hoodadtech.com.sepita.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import hoodadtech.com.sepita.R;

/**
 * Created by Milad on 1/5/2018.
 */

public class BuyingHomeItemHolder extends RecyclerView.ViewHolder  {

    public TextView title;
    public CheckBox checkBox;
    public RelativeLayout layout_background;

    public BuyingHomeItemHolder(View itemView) {
        super(itemView);
        title=itemView.findViewById(R.id.TV_title);
        layout_background=itemView.findViewById(R.id.layout_background);
        checkBox=itemView.findViewById(R.id.checkbox);
    }
}
