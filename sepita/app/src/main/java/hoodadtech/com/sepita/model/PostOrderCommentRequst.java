
package hoodadtech.com.sepita.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostOrderCommentRequst {

    @SerializedName("orderCode")
    @Expose
    private String orderCode;
    @SerializedName("starNumber")
    @Expose
    private String starNumber;
    @SerializedName("commentItems")
    @Expose
    private List<String> commentItems = null;
    @SerializedName("commentDesc")
    @Expose
    private String commentDesc;

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getStarNumber() {
        return starNumber;
    }

    public void setStarNumber(String starNumber) {
        this.starNumber = starNumber;
    }

    public List<String> getCommentItems() {
        return commentItems;
    }

    public void setCommentItems(List<String> commentItems) {
        this.commentItems = commentItems;
    }

    public String getCommentDesc() {
        return commentDesc;
    }

    public void setCommentDesc(String commentDesc) {
        this.commentDesc = commentDesc;
    }

}
