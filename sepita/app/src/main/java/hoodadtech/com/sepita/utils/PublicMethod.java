package hoodadtech.com.sepita.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.NumberFormat;
import java.util.Locale;

public class PublicMethod {
    public String name_key = "name";
    public String image_key = "image";
    public String credit_key = "credit";
    public SharedPreferences prefs;
    public String MY_PREFS_NAME = "mySD";

    public PublicMethod(Context context) {
        prefs = context.getSharedPreferences(MY_PREFS_NAME, context.MODE_PRIVATE);
    }

    public static String convert(String price){
        String newPrice = price.trim();
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        String numberAsString = numberFormat.format(Integer.parseInt(newPrice));
        return numberAsString;
    }

    public void saveFullName(String fullName){
        prefs.edit().putString(name_key , fullName).apply();
    }

    public String getFullName(){
        return prefs.getString(name_key,"?");
    }

    public void saveImage(String image){
        prefs.edit().putString(image_key , image).apply();
    }

    public String getImage(){
        return prefs.getString(image_key,null);
    }

    public void saveCredit(String credit){
        prefs.edit().putString(credit_key , credit).apply();
    }

    public String getCredit(){
        return prefs.getString(credit_key,"00");
    }

}

