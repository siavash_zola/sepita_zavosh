
package hoodadtech.com.sepita.model.serverResult;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.Order;

public class OrderListResult {

    @SerializedName("result")
    @Expose
    private List<Order> result = null;

    public List<Order> getResult() {
        return result;
    }

    public void setResult(List<Order> result) {
        this.result = result;
    }

}
