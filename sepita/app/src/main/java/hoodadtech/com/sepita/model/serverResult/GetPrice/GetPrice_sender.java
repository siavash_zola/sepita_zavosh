package hoodadtech.com.sepita.model.serverResult.GetPrice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import hoodadtech.com.sepita.model.ServiceDetails;

public class GetPrice_sender {
    @SerializedName("serviceDetails")
    @Expose
    private List<ServiceDetails> serviceDetails = null;

    public List<ServiceDetails> getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(List<ServiceDetails> serviceDetails) {
        this.serviceDetails = serviceDetails;
    }
}
