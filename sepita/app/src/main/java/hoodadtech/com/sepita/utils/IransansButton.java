package hoodadtech.com.sepita.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Milad on 12/15/2017.
 */

public class IransansButton extends android.support.v7.widget.AppCompatButton {
    public IransansButton(Context context) {
        super(context);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "IRANSansMobile(FaNum)_Light.ttf");
        this.setTypeface(face);
    }

    public IransansButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "IRANSansMobile(FaNum)_Light.ttf");
        this.setTypeface(face);
    }

    public IransansButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "IRANSansMobile(FaNum)_Light.ttf");
        this.setTypeface(face);
    }
}
