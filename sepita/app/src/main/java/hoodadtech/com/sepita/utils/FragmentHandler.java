package hoodadtech.com.sepita.utils;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import hoodadtech.com.sepita.R;

/**
 * Created by Milad on 12/15/2017.
 */

public class FragmentHandler {

    Activity context;
    FragmentManager fragmentManager;

    public FragmentHandler(Activity context, FragmentManager fragmentManager) {
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    public void loadFragment(Fragment fragment, boolean addToBackStack) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            String frgTag = fragment.getClass().getSimpleName();
            fragmentTransaction.replace(R.id.fragment_container, fragment, frgTag);
            if (addToBackStack)
                fragmentTransaction.addToBackStack(frgTag);
            fragmentTransaction.commit();
        }catch (Exception e){}


    }

    public void loadFragment(Fragment fragment, boolean addToBackStack,Bundle bundle) {
        try {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            String frgTag = fragment.getClass().getSimpleName();
            if(bundle!=null)
                fragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.fragment_container, fragment, frgTag);
            if (addToBackStack)
                fragmentTransaction.addToBackStack(frgTag);

            fragmentTransaction.commit();
        }catch (Exception e){}


    }


    private boolean isExistFragment(String tag) {
        Fragment targetFragment = fragmentManager.findFragmentByTag(tag);
        return targetFragment != null;
    }
}
