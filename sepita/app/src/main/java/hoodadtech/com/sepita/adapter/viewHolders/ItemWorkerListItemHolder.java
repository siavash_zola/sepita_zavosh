package hoodadtech.com.sepita.adapter.viewHolders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.IranSansTextView;

/**
 * Created by Milad on 1/30/2018.
 */

public class ItemWorkerListItemHolder extends RecyclerView.ViewHolder {
    public IranSansTextView TV_assistanName, TV_assistanScore;
    public RelativeLayout layout_favorites;
    public LinearLayout layout_main;


    public ItemWorkerListItemHolder(View itemView) {
        super(itemView);
        TV_assistanName = itemView.findViewById(R.id.TV_assistanName);
        TV_assistanScore = itemView.findViewById(R.id.TV_assistanScore);
        layout_favorites = itemView.findViewById(R.id.layout_favorites);
        layout_main = itemView.findViewById(R.id.layout_main);


    }
}
