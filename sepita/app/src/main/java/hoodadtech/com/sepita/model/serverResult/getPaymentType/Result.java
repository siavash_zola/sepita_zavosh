package hoodadtech.com.sepita.model.serverResult.getPaymentType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import hoodadtech.com.sepita.model.Bank;

public class Result {
    @SerializedName("banks")
    @Expose
    private List<Bank> banks = null;
    @SerializedName("remainAmount")
    @Expose
    private String remainAmount;
    @SerializedName("paymentType")
    @Expose
    private List<PaymentType> paymentType = null;

    public List<Bank> getBanks() {
        return banks;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    public String getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(String remainAmount) {
        this.remainAmount = remainAmount;
    }

    public List<PaymentType> getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(List<PaymentType> paymentType) {
        this.paymentType = paymentType;
    }

}
