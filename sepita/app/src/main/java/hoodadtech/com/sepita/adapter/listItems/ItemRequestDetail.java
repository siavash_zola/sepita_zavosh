package hoodadtech.com.sepita.adapter.listItems;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.viewHolders.DetailItemHolder;
import hoodadtech.com.sepita.adapter.viewHolders.ItemRequestDetailHolder;
import hoodadtech.com.sepita.model.ServiceDetail;
import hoodadtech.com.sepita.model.ServiceDetail2;
import hoodadtech.com.sepita.utils.StringUtility;

/**
 * Created by Milad on 1/30/2018.
 */

public class ItemRequestDetail implements MultiAdapterItem {
    ItemRequestDetailHolder itemHolder;
    ServiceDetail2 item;
    @Override
    public int getTypeId() {
        return 5;
    }

    @Override
    public void setup(RecyclerView.ViewHolder holder ) {
        itemHolder= (ItemRequestDetailHolder) holder;
        if(itemHolder!=null) {


            itemHolder.txt_title.setText(StringUtility.numberEn2Fa(item.getTitle() +(item.getQuantity()!=null?" : "+item.getQuantity():"")
                    +" " +(item.getUnit()!=null?item.getUnit():"")));
        }


    }

    public void setItem(ServiceDetail2 item) {
        this.item = item;
    }

    @Override
    public Object getItem() {
        return item;
    }



}
