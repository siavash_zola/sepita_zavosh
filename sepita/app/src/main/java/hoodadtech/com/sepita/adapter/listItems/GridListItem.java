package hoodadtech.com.sepita.adapter.listItems;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.viewHolders.GridItemHolder;
import hoodadtech.com.sepita.model.HomeItem;

/**
 * Created by Milad on d1/5/2018.
 */

public class GridListItem implements MultiAdapterItem {
    HomeItem item;
    GridItemHolder itemHolder;
    @Override
    public int getTypeId() {
        return 1;
    }

    @Override
    public void setup(RecyclerView.ViewHolder holder ) {
        itemHolder= (GridItemHolder) holder;
        if (itemHolder!=null){
            itemHolder.title.setText(item.getTitle());

            //switch (item.getId()){
            //    case 11:
            //        itemHolder.imageView.setImageResource(R.drawable.d11);
            //        break;
            //    case 22:
            //        itemHolder.imageView.setImageResource(R.drawable.d22);
            //        break;
            //    case 33:
            //        itemHolder.imageView.setImageResource(R.drawable.d33);
            //        break;
            //    case 44:
            //        itemHolder.imageView.setImageResource(R.drawable.d44);
            //        break;
            //    case 55:
            //        itemHolder.imageView.setImageResource(R.drawable.d55);
            //        break;
            //}
            itemHolder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(itemClickListener!=null)
                        itemClickListener.onClick(item,view);

                }
            });
        }



    }

    public void setItem(HomeItem item) {
        this.item = item;
    }

    @Override
    public Object getItem() {
        return item;
    }

    ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener{
        void onClick(HomeItem item,View view);

    }
}
