package hoodadtech.com.sepita.view.fragment.entrancefragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.Date;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.IransansEditText;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;

public class FrgRegister extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    IransansButton btn_signup;
    IranSansTextView btn_login;
    IransansEditText ET_fullName, ET_phoneNumber, ET_reagentCode;
    String fullName, phoneNumber;
    RelativeLayout mainLayout;
    ProgressBar progressBar;
    ImageView IV_bg;


    public FrgRegister() {

    }

    public static FrgRegister newInstance(String param1, String param2) {
        FrgRegister fragment = new FrgRegister();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_register, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });
        btn_login = view.findViewById(R.id.btn_login);
        ET_fullName = view.findViewById(R.id.input_name);
        ET_phoneNumber = view.findViewById(R.id.input_phoneNumber);
        ET_reagentCode = view.findViewById(R.id.reagentCode);
        mainLayout = view.findViewById(R.id.mainLayout);
        btn_signup = view.findViewById(R.id.btn_signup);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.gray_light0), android.graphics.PorterDuff.Mode.SRC_ATOP);

        btn_signup.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        mainLayout.setOnClickListener(this);
        IV_bg = view.findViewById(R.id.IV_bg);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getActivity().getWindowManager().getDefaultDisplay().getWidth(),
                getActivity().getWindowManager().getDefaultDisplay().getHeight());
        IV_bg.setLayoutParams(layoutParams);
    }


    @Override
    public void onClick(View view) {
        FragmentHandler fragmentHandler = new FragmentHandler(getActivity(), getFragmentManager());
        switch (view.getId()) {
            case R.id.btn_signup:
                hideKeyboard();
                registerUser(fragmentHandler);
                break;
            case R.id.btn_login:
                hideKeyboard();
                //fragmentHandler.loadFragment(new FrgLogin(), true);
                getActivity().onBackPressed();
                break;
            case R.id.mainLayout:
                hideKeyboard();
                break;
        }


    }

    private void registerUser(FragmentHandler fragmentHandler) {

        fullName = ET_fullName.getMyText();
        phoneNumber = ET_phoneNumber.getMyText();
        if (fullName.length() == 0) {
            Toast.makeText(getContext(), "لطفا نام کاربری خود را وارد کنید", Toast.LENGTH_SHORT).show();
        } else if (phoneNumber.length() == 0) {
            Toast.makeText(getContext(), "لطفا شماره تلفن همراه خود را وارد کنید", Toast.LENGTH_SHORT).show();
        } else if (!StringUtility.isValidPhoneNumber(phoneNumber)) {
            Toast.makeText(getContext(), "شماره وارد شده معتبر نمی باشد", Toast.LENGTH_SHORT).show();
        } else {
            String reagentCode = ET_reagentCode.getMyText();
            if (reagentCode.matches("")){
                reagentCode = "";
            }
            progressBar.setVisibility(View.VISIBLE);
            btn_signup.setText("لطفا منتظر بمانید");
            btn_signup.setEnabled(false);
            btn_login.setEnabled(false);
            Server server = Server.getInstance(getContext());

            server.accountRegisterUser(fullName, StringUtility.numberFa2En(phoneNumber), reagentCode, new FutureCallback<hoodadtech.com.sepita.model.serverResult.registerResult.RegisterResult>() {
                @Override
                public void onCompleted(Exception e, hoodadtech.com.sepita.model.serverResult.registerResult.RegisterResult result) {
                    progressBar.setVisibility(View.INVISIBLE);
                    btn_signup.setText("دریافت کد فعالسازی");
                    btn_signup.setEnabled(true);
                    btn_login.setEnabled(true);
                    if (result != null){
                        if (result.getStatus() != null) {
                            if (result.getStatus().getIsSuccess()) {
                                SharedPreference sharedPreference = new SharedPreference();
                                UserObject userObject = new UserObject();
                                userObject.setFullName(fullName);
                                userObject.setCellNumber(StringUtility.numberFa2En(phoneNumber));
                                sharedPreference.saveUserObject(getContext(), new Gson().toJson(userObject));
                                sharedPreference.saveTempLoginToken(getContext(), result.getResult().getTokenId());
                                Bundle bundle = new Bundle();
                                bundle.putString("activationCode", result.getResult().getUserCode() + "");
                                Toast.makeText(getContext(), result.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                                fragmentHandler.loadFragment(new FrgActivation(), true, bundle);
                            } else {
                                Toast.makeText(getActivity(), result.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(getContext(), getActivity().getString(R.string.error), Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(getContext(), getActivity().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

}
