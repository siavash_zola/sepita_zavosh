package hoodadtech.com.sepita.adapter.listItems;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.koushikdutta.ion.Ion;

import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.viewHolders.DetailItemHolder;
import hoodadtech.com.sepita.model.ServiceDetail;
import hoodadtech.com.sepita.utils.StringUtility;

/**
 * Created by Milad on 1/30/2018.
 */

public class DetailsItem implements MultiAdapterItem {
    DetailItemHolder itemHolder;
    ServiceDetail item;
    @Override
    public int getTypeId() {
        return 3;
    }

    @Override
    public void setup(RecyclerView.ViewHolder holder ) {
        itemHolder= (DetailItemHolder) holder;
        if(itemHolder!=null){
            Ion.with(itemHolder.icon.getContext()).load(item.getIconUrl()).intoImageView(itemHolder.icon);
            itemHolder.TV_title.setText(item.getTitle());
            itemHolder.TV_value.setText(StringUtility.numberEn2Fa(String.valueOf(item.getSeedValue())));
            itemHolder.minus_one.setOnClickListener(view -> {
                if(itemClickListener!=null){
                    int numberofunit=Integer.valueOf(itemHolder.TV_value.getText().toString())-item.getIncreamentValue();
                    Log.i("kk",numberofunit+"");
                    if(numberofunit<item.getMinValue()){
                        return;
                    }
                    int priceofunit=item.getAmount();
                    int timeofunit=item.getDuration();
                    itemHolder.TV_value.setText(StringUtility.numberEn2Fa(String.valueOf(numberofunit)));
                    itemClickListener.onClick(item,view,timeofunit,priceofunit,numberofunit);

                    Log.i("kk",item.getUnit()+"");
                    Log.i("kk",item.getIncreamentValue()+"");
                    Log.i("kk",item.getSeedValue()+"");


                }
            });
            itemHolder.plus_one.setOnClickListener(view -> {
                if(itemClickListener!=null){
                    int numberofunit=Integer.valueOf(itemHolder.TV_value.getText().toString())+item.getIncreamentValue();
                    int priceofunit=item.getAmount();
                    int timeofunit=item.getDuration();
                    itemHolder.TV_value.setText(StringUtility.numberEn2Fa(String.valueOf(numberofunit)));
                    itemClickListener.onClick(item,view,timeofunit,priceofunit,numberofunit);
                }
            });

        }


    }

    public void setItem(ServiceDetail item) {
        this.item = item;
    }

    @Override
    public Object getItem() {
        return item;
    }

    private ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener{
        void onClick(ServiceDetail item,View view,int time,int price,int quantity);

    }
}
