package hoodadtech.com.sepita.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.AddressNew;
import hoodadtech.com.sepita.utils.AddressManager;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.OnItemLatLonClickListener;
import hoodadtech.com.sepita.utils.StringUtility;

public class AddressAdapter extends RecyclerView.Adapter {
    private List<AddressNew> list;
    private Activity activity;
    private final int TYPE_HEADER = 0;
    private final int TYPE_FOOTER = 1;
    private final int TYPE_ITEM = 2;
    public AddressManager addressManager;

    public OnItemLatLonClickListener listener;

    public AddressAdapter(List<AddressNew> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address, parent, false);
            return new ItemViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            AddressNew addressNew = list.get(position);
            itemViewHolder.plaqueTextView.setText(StringUtility.numberEn2Fa(String.valueOf(addressNew.getPlaque())));
            itemViewHolder.unitTextView.setText(StringUtility.numberEn2Fa(String.valueOf(addressNew.getUnit())));
            itemViewHolder.addressTextView.setText(addressNew.getAddress());
            itemViewHolder.phoneTextView.setText(StringUtility.numberEn2Fa(String.valueOf(addressNew.getPhone())));
            itemViewHolder.city_neighborhood.setText(addressNew.getCity() + "-" + addressNew.getNeighborhood());

            if (addressNew.isSelected()) {
                itemViewHolder.selector.setBackgroundResource(R.drawable.bg_button_blue);
            } else {
                itemViewHolder.selector.setBackgroundResource(R.drawable.bg_button_gray);
            }
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        IranSansTextView unitTextView,plaqueTextView,city_neighborhood,addressTextView,phoneTextView;
        LinearLayout selector,linearAddress,delete,edit;
        public ItemViewHolder(View itemView) {
            super(itemView);
            unitTextView = itemView.findViewById(R.id.unitTextView);
            plaqueTextView = itemView.findViewById(R.id.plaqueTextView);
            city_neighborhood = itemView.findViewById(R.id.city_neighborhood);
            addressTextView = itemView.findViewById(R.id.addressTextView);
            phoneTextView = itemView.findViewById(R.id.phoneTextView);
            selector = itemView.findViewById(R.id.selector);
            linearAddress = itemView.findViewById(R.id.linearAddress);
            delete = itemView.findViewById(R.id.delete);
            edit = itemView.findViewById(R.id.edit);



            linearAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.itemLatLonClicked(getAdapterPosition());
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addressManager.delete(list.get(getAdapterPosition()));
                }
            });
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addressManager.edit(list.get(getAdapterPosition()));
                }
            });

        }
    }
}
