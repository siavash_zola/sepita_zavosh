package hoodadtech.com.sepita.adapter;

import android.support.v7.widget.RecyclerView;

/**
 * Created by sajad_000 on 2/27/2016.
 */
public interface MultiAdapterItem {
    int getTypeId();

    void setup(RecyclerView.ViewHolder holder);

    Object getItem();
}
