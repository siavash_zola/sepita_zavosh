
package hoodadtech.com.sepita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import hoodadtech.com.sepita.model.serverResult.serviceDetailResult.ServiceDetailsInfo;

public class ServiceDetail {


    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("seedValue")
    @Expose
    private Integer seedValue;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("increamentValue")
    @Expose
    private Integer increamentValue;
    @SerializedName("minValue")
    @Expose
    private Integer minValue;

    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;

    @SerializedName("increamentValueLimmit")
    @Expose
    private int increamentValueLimmit;
    @SerializedName("serviceDetailInfo")
    @Expose
    private List<ServiceDetailInfoItem> serviceDetailInfo = null;

    boolean selected;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getSeedValue() {
        return seedValue;
    }

    public void setSeedValue(Integer seedValue) {
        this.seedValue = seedValue;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getIncreamentValue() {
        return increamentValue;
    }

    public void setIncreamentValue(Integer increamentValue) {
        this.increamentValue = increamentValue;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Integer getMinValue() {
        return minValue;
    }

    public void setMinValue(Integer minValue) {
        this.minValue = minValue;
    }

    public int getIncreamentValueLimmit() {
        return increamentValueLimmit;
    }

    public void setIncreamentValueLimmit(int increamentValueLimmit) {
        this.increamentValueLimmit = increamentValueLimmit;
    }

    public List<ServiceDetailInfoItem> getServiceDetailInfo() {
        return serviceDetailInfo;
    }

    public void setServiceDetailInfo(List<ServiceDetailInfoItem> serviceDetailInfo) {
        this.serviceDetailInfo = serviceDetailInfo;
    }
}
