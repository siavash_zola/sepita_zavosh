package hoodadtech.com.sepita.adapter.listItems;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.viewHolders.ItemCommentAssistHolder;
import hoodadtech.com.sepita.adapter.viewHolders.ItemCommentEmplyoeeHolder;
import hoodadtech.com.sepita.model.Reason;
import hoodadtech.com.sepita.model.serverResult.EmployeeCommentItem;
import hoodadtech.com.sepita.utils.StringUtility;

/**
 * Created by Milad on 1/30/2018.
 */

public class ItemCommentEmployee implements MultiAdapterItem {
    ItemCommentEmplyoeeHolder itemHolder;
    EmployeeCommentItem item;

    @Override
    public int getTypeId() {
        return 7;
    }

    @Override
    public void setup(RecyclerView.ViewHolder holder ) {
        itemHolder = (ItemCommentEmplyoeeHolder) holder;
        if (itemHolder != null) {
            itemHolder.TV_Name.setText(item.getFullName());
            itemHolder.TV_comment.setText(item.getCommentBody());
            if (item.getPreviousDays().equals("0"))
                itemHolder.TV_days_ago.setText(StringUtility.numberEn2Fa(itemHolder.TV_days_ago.getResources().getString(R.string.today)));
            else
                itemHolder.TV_days_ago.setText(StringUtility.numberEn2Fa(item.getPreviousDays() + " " + itemHolder.TV_days_ago.getResources().getString(R.string.daysago)));
        }

    }

    public void setItem(EmployeeCommentItem item) {
        this.item = item;
    }

    @Override
    public EmployeeCommentItem getItem() {
        return item;
    }


    ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onClick(Reason item, View view);

    }


}
