package hoodadtech.com.sepita.view.fragment.entrancefragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.IransansEditText;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import hoodadtech.com.sepita.view.activity.HomeActivity;

public class FrgActivation extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    IransansButton btn_signUp;
    IranSansTextView btn_login, TV_timer, btn_resend_code;
    RelativeLayout mainLayout;
    ImageView IV_bg;
    String osVersion, deviceId, deviceModel;
    final String osType = "Android";
    ProgressBar progressBar;


    IransansEditText input_activationCode;
    String activationCode;
    LinearLayout layout_resendCode;


    CountDownTimer countDownTimer;


    public FrgActivation() {

    }

    public static FrgActivation newInstance(String param1, String param2) {
        FrgActivation fragment = new FrgActivation();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_activation, container, false);

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(() -> {

        });


        btn_login = view.findViewById(R.id.btn_login);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);

        layout_resendCode = view.findViewById(R.id.layout_resendCode);
        TV_timer = view.findViewById(R.id.TV_timer);
        input_activationCode = view.findViewById(R.id.input_activationCode);
        btn_signUp = view.findViewById(R.id.btn_signup);
        btn_resend_code = view.findViewById(R.id.btn_resend_code);
        mainLayout = view.findViewById(R.id.mainLayout);

//        activationCode = getArguments().getString("activationCode");
//        input_activationCode.setText(StringUtility.numberEn2Fa(activationCode));
        IV_bg = view.findViewById(R.id.IV_bg);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getActivity().getWindowManager().getDefaultDisplay().getWidth(),
                getActivity().getWindowManager().getDefaultDisplay().getHeight());
        IV_bg.setLayoutParams(layoutParams);

        btn_login.setOnClickListener(this);
        btn_signUp.setOnClickListener(this);
        mainLayout.setOnClickListener(this);
        btn_resend_code.setOnClickListener(this);

        countDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {

                String text = String.format("%02d : %02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                        , (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));


                TV_timer.setText(StringUtility.numberEn2Fa(text));
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                layout_resendCode.setVisibility(View.VISIBLE);
                TV_timer.setVisibility(View.GONE);

            }

        }.start();

        return view;
    }

    @Override
    public void onClick(View view) {
        FragmentHandler fragmentHandler = new FragmentHandler(getActivity(), getFragmentManager());
        switch (view.getId()) {
            case R.id.btn_signup:
//               / Intent intent=new Intent(getActivity(), MapsActivity.class);
//                startActivity(intent);
                hideKeyboard();
                activateUser(fragmentHandler);

                break;
            case R.id.btn_login:
                hideKeyboard();
                if (countDownTimer != null)
                    countDownTimer.cancel();
                fragmentHandler.loadFragment(new FrgLogin(), true);
                break;
            case R.id.mainLayout:
                hideKeyboard();
                break;

            case R.id.btn_resend_code:
                resendCode();
                break;
        }


    }

    private void resendCode() {
        Server server = Server.getInstance(getContext());
        layout_resendCode.setVisibility(View.INVISIBLE);
        TV_timer.setVisibility(View.VISIBLE);
        countDownTimer.start();
        server.accountResendActivationCode(new SharedPreference().getTempLoginTokenValue(getContext()), (e, result) -> {
            if (OperationResult.isOk(result, e)) {

                new SharedPreference().saveTempLoginToken(getContext(), result.getResult().getTokenId());


            } else {
                Toast.makeText(getContext(), "لطفا وضعیت اینترنت خود را بررسی کنید", Toast.LENGTH_SHORT).show();
                if (countDownTimer != null)
                    countDownTimer.cancel();
                layout_resendCode.setVisibility(View.VISIBLE);
                TV_timer.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @SuppressLint("HardwareIds")
    private void activateUser(FragmentHandler fragmentHandler) {
          activationCode=input_activationCode.getMyText();

        if (activationCode.length() == 0) {
            Toast.makeText(getContext(), "لطفا کد دریافت شده را وارد کنید", Toast.LENGTH_SHORT).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);
            btn_signUp.setText("لطفا منتظر بمانید");
            btn_signUp.setEnabled(false);
            btn_login.setEnabled(false);
            Server server = Server.getInstance(getContext());
            deviceId = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
            deviceModel = server.GetDeviceString();
            osVersion = String.valueOf(android.os.Build.VERSION.SDK_INT);
            server.accountActivateUser(new SharedPreference().getTempLoginTokenValue(getContext()),
                    StringUtility.numberFa2En(activationCode), deviceId, deviceModel, osType, osVersion, (e, result) -> {
                        progressBar.setVisibility(View.INVISIBLE);
                        btn_signUp.setText("ثبت نام");
                        btn_signUp.setEnabled(true);
                        btn_login.setEnabled(true);
                        if (isAdded()) {
                            if (OperationResult.isOk(result, e)) {
                                if (result.getStatus().isSuccess()) {
                                    SharedPreference sharedPreference = new SharedPreference();
                                    sharedPreference.saveLoginToken(getContext(), result.getResult().getTokenId());
                                    UserObject userObject = new Gson().fromJson(sharedPreference.getUserObject(getContext()), UserObject.class);
                                    userObject.setActivationCode(activationCode);
                                    sharedPreference.saveUserObject(getContext(), new Gson().toJson(userObject));
                                    sharedPreference.saveUserObject(getContext(), new Gson().toJson(userObject));
                                    if (countDownTimer != null)
                                        countDownTimer.cancel();
                                    startActivity(new Intent(this.getContext(), HomeActivity.class));
                                    getActivity().finish();
                                    Toast.makeText(getContext(), result.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(getContext(), result.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else {
                                Toast.makeText(getContext(), "لطفا وضعیت اینترنت خود را بررسی کنید", Toast.LENGTH_SHORT).show();
                            }
                        }


                    });
        }


    }


    void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }


}
