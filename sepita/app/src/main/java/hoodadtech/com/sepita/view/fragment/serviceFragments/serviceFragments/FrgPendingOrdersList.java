package hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.MultiRecyclerAdapter;
import hoodadtech.com.sepita.adapter.listItems.OrderListItem;
import hoodadtech.com.sepita.model.serverResult.HomeResult;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.Order;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.view.activity.DetailsActivity;
import hoodadtech.com.sepita.view.activity.OrderDetailsActivity;

public class FrgPendingOrdersList extends Fragment implements OrderListItem.ItemClickListener,SwipeRefreshLayout.OnRefreshListener {

    Server server;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    private MultiRecyclerAdapter multiRecyclerAdapter;
    private List<MultiAdapterItem> homeItems;
    HomeResult homeResult;
    ProgressBar progressBar;
    private SwipeRefreshLayout swipeContainer;
    View view;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view!=null)
            return view;

         view = inflater.inflate(R.layout.frg_home, container, false);
        ((DetailsActivity)getActivity()).updateDrawerFragmentUi();

        server=Server.getInstance(getContext());
        view.setFocusableInTouchMode(true);
        view.requestFocus();

        view.setOnKeyListener((v, keyCode, event) -> {

            if( keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
                getActivity().finish();
                return true;
            }
            return false;
        });

        initView(view);
        return view;
    }

    private void initView(View view) {
        //TextView title=getActivity().findViewById(R.id.toolbar_text);
        //title.setText("لیست درخواست ها");
        swipeContainer = view.findViewById(R.id.swiperefresh);
        swipeContainer.setOnRefreshListener(this);
        swipeContainer.setColorSchemeResources(R.color.app_blue1);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);
        recyclerView = view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getContext());
        multiRecyclerAdapter = new MultiRecyclerAdapter(getContext());
        recyclerView.setAdapter(multiRecyclerAdapter);
        recyclerView.setLayoutManager(layoutManager);
        setData(false);
    }

    private void setData(boolean isRefresh) {
        if(!isRefresh){
            progressBar.setVisibility(View.VISIBLE);
        }
        Server server=Server.getInstance(getContext());
        server.getOrderList("0", (e, result) -> {
            progressBar.setVisibility(View.GONE);
            swipeContainer.setRefreshing(false);
            if(e!=null){
                CustomSnackbar.showSnackBar(view);
            }
            else if(OperationResult.isOk(result,e)){
                homeItems=new ArrayList<>();
                for(int i=0;i<result.getResult().size();i++){
                    OrderListItem orderListItem=new OrderListItem();
                    orderListItem.setItem(result.getResult().get(i));
                    orderListItem.setItemClickListener(this);
                    homeItems.add(orderListItem);
                }
                multiRecyclerAdapter.setItems(homeItems);
            }
        });

//        progressBar.setVisibility(View.VISIBLE);
//        Server server = Server.getInstance(getContext());
//        server.getServices((e, result) -> {
//            progressBar.setVisibility(View.GONE);
//            homeItems = new ArrayList<>();
//            if (OperationResult.isOk(result, e)) {
//
//                List<Service> hhomeItems = result.getResult();
//                for (int i = 0; i < hhomeItems.size(); i++) {
//                    VerticalListItem listItem = new VerticalListItem();
//                    listItem.setItem(hhomeItems.get(i));
//                    listItem.setItemClickListener(this);
//                    homeItems.add(listItem);
//                }
//                multiRecyclerAdapter.setItems(homeItems);
//            } else {
//                Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();
//
//            }
//        });
    }





    @Override
    public void onClick(Order item, View view) {
        switch (view.getId()){
            case R.id.Icon_cancel:
                if(item.getStatusCode().equals("3")||item.getStatusCode().equals("4")){
                    Toast.makeText(getActivity(),"امکان لغو این درخواست وجود ندارد",Toast.LENGTH_SHORT).show();
                    return;
                }
                showDialog(item);

                break;
            case R.id.layout_details:
                Intent intent = new Intent(getActivity(), OrderDetailsActivity.class);
                intent.putExtra("id",item.getOrderId());
                intent.putExtra("status",item.getStatusCode());
                getActivity().startActivity(intent);
                break;
        }

    }

    private void cancelOrder(Order item) {
        server.cancelOrder(item.getOrderId(), (e, result) -> {
            if(e!=null){
                CustomSnackbar.showSnackBar(view);
            }
            else if(result!=null&&result.getStatus()!=null&&result.getStatus().isSuccess()){
                multiRecyclerAdapter.delete(multiRecyclerAdapter.getHomeItems().get(0));
                multiRecyclerAdapter.notifyDataSetChanged();
                Toast.makeText(getContext(),result.getStatus().getMessage(),Toast.LENGTH_SHORT).show();

            }
            else if(result!=null&&result.getStatus()!=null) {
                Toast.makeText(getContext(),result.getStatus().getMessage(),Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getContext(),"وضعیت ارتباط خود را بررسی کنید",Toast.LENGTH_SHORT).show();
            }
        });
    }

    void showDialog(Order item) {
        Dialog dialog;
        dialog = new Dialog(getContext());
        dialog.setCancelable(true);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirmation);


        IransansButton yesBtn = dialog.findViewById(R.id.yesBtn);
        IranSansTextView textView = dialog.findViewById(R.id.text);
        IransansButton noBtn = dialog.findViewById(R.id.noBtn);
        textView.setText("آیا مایل به لغو این سفارش هستید؟");

//
        yesBtn.setOnClickListener(view -> {

            cancelOrder(item);
            dialog.dismiss();


        });
        noBtn.setOnClickListener(view -> {
                    dialog.dismiss();
                }
        );


        dialog.show();
    }


    @Override
    public void onRefresh() {
        setData(true);
    }
}
