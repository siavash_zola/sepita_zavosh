package hoodadtech.com.sepita.view.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.about_us.AboutUsResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.IranSansTextView;

public class AboutUsActivity extends AppCompatActivity {


    Server server;
    private IranSansTextView tv_about_us,tv_phone;
    private LinearLayout linearCall,btn_back,linearBox;
    private ProgressBar loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        reference();
        listener();
        getAboutUs();
    }

    private void listener() {
        linearCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + tv_phone.getText().toString()));
                startActivity(intent);
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void reference() {
        tv_about_us = findViewById(R.id.tv_about_us);
        tv_phone = findViewById(R.id.tv_phone);
        linearCall = findViewById(R.id.linearCall);
        btn_back = findViewById(R.id.btn_back);
        linearBox = findViewById(R.id.linearBox);
        server = Server.getInstance(this);
        tv_about_us.setMovementMethod(new ScrollingMovementMethod());
        loader = findViewById(R.id.loader);
        loader.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);
    }


    public void getAboutUs() {
        server.getAboutUs(new FutureCallback<Response<AboutUsResult>>() {
            @Override
            public void onCompleted(Exception e, Response<AboutUsResult> result) {
                loader.setVisibility(View.INVISIBLE);
                if (result != null){
                    if (result.getHeaders().code() == 401 ){
                        Toast.makeText(AboutUsActivity.this,getString(R.string.error),Toast.LENGTH_SHORT).show();
                    }else if (result.getHeaders().code() == 200 ){
                        if (result.getResult().getStatus().getIsSuccess()){
                            tv_about_us.setText(result.getResult().getResult().getAbout());
                            tv_phone.setText(result.getResult().getResult().getContact());
                            linearBox.setVisibility(View.VISIBLE);
                        }else {
                            Toast.makeText(AboutUsActivity.this,result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(AboutUsActivity.this,getString(R.string.error),Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(AboutUsActivity.this,getString(R.string.checkConnection),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
