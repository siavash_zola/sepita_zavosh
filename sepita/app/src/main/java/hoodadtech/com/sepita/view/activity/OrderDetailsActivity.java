package hoodadtech.com.sepita.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.newAdapters.BankAdapter;
import hoodadtech.com.sepita.adapter.newAdapters.OrderDetailsAdapter;
import hoodadtech.com.sepita.model.AddressNew;
import hoodadtech.com.sepita.model.Bank;
import hoodadtech.com.sepita.model.TestCode;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.checkOrder.CheckOrder;
import hoodadtech.com.sepita.model.serverResult.getPaymentType.PaymentType;
import hoodadtech.com.sepita.model.serverResult.getPaymentType.PaymentTypeResult;
import hoodadtech.com.sepita.model.serverResult.orderDetailResult.OrderDetailResultNew;
import hoodadtech.com.sepita.model.serverResult.orderDetailResult.ServiceDetail;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.MyClickListener;
import hoodadtech.com.sepita.utils.OrderToast;
import hoodadtech.com.sepita.utils.RtlGridLayoutManager;
import hoodadtech.com.sepita.utils.SharedPreference;

public class OrderDetailsActivity extends AppCompatActivity implements MyClickListener{

    private IranSansTextView type_wash,tv_phone,tv_name,tv_title,tv_numberOrder,tv_date,tv_sex,tv_number,tv_hour,tv_city,tv_price,tv_description,tv_retry;
    private ImageView iv_sex,iv_profile;
    private LinearLayout type_two,type_One,linearLayout_accepted,linearLayout_waitToAccept,linearLayout_paymentType,linearLayout_cancel,linearLayout_back,linearLayout_allDetail,linearLayout_call;
    private ProgressBar loader;
    private String orderId;
    private String status,paymentType = "null";
    private boolean check = false;
    private List<Bank> banks ;
    private BankAdapter adapter;
    private Bank bankSelected;
    private Integer finalAmount;
    private RecyclerView recyclerView;
    private RtlGridLayoutManager layoutManager;
    private List<ServiceDetail> list;
    private OrderDetailsAdapter adapterNew;
    private Server server;
    private AlertDialog show;
    private IranSansTextView tv_cash,tv_credit,tv_online,tv_deductedCredit,tv_priceDialog;
    private LinearLayout linear_pay,linear_cash,linear_credit,linear_online;
    private boolean isSendRequest = false;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        reference();
        listeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ProgressBar myLoader = (ProgressBar) findViewById(R.id.myLoader);
        myLoader.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);
        myLoader.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            handler.removeCallbacksAndMessages(null);
        }catch (Exception e){}

    }

    private void reference() {

        server = Server.getInstance(this);
        banks = new ArrayList<>();
        tv_numberOrder = findViewById(R.id.tv_numberOrder);
        tv_name = findViewById(R.id.tv_name);
        tv_phone = findViewById(R.id.tv_phone);
        tv_date = findViewById(R.id.tv_date);
        tv_sex = findViewById(R.id.tv_sex);
        tv_number = findViewById(R.id.tv_number);
        tv_hour = findViewById(R.id.tv_hour);
        tv_city = findViewById(R.id.tv_city);
        tv_price = findViewById(R.id.tv_price);
        tv_description = findViewById(R.id.tv_description);
        tv_title = findViewById(R.id.tv_title);
        tv_retry = findViewById(R.id.tv_retry);
        type_wash = findViewById(R.id.type_wash);

        type_One = findViewById(R.id.type_One);
        type_two = findViewById(R.id.type_two);


        iv_sex = findViewById(R.id.iv_sex);
        iv_profile = findViewById(R.id.iv_profile);

        list = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        list.clear();
        layoutManager = new RtlGridLayoutManager(this,2);
        recyclerView.setLayoutManager(layoutManager);
        adapterNew = new OrderDetailsAdapter(list,OrderDetailsActivity.this);
        recyclerView.setAdapter(adapterNew);

        linearLayout_waitToAccept = findViewById(R.id.linearLayout_waitToAccept);
        linearLayout_paymentType = findViewById(R.id.linearLayout_paymentType);
        linearLayout_cancel = findViewById(R.id.linearLayout_cancel);
        linearLayout_back = findViewById(R.id.linearLayout_back);
        linearLayout_call = findViewById(R.id.linearLayout_call);
        linearLayout_allDetail = findViewById(R.id.linearLayout_allDetail);
        linearLayout_accepted = findViewById(R.id.linearLayout_accepted);


        loader = findViewById(R.id.loader);

        loader.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.white), android.graphics.PorterDuff.Mode.SRC_ATOP);

        if (getIntent().hasExtra("checkFirst")){
            OrderToast.showToast(OrderDetailsActivity.this);
        }

        if (getIntent().hasExtra("status")){
            status = getIntent().getExtras().getString("status");
            //if (status.equals("1")){
            //    linearLayout_accepted.setVisibility(View.VISIBLE);
            //    linearLayout_waitToAccept.setVisibility(View.GONE);
            //}else {
            //    linearLayout_accepted.setVisibility(View.GONE);
            //    linearLayout_waitToAccept.setVisibility(View.VISIBLE);
            //}
            Log.i("kk",status);
            switch (status){
                case "1":
                    linearLayout_accepted.setVisibility(View.GONE);
                    linearLayout_waitToAccept.setVisibility(View.VISIBLE);
                    linearLayout_cancel.setBackgroundResource(R.drawable.bg_button_blue);
                    check = true;


                    break;
                case "2":
                    linearLayout_accepted.setVisibility(View.VISIBLE);
                    linearLayout_waitToAccept.setVisibility(View.GONE);
                    linearLayout_cancel.setBackgroundResource(R.drawable.bg_button_blue);
                    check = true;
                    break;
                case "3":
                    linearLayout_accepted.setVisibility(View.VISIBLE);
                    linearLayout_waitToAccept.setVisibility(View.GONE);
                    //linearLayout_cancel.setBackgroundResource(R.drawable.bg_button_silver);
                    linearLayout_cancel.setVisibility(View.GONE);
                    check = false;
                    break;
                case "4":
                    linearLayout_accepted.setVisibility(View.VISIBLE);
                    linearLayout_waitToAccept.setVisibility(View.GONE);
                    //linearLayout_cancel.setBackgroundResource(R.drawable.bg_button_silver);
                    linearLayout_cancel.setVisibility(View.GONE);
                    check = false;
                    break;
            }
        }else {
            status = "1";
        }
        if (getIntent().hasExtra("id")){
            orderId = getIntent().getExtras().getString("id");
            Log.i("kk",orderId);
            loadOrder(server , orderId);
        }else {
            //Log.i("kk","intent");
        }

    }

    private void listeners() {
        tv_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSendRequest) {
                    tv_retry.setVisibility(View.GONE);
                    loader.setVisibility(View.VISIBLE);
                    loadOrder(server, orderId);
                }
            }
        });
        linearLayout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        linearLayout_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSendRequest) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + tv_phone.getText().toString()));
                    startActivity(intent);
                }
            }
        });
        linearLayout_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSendRequest) {
                    showDialogCancel();
                }
            }
        });
        linearLayout_paymentType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isSendRequest) {
                    showDialogPaymentType();
                }
            }
        });
    }

    private void showDialogCancel() {
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailsActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_confirmation, null);
        builder.setView(rootView);
        builder.create();
        AlertDialog show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        IransansButton cancel = rootView.findViewById(R.id.yesBtn);
        IransansButton no = rootView.findViewById(R.id.noBtn);
        IranSansTextView text = rootView.findViewById(R.id.text);
        text.setText(getString(R.string.didUWantCancel));
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelOrder(view);
                if (check) {
                    show.cancel();
                }
            }
        });

    }

    private void loadOrder(Server server,String orderId) {
        isSendRequest = true;
        server.getOrderDetailNew(orderId, new FutureCallback<Response<OrderDetailResultNew>>() {
            @Override
            public void onCompleted(Exception e, Response<OrderDetailResultNew> result) {
                if (result != null) {
                    if (result.getHeaders().code() == 401) {
                        getToken(1, orderId);
                    } else if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().getStatusCode() == 16) {
                            getToken(1, orderId);
                        } else if (result.getResult().getStatus().getIsSuccess()) {
                            isSendRequest = false;
                            loader.setVisibility(View.GONE);
                            linearLayout_allDetail.setVisibility(View.VISIBLE);

                            handler = new Handler();
                            int delay = 15000; //milliseconds
                            if (status.equals("1")) {
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        //do something
                                        Log.i("kk", "check");
                                        checkStatusOrder(result.getResult().getResult().getOrderCode());
                                        handler.postDelayed(this, delay);
                                    }
                                }, delay);
                            }






                            tv_title.setText(result.getResult().getResult().getServiceTitle());
                            tv_numberOrder.setText(result.getResult().getResult().getOrderCode());
                            tv_name.setText(result.getResult().getResult().getEmployeeTitle() + "");
                            tv_phone.setText(result.getResult().getResult().getEmployeePhone() + "");

                            List<ServiceDetail> serviceDetails = result.getResult().getResult().getServiceDetails();
                            list.clear();
                            for (int i = 0; i < serviceDetails.size(); i++) {
                                list.add(serviceDetails.get(i));
                            }
                            adapterNew.notifyDataSetChanged();
                            if (result.getResult().getResult().getEmployeeImageUrl() != null && result.getResult().getResult().getEmployeeImageUrl().length()>0) {
                                loadIcon(iv_profile, result.getResult().getResult().getEmployeeImageUrl());
                            }else {
                                iv_profile.setImageResource(R.drawable.user);
                            }

                            tv_date.setText(result.getResult().getResult().getVisitDateTime());


                            finalAmount = result.getResult().getResult().getFinalAmount();
                            tv_price.setText(convert(result.getResult().getResult().getFinalAmount() + ""));

                            tv_description.setText(result.getResult().getResult().getDescription());
                            tv_city.setText(result.getResult().getResult().getCityArea());


                            //serviceTypeCode
                            switch (result.getResult().getResult().getServiceTypeCode()) {
                                case "1":
                                    type_One.setVisibility(View.VISIBLE);
                                    type_two.setVisibility(View.GONE);

                                    tv_number.setText(result.getResult().getResult().getNumber() + "");
                                    tv_hour.setText(result.getResult().getResult().getDuration() + "");
                                    switch (result.getResult().getResult().getEmployeeGender()) {
                                        case "خانم":
                                            iv_sex.setImageResource(R.drawable.female);
                                            tv_sex.setText(getString(R.string.female));
                                            break;
                                        case "آقا":
                                            iv_sex.setImageResource(R.drawable.male_icon);
                                            tv_sex.setText(getString(R.string.male));
                                            break;
                                        case "N/A":
                                            iv_sex.setImageResource(R.drawable.female_male);
                                            tv_sex.setText(getString(R.string.notImportant));
                                            break;
                                    }
                                    break;
                                case "2":
                                    type_One.setVisibility(View.GONE);
                                    type_two.setVisibility(View.VISIBLE);

                                    type_wash.setText(result.getResult().getResult().getWashType());
                                    break;
                                case "4":
                                    type_One.setVisibility(View.VISIBLE);
                                    type_two.setVisibility(View.GONE);

                                    tv_number.setText(result.getResult().getResult().getNumber() + "");
                                    tv_hour.setText(result.getResult().getResult().getDuration() + "");
                                    switch (result.getResult().getResult().getEmployeeGender()) {
                                        case "خانم":
                                            iv_sex.setImageResource(R.drawable.female);
                                            tv_sex.setText(getString(R.string.female));
                                            break;
                                        case "آقا":
                                            iv_sex.setImageResource(R.drawable.male_icon);
                                            tv_sex.setText(getString(R.string.male));
                                            break;
                                        case "N/A":
                                            iv_sex.setImageResource(R.drawable.female_male);
                                            tv_sex.setText(getString(R.string.notImportant));
                                            break;
                                    }
                                    break;
                            }
                        } else {
                            loader.setVisibility(View.GONE);
                            tv_retry.setVisibility(View.VISIBLE);
                            Toast.makeText(OrderDetailsActivity.this, result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                            isSendRequest = false;
                        }
                    } else {
                        loader.setVisibility(View.GONE);
                        tv_retry.setVisibility(View.VISIBLE);
                        Toast.makeText(OrderDetailsActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                        isSendRequest = false;
                    }
                }else {
                    loader.setVisibility(View.GONE);
                    tv_retry.setVisibility(View.VISIBLE);
                    Toast.makeText(OrderDetailsActivity.this, getString(R.string.checkConnection), Toast.LENGTH_SHORT).show();
                    isSendRequest = false;
                }
            }
        });
    }

    private void loadIcon(ImageView imageView, String iconUrl) {
        Ion.with(OrderDetailsActivity.this).load(iconUrl).intoImageView(imageView);
    }
    private void cancelOrder(View view) {
        Server server = Server.getInstance(OrderDetailsActivity.this);
        server.cancelOrder(orderId, (e, result) -> {
            if (e != null) {
                CustomSnackbar.showSnackBar(view);
            } else if (OperationResult.isOk(result, e)) {
                Toast.makeText(OrderDetailsActivity.this, result.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                onBackPressed();
            } else if (result != null && result.getStatus() != null) {
                Toast.makeText(OrderDetailsActivity.this, result.getStatus().getMessage(), Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(OrderDetailsActivity.this, "", Toast.LENGTH_SHORT).show();
            }

        });
    }
    public String convert(String price){
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        String numberAsString = numberFormat.format(Integer.parseInt(price));
        return numberAsString;
    }
    private void showDialogPaymentType() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailsActivity.this);
        View rootView = inflater.inflate(R.layout.dialog_payment_type, null);
        builder.setView(rootView);
        builder.create() ;
        show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ProgressBar loader2 = rootView.findViewById(R.id.loader);
        loader2.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.accent), android.graphics.PorterDuff.Mode.SRC_ATOP);
        LinearLayout linearLayout_cash = rootView.findViewById(R.id.linearLayout_cash);
        LinearLayout linearLayout_Credit = rootView.findViewById(R.id.linearLayout_Credit);
        LinearLayout linearLayout_online = rootView.findViewById(R.id.linearLayout_online);
        LinearLayout linearLayout_increaseCredit = rootView.findViewById(R.id.linearLayout_increaseCredit);
        linear_pay = rootView.findViewById(R.id.linear_pay);
        linear_cash = rootView.findViewById(R.id.linear_cash);
        linear_credit = rootView.findViewById(R.id.linear_credit);
        linear_online = rootView.findViewById(R.id.linear_online);
        IransansButton btn_pay = rootView.findViewById(R.id.btn_pay);

        tv_cash = rootView.findViewById(R.id.tv_cash);
        tv_credit = rootView.findViewById(R.id.tv_credit);
        tv_online = rootView.findViewById(R.id.tv_online);
        tv_deductedCredit = rootView.findViewById(R.id.tv_deductedCredit);
        tv_priceDialog = rootView.findViewById(R.id.tv_price);



        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView_bank);
        LinearLayoutManager layoutManager = new LinearLayoutManager(OrderDetailsActivity.this,LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new BankAdapter(banks,OrderDetailsActivity.this);
        adapter.listener = this;
        recyclerView.setAdapter(adapter);




        linearLayout_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout_cash.setBackgroundResource(R.drawable.bg_button_blue);
                linearLayout_Credit.setBackgroundResource(R.drawable.bg_button_silver);
                linearLayout_online.setBackgroundResource(R.drawable.bg_button_silver);

                tv_cash.setTextColor(getResources().getColor(R.color.white));
                tv_credit.setTextColor(getResources().getColor(R.color.text_color));
                tv_online.setTextColor(getResources().getColor(R.color.text_color));
                tv_online.setTextColor(getResources().getColor(R.color.text_color));

                paymentType = "cash";
            }
        });
        linearLayout_Credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout_cash.setBackgroundResource(R.drawable.bg_button_silver);
                linearLayout_Credit.setBackgroundResource(R.drawable.bg_button_blue);
                linearLayout_online.setBackgroundResource(R.drawable.bg_button_silver);

                tv_cash.setTextColor(getResources().getColor(R.color.text_color));
                tv_credit.setTextColor(getResources().getColor(R.color.white));
                tv_online.setTextColor(getResources().getColor(R.color.text_color));

                paymentType = "credit";
            }
        });
        linearLayout_online.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout_cash.setBackgroundResource(R.drawable.bg_button_silver);
                linearLayout_Credit.setBackgroundResource(R.drawable.bg_button_silver);
                linearLayout_online.setBackgroundResource(R.drawable.bg_button_blue);

                tv_cash.setTextColor(getResources().getColor(R.color.text_color));
                tv_credit.setTextColor(getResources().getColor(R.color.text_color));
                tv_online.setTextColor(getResources().getColor(R.color.white));

                paymentType = "online";
            }
        });
        linearLayout_increaseCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OrderDetailsActivity.this,IncreaseCreditActivity.class));
                show.dismiss();
            }
        });

        getPaymentType(server);
    }

    @Override
    public void itemClicked(int position) {
        for (int i = 0; i < banks.size(); i++) {
            Bank bank = banks.get(i);
            if (i == position){
                bank.setSelected(true);
                bankSelected = bank;

            }else {
                bank.setSelected(false);
            }
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        if (!isSendRequest) {
            if (getIntent().hasExtra("from")) {
                super.onBackPressed();
            } else {
                Intent intent = new Intent(OrderDetailsActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }

    public void getToken(int b,String orderId){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(OrderDetailsActivity.this), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(OrderDetailsActivity.this, result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(OrderDetailsActivity.this, sharedPreference.getLoginTokenValue(OrderDetailsActivity.this));
                        if (b == 1) {
                            loadOrder(server1,orderId);
                        }else {
                            getPaymentType(server1);
                        }

                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(OrderDetailsActivity.this, result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        isSendRequest = false;
                    }
                }else {
                    Toast.makeText(OrderDetailsActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    isSendRequest = false;
                }
            }
        });
    }

    public void getPaymentType(Server server) {
        isSendRequest = true;
        server.getPaymentType(new FutureCallback<Response<PaymentTypeResult>>() {
            @Override
            public void onCompleted(Exception e, Response<PaymentTypeResult> result) {

                if (result != null) {
                    if (result.getHeaders().code() == 401) {
                        getToken(2, null);
                    } else if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().getStatusCode() == 16) {
                            getToken(2, null);
                        } else if (result.getResult().getStatus().getIsSuccess()) {
                            tv_priceDialog.setText(convert(result.getResult().getResult().getRemainAmount()));
                            if (Integer.parseInt(result.getResult().getResult().getRemainAmount()) > finalAmount) {
                                tv_deductedCredit.setText(getString(R.string.price) + " " + tv_price.getText().toString() + " " + getString(R.string.deductedCredit));
                            } else {
                                tv_deductedCredit.setText(R.string.notEnough);
                            }
                            banks.clear();
                            for (int i = 0; i < result.getResult().getResult().getBanks().size(); i++) {
                                banks.add(result.getResult().getResult().getBanks().get(i));
                            }

                            for (int i = 0; i < result.getResult().getResult().getPaymentType().size(); i++) {
                                PaymentType paymentType = result.getResult().getResult().getPaymentType().get(i);
                                switch (paymentType.getTitle()) {
                                    case "پرداخت آنلاین":
                                        if (paymentType.getIsActive()) {
                                            linear_online.setVisibility(View.VISIBLE);
                                        } else {
                                            linear_online.setVisibility(View.GONE);
                                        }
                                        break;
                                    case "اعتباری":
                                        if (paymentType.getIsActive()) {
                                            linear_credit.setVisibility(View.VISIBLE);
                                        } else {
                                            linear_credit.setVisibility(View.GONE);
                                        }
                                        break;
                                    case "نقدی":
                                        if (paymentType.getIsActive()) {
                                            linear_cash.setVisibility(View.VISIBLE);
                                        } else {
                                            linear_cash.setVisibility(View.GONE);
                                        }
                                        break;
                                }

                            }
                            linear_pay.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();
                            isSendRequest = false;
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                            show.dismiss();
                            isSendRequest = false;
                        }
                    } else {
                        Toast.makeText(OrderDetailsActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                        show.dismiss();
                        isSendRequest = false;
                    }

                }else {
                    Toast.makeText(OrderDetailsActivity.this, getString(R.string.checkConnection), Toast.LENGTH_SHORT).show();
                    show.dismiss();
                    isSendRequest = false;
                }

            }
        });
    }

    private void checkStatusOrder(String orderCode) {
        server.checkMyOrder(orderCode, new FutureCallback<Response<CheckOrder>>() {
            @Override
            public void onCompleted(Exception e, Response<CheckOrder> result) {
                if (result!=null){
                    if (result.getHeaders().code() == 401){

                    }else if (result.getHeaders().code() ==200){
                        if (result.getResult().getStatus().getStatusCode() == 16){

                        }else if (result.getResult().getStatus().getIsSuccess()){
                            if (result.getResult().getResult().getHasEmploye()){
                                //Log.i("kk","ghabool shod");
                                handler.removeCallbacksAndMessages(null);
                                linearLayout_accepted.setVisibility(View.VISIBLE);
                                linearLayout_waitToAccept.setVisibility(View.GONE);

                                tv_name.setText(result.getResult().getResult().getName());
                                tv_phone.setText(result.getResult().getResult().getCellNumber());
                                if (result.getResult().getResult().getImageUrl() != null && result.getResult().getResult().getImageUrl().length()>0){
                                    loadIcon(iv_profile, result.getResult().getResult().getImageUrl());
                                }else {
                                    iv_profile.setImageResource(R.drawable.user);
                                }
                            }else {
                                //Log.i("kk","entezar");
                            }
                        }else {
                            //Toast.makeText(OrderDetailsActivity.this,result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                            //Log.i("kk",""+result.getResult().getStatus().getStatusCode());
                        }
                    }else {
                        //Toast.makeText(OrderDetailsActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    //Log.i("kk","internet");
                }
            }
        });
    }
}
