
package hoodadtech.com.sepita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceDetails {

    @SerializedName("ServiceDetailId")
    @Expose
    private String serviceDetailId;
    @SerializedName("Quantity")
    @Expose
    private String quantity;

    public String getServiceDetailId() {
        return serviceDetailId;
    }

    public void setServiceDetailId(String serviceDetailId) {
        this.serviceDetailId = serviceDetailId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }


    @Override
    public String toString() {
        return "ServiceDetails{" +
                "serviceDetailId='" + serviceDetailId + '\'' +
                ", quantity='" + quantity + '\'' +
                '}';
    }
}
