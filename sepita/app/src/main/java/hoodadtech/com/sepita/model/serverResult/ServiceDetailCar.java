
package hoodadtech.com.sepita.model.serverResult;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceDetailCar {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("serviceDetailPrices")
    @Expose
    private List<ServiceDetailPrice> serviceDetailPrices = null;

    boolean selected=false;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ServiceDetailPrice> getServiceDetailPrices() {
        return serviceDetailPrices;
    }

    public void setServiceDetailPrices(List<ServiceDetailPrice> serviceDetailPrices) {
        this.serviceDetailPrices = serviceDetailPrices;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
