package hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.Service;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.menuInfoResult.MenuInfoResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.PublicMethod;
import hoodadtech.com.sepita.utils.SeterInfo;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import hoodadtech.com.sepita.view.activity.BaseActivity;
import hoodadtech.com.sepita.view.activity.HomeActivity;
import hoodadtech.com.sepita.view.activity.MapsActivity;
import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.MultiRecyclerAdapter;
import hoodadtech.com.sepita.adapter.listItems.VerticalListItem;
import hoodadtech.com.sepita.model.serverResult.HomeResult;

public class FrgHomeList extends Fragment implements VerticalListItem.ItemClickListener, SwipeRefreshLayout.OnRefreshListener {


    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    private MultiRecyclerAdapter multiRecyclerAdapter;
    private List<MultiAdapterItem> homeItems;
    HomeResult homeResult;
    ProgressBar progressBar;
    private SwipeRefreshLayout swipeContainer;
    RelativeLayout layout_no_internet;
    boolean dataLoaded = false;
    View view;
    Server server;

    public static SeterInfo seterInfo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frg_home, container, false);
         server = Server.getInstance(getContext());
        //((HomeActivity)getActivity()).filldraweritems();
        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });
        initView(view);
        return view;
    }

    private void initView(View view) {
        swipeContainer = view.findViewById(R.id.swiperefresh);
        swipeContainer.setColorSchemeResources(
                R.color.app_blue2
                );
        layout_no_internet = view.findViewById(R.id.layout_no_internet);
        swipeContainer.setOnRefreshListener(this);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);
        recyclerView = view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getContext());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),2);
        multiRecyclerAdapter = new MultiRecyclerAdapter(getContext());
        recyclerView.setAdapter(multiRecyclerAdapter);
        recyclerView.setLayoutManager(gridLayoutManager);

    }

    @Override
    public void onResume() {
        super.onResume();
        BaseActivity.isHome = true;
        setData(server,false);
    }

    @Override
    public void onPause() {
        super.onPause();
        BaseActivity.isHome = false;
    }

    private void updateCredit() {
        server.getCredit((e, result) -> {
            if(isAdded()) {
                if (OperationResult.isOk(result, e)) {
                    //new SharedPreference().saveCodeTakhfif(getContext(), result.getResult());
                    new SharedPreference().saveCodeTakhfif(getContext(), "siavash");
                    //((HomeActivity)getActivity()).filldraweritems();
                }
            }

        });
    }

    private void setData(Server server,boolean refresh) {
        //((HomeActivity)getActivity()).filldraweritems();
        layout_no_internet.setVisibility(View.GONE);
        if (!refresh)
            progressBar.setVisibility(View.VISIBLE);

        server.getServices((e, result) -> {
            progressBar.setVisibility(View.GONE);
            swipeContainer.setRefreshing(false);
            homeItems = new ArrayList<>();
            if (isAdded()) {

                if (result != null) {
                    if (result.getHeaders().code() == 401) {
                        getToken(refresh);
                    } else if (result.getHeaders().code() == 200) {
                        if (e != null) {
                            //   TSnackbar.make(view,"Hello from TSnackBar.",TSnackbar.LENGTH_LONG).show();
                            CustomSnackbar.showSnackBar(view);

                        } else if (result.getResult().getStatus().getStatusCode() == 16) {
                            getToken(refresh);
                        } else if (result.getResult().getStatus().isSuccess()) {
                            layout_no_internet.setVisibility(View.GONE);
                            dataLoaded = true;
                            List<Service> hhomeItems = result.getResult().getResult();

                            for (int i = 0; i < hhomeItems.size(); i++) {
                                VerticalListItem listItem = new VerticalListItem();
                                listItem.setItem(hhomeItems.get(i));
                                listItem.setItemClickListener(this);
                                homeItems.add(listItem);
                            }

                            //------pishnahad
                            //VerticalListItem listItem = new VerticalListItem();
                            //Service suggest = new Service();
                            //suggest.setId("suggest");
                            //suggest.setActive(true);
                            //suggest.setTitle(getString(R.string.newText));
                            //suggest.setBigTitle(getString(R.string.suggestService));
                            //suggest.setTitleEn("COMING SOON");
                            //listItem.setItem(suggest);
                            //listItem.setItemClickListener(this);
                            //homeItems.add(listItem);
                            //------pishnahad

                            multiRecyclerAdapter.setItems(homeItems);
                            multiRecyclerAdapter.notifyDataSetChanged();

                            getInfo();
                        } else {
                            Toast.makeText(getContext(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        }
//                    Toast.makeText(getContext(), String.valueOf(result.getHeaders().code()), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), getString(R.string.plzCheckNetwork), Toast.LENGTH_SHORT).show();
                }
            }

        });
    }

    public void getToken(boolean b){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getContext()), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(getActivity(), result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(getActivity(), sharedPreference.getLoginTokenValue(getActivity()));
                        setData(server1,b);
                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //isSendRequest = false;
                }
            }
        });
    }

    @Override
    public void onClick(Service item, View view) {
        if (item.getIsActive()) {
            PostOrderObject postOrderObject = new PostOrderObject();
            postOrderObject.setServiceId(item.getId());
            postOrderObject.setServiceTypeCodeId(item.getServiceTypeCode());
            FileUtility fileUtility = new FileUtility(getContext());
            fileUtility.writePostOrderObject(postOrderObject);
            Intent intent = new Intent(getContext(), MapsActivity.class);
            Gson gson = new Gson();
            intent.putExtra("service", gson.toJson(item));
            startActivity(intent);
        }
    }

    @Override
    public void onRefresh() {
        setData(server,true);
    }

    public void getInfo(){
        Server server = Server.getInstance(getActivity());
        server.getMenuInfo(new FutureCallback<MenuInfoResult>() {
            @Override
            public void onCompleted(Exception e, MenuInfoResult result) {
                if (result != null){
                    if (result.getStatus() != null) {
                        if (result.getStatus().getIsSuccess()) {

                            PublicMethod publicMethod = new PublicMethod(getActivity());
                            publicMethod.saveFullName(result.getResult().getFullname());
                            publicMethod.saveCredit(result.getResult().getUserCredit());
                            publicMethod.saveImage(result.getResult().getImageUrl());

                            seterInfo.setInfo(getActivity());


                        }
                    }
                }
            }
        });
    }

}
