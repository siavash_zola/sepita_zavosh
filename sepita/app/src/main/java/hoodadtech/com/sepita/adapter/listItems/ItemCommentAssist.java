package hoodadtech.com.sepita.adapter.listItems;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.viewHolders.ItemCommentAssistHolder;
import hoodadtech.com.sepita.adapter.viewHolders.ItemRequestDetailHolder;
import hoodadtech.com.sepita.model.Reason;
import hoodadtech.com.sepita.model.ServiceDetail2;
import hoodadtech.com.sepita.utils.StringUtility;

/**
 * Created by Milad on 1/30/2018.
 */

public class ItemCommentAssist implements MultiAdapterItem {
    ItemCommentAssistHolder itemHolder;
    Reason item;
    @Override
    public int getTypeId() {
        return 6;
    }

    @Override
    public void setup(RecyclerView.ViewHolder holder ) {
        itemHolder= (ItemCommentAssistHolder) holder;
        if(itemHolder!=null) {
            itemHolder.txt_title.setText(item.getTitle());
            itemHolder.txt_title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(itemClickListener!=null){
                        itemClickListener.onClick(item,view);
                        if(item.isSelected())
                        {
                            itemHolder.layout_bg.setBackgroundDrawable(itemHolder.layout_bg.getResources().getDrawable(R.drawable.bg_border_accent));
                            itemHolder.txt_title.setTextColor(itemHolder.txt_title.getResources().getColor(R.color.white));
                        }
                        else {
                            itemHolder.layout_bg.setBackgroundDrawable(itemHolder.layout_bg.getResources().getDrawable(R.drawable.bg_rounded_border_gray));
                            itemHolder.txt_title.setTextColor(itemHolder.txt_title.getResources().getColor(R.color.gray_medium));
                        }
                    }
                }
            });

        }

    }

    public void setItem(Reason item) {
        this.item = item;
    }

    @Override
    public Reason getItem() {
        return item;
    }


    ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onClick(Reason item, View view);

    }



}
