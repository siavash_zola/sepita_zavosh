
package hoodadtech.com.sepita.model.serverResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.serverResult.DateAndTimeResult.ResultDate;
import hoodadtech.com.sepita.model.serverResult.addAddress.Status;

public class RegisterResult {

    @SerializedName("userCode")
    @Expose
    private Integer userCode;
    @SerializedName("tokenId")
    @Expose
    private String tokenId;
    @SerializedName("activationCode")
    @Expose
    private String activationCode;



    public Integer getUserCode() {
        return userCode;
    }

    public void setUserCode(Integer userCode) {
        this.userCode = userCode;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

}
