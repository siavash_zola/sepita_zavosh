
package hoodadtech.com.sepita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("Lat")
    @Expose
    private String lat;
    @SerializedName("Long")
    @Expose
    private String _long;
    @SerializedName("AddressDescription")
    @Expose
    private String addressDescription;
    @SerializedName("Number")
    @Expose
    private String number;
    @SerializedName("Unit")
    @Expose
    private String unit;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Description")
    @Expose
    private String description;



    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("neighborhood")
    @Expose
    private String neighborhood;

    @SerializedName("addressId")
    @Expose
    private String addressId;


    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLong() {
        return _long;
    }

    public void setLong(String _long) {
        this._long = _long;
    }

    public String getAddressDescription() {
        return addressDescription;
    }

    public void setAddressDescription(String addressDescription) {
        this.addressDescription = addressDescription;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    @Override
    public String toString() {
        return "Address{" +
                "lat='" + lat + '\'' +
                ", _long='" + _long + '\'' +
                ", addressDescription='" + addressDescription + '\'' +
                ", number='" + number + '\'' +
                ", unit='" + unit + '\'' +
                ", phone='" + phone + '\'' +
                ", description='" + description + '\'' +
                ", city='" + city + '\'' +
                ", neighborhood='" + neighborhood + '\'' +
                '}';
    }
}
