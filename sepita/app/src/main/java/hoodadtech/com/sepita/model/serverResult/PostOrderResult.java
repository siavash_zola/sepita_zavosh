package hoodadtech.com.sepita.model.serverResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.utils.StringUtility;

/**
 * Created by Milad on 2/6/2018.
 */

public class PostOrderResult {
    @SerializedName("orderId")
    @Expose
    String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
