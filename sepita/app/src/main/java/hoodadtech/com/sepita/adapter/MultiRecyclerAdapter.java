package hoodadtech.com.sepita.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.viewHolders.BuyingHomeItemHolder;
import hoodadtech.com.sepita.adapter.viewHolders.CarwashItemHolder;
import hoodadtech.com.sepita.adapter.viewHolders.DateItemHolder;
import hoodadtech.com.sepita.adapter.viewHolders.DetailItemHolder;
import hoodadtech.com.sepita.adapter.viewHolders.GridItemHolder;
import hoodadtech.com.sepita.adapter.viewHolders.ItemCommentAssistHolder;
import hoodadtech.com.sepita.adapter.viewHolders.ItemCommentEmplyoeeHolder;
import hoodadtech.com.sepita.adapter.viewHolders.ItemRequestDetailHolder;
import hoodadtech.com.sepita.adapter.viewHolders.ItemWorkerListItemHolder;
import hoodadtech.com.sepita.adapter.viewHolders.OrderListItemHolder;
import hoodadtech.com.sepita.adapter.viewHolders.VerticalItemHolder;

public class
MultiRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<MultiAdapterItem> homeItems = new ArrayList<>();
    Context context;
    LayoutInflater layoutInflater;

    public MultiRecyclerAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    public List<MultiAdapterItem> getHomeItems() {
        return homeItems;
    }

    public void setItems(List<MultiAdapterItem> homeItems) {
        this.homeItems = homeItems;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case 0: {//list item
                view = layoutInflater.inflate(R.layout.item_services_new, parent, false);
                return new VerticalItemHolder(view);
            }
            case 1: {//grid item
                view = layoutInflater.inflate(R.layout.item_grid, parent, false);
                return new GridItemHolder(view);
            }

            case 2: {//grid item
                view = layoutInflater.inflate(R.layout.item_date_time, parent, false);
                return new DateItemHolder(view);
            }
            case 3: {
                view = layoutInflater.inflate(R.layout.item_first_detail, parent, false);
                return new DetailItemHolder(view);
            }

            case 4: {
                view = layoutInflater.inflate(R.layout.item_order_pending, parent, false);
                return new OrderListItemHolder(view);
            }
            case 5: {
                view = layoutInflater.inflate(R.layout.item_details_order, parent, false);
                return new ItemRequestDetailHolder(view);
            }
            case 6: {
                view = layoutInflater.inflate(R.layout.item_comment, parent, false);
                return new ItemCommentAssistHolder(view);
            }
            case 7: {
                view = layoutInflater.inflate(R.layout.item_employee_comment, parent, false);
                return new ItemCommentEmplyoeeHolder(view);
            }
            case 8: {
                view = layoutInflater.inflate(R.layout.item_worker, parent, false);
                return new ItemWorkerListItemHolder(view);
            }

            case 9: {
                view = layoutInflater.inflate(R.layout.item_carwash, parent, false);
                return new CarwashItemHolder(view);
            }
            case 10: {
                view = layoutInflater.inflate(R.layout.item_buying_home, parent, false);
                return new BuyingHomeItemHolder(view);
            }

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            MultiAdapterItem homeItem = homeItems.get(position);
            //setFadeAnimation(holder.itemView);
            //homeItem.setup(holder,false);
            homeItem.setup(holder);
        } catch (Exception e) {
            //TODO check for why class cast exception raise
        }
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(300);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return homeItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return homeItems.get(position).getTypeId();
    }

    public void delete(MultiAdapterItem item) {
        int temp = homeItems.indexOf(item);
        homeItems.remove(temp);
        notifyItemRemoved(temp);

    }



}
