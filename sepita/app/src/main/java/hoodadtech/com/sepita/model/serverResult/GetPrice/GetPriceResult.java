package hoodadtech.com.sepita.model.serverResult.GetPrice;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import hoodadtech.com.sepita.model.ServiceDetails;
import hoodadtech.com.sepita.model.serverResult.addAddress.Status;

public class GetPriceResult {
    @SerializedName("result")
    @Expose
    private Result result;

    @SerializedName("status")
    @Expose
    private Status status;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
