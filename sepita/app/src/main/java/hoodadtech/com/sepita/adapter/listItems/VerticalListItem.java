package hoodadtech.com.sepita.adapter.listItems;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.koushikdutta.ion.Ion;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.viewHolders.VerticalItemHolder;
import hoodadtech.com.sepita.model.Service;

/**
 * Created by Milad on d1/5/2018.
 */

public class VerticalListItem implements MultiAdapterItem {
    Service item;
    VerticalItemHolder itemHolder;

    @Override
    public int getTypeId() {
        return 0;
    }

    @Override
    public void setup(RecyclerView.ViewHolder holder) {
        itemHolder = (VerticalItemHolder) holder;
        if (itemHolder != null) {
            if (item.getId().equals("suggest")){
                itemHolder.bigTitle.setVisibility(View.VISIBLE);
                itemHolder.bigTitle.setText(item.getBigTitle());
                itemHolder.linearBox_service.setBackgroundResource(R.drawable.bg_item_suggest);
                itemHolder.img.setImageResource(R.drawable.suggest);
            }else {
                //Ion.with(itemHolder.img.getContext()).load("http://mobile.3pita.com/"+item.getImageUrl()).intoImageView(itemHolder.img);
                Ion.with(itemHolder.img.getContext()).load(item.getImageUrl()).intoImageView(itemHolder.img);
                //Log.i("kk","http://mobile.3pita.com/"+item.getImageUrl());
                itemHolder.bigTitle.setVisibility(View.GONE);
                itemHolder.linearBox_service.setBackgroundResource(R.drawable.bg_button_white);
            }
            if (!item.getActive()){
                itemHolder.linearBox_service.setBackgroundResource(R.drawable.bg_item_suggest);
            }
            itemHolder.title.setText(item.getTitle());
            itemHolder.english_title.setText(item.getTitleEn());
            itemHolder.linearBox_service.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(itemClickListener!=null)
                        itemClickListener.onClick(item,view);
                }
            });
        }


    }

    public void setItem(Service item) {
        this.item = item;
    }

    @Override
    public Service getItem() {
        return item;
    }
    ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener{
        void onClick(Service item,View view);

    }
}
