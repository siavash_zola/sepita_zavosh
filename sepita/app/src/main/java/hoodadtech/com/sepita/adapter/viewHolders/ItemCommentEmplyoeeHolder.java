package hoodadtech.com.sepita.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.IranSansTextView;

/**
 * Created by Milad on 1/30/2018.
 */

public class ItemCommentEmplyoeeHolder extends RecyclerView.ViewHolder {
    public IranSansTextView TV_comment, TV_days_ago, TV_Name;


    public ItemCommentEmplyoeeHolder(View itemView) {
        super(itemView);
        TV_Name = itemView.findViewById(R.id.TV_Name);
        TV_days_ago = itemView.findViewById(R.id.TV_days_ago);
        TV_comment = itemView.findViewById(R.id.TV_comment);


    }
}
