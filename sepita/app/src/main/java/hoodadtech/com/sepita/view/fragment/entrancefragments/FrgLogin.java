package hoodadtech.com.sepita.view.fragment.entrancefragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.IransansEditText;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import hoodadtech.com.sepita.view.activity.HomeActivity;

public class FrgLogin extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    IranSansTextView btn_register;
    IransansButton btn_login;
    IranSansTextView btn_forgot_password;
    IransansEditText ET_input_name,ET_password;
    RelativeLayout mainLayout;
    ProgressBar progressBar;
    String phoneNumber,password;
    ImageView IV_bg;


    public FrgLogin() {

    }

    public static FrgLogin newInstance(String param1, String param2) {
        FrgLogin fragment = new FrgLogin();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.frg_login, container, false);

        initView(view);
        return view;
    }

    private void initView(View view) {
        btn_register=view.findViewById(R.id.btn_register);
        ET_input_name=view.findViewById(R.id.input_name);
        ET_password=view.findViewById(R.id.input_email);
        mainLayout=view.findViewById(R.id.mainLayout);
        btn_login=view.findViewById(R.id.btn_login);
        btn_forgot_password=view.findViewById(R.id.btn_forgot_password);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.gray_light0), android.graphics.PorterDuff.Mode.SRC_ATOP);

        IV_bg = view.findViewById(R.id.IV_bg);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getActivity().getWindowManager().getDefaultDisplay().getWidth(),
                getActivity().getWindowManager().getDefaultDisplay().getHeight());
        IV_bg.setLayoutParams(layoutParams);
        btn_register.setOnClickListener(this);
        btn_forgot_password.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        mainLayout.setOnClickListener(this);

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });

    }


    @Override
    public void onClick(View view) {
        FragmentHandler fragmentHandler=new FragmentHandler(getActivity(),getFragmentManager());

        switch (view.getId()){
            case R.id.btn_register:
                hideKeyboard();
                fragmentHandler.loadFragment(new FrgRegister(),true);
                break;
            case R.id.btn_forgot_password:
                hideKeyboard();
                fragmentHandler.loadFragment(new FrgForgetPassword(),true);
                break;
            case R.id.btn_login:
                hideKeyboard();
                loginUser(fragmentHandler);
                break;
            case R.id.mainLayout:
                hideKeyboard();
                break;

        }
    }

    private void loginUser(FragmentHandler fragmentHandler) {
        phoneNumber=ET_input_name.getMyText();
        password=ET_password.getMyText();
        if (phoneNumber.length() == 0) {
            Toast.makeText(getContext(), "لطفا شماره تلفن همراه خود را وارد کنید", Toast.LENGTH_SHORT).show();
        } else if (password.length() == 0) {
            Toast.makeText(getContext(), "لطفا رمز عبور خود را وارد کنید", Toast.LENGTH_SHORT).show();
        } else if (!StringUtility.isValidPhoneNumber(phoneNumber)) {
            Toast.makeText(getContext(), "شماره وارد شده معتبر نمی باشد", Toast.LENGTH_SHORT).show();
        }
        else {
            progressBar.setVisibility(View.VISIBLE);
            btn_login.setText("لطفا منتظر بمانید");
            btn_login.setEnabled(false);
            btn_forgot_password.setEnabled(false);
            btn_register.setEnabled(false);
            Server server=Server.getInstance(getContext());
            server.accountLoginUser1(StringUtility.numberFa2En(phoneNumber), password, (e, result) -> {
                progressBar.setVisibility(View.INVISIBLE);
                btn_login.setText("ورود");
                btn_login.setEnabled(true);
                btn_forgot_password.setEnabled(true);
                btn_register.setEnabled(true);
                if(result != null) {
                    //Toast.makeText(getContext(), result.getResult().getResult().getTokenId() + " "+result.getResult().getStatus().getMessage()+" " +result.getHeaders().code(), Toast.LENGTH_SHORT).show();
                        if (result.getResult().getStatus().getIsSuccess()) {
                            SharedPreference sharedPreference = new SharedPreference();
                            sharedPreference.saveLoginToken(getContext(), result.getResult().getResult().getTokenId());
                            startActivity(new Intent(getActivity(), HomeActivity.class));
                            UserObject userObject = new UserObject();
                            userObject.setActivationCode(password);
                            userObject.setCellNumber(StringUtility.numberFa2En(phoneNumber));
                            sharedPreference.saveUserObject(getContext(), new Gson().toJson(userObject));
                            getActivity().finish();
                        }else  {
                            Toast.makeText(getContext(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();

                        }
                }else  {
                    Toast.makeText(getContext(), getActivity().getString(R.string.error), Toast.LENGTH_SHORT).show();
                }

            });
        }
    }

    void hideKeyboard(){
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

}
