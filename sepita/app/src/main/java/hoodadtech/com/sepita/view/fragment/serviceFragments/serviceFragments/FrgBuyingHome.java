package hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.MultiRecyclerAdapter;
import hoodadtech.com.sepita.adapter.listItems.BuyingHomeItem;
import hoodadtech.com.sepita.adapter.listItems.DetailsItem;
import hoodadtech.com.sepita.adapter.newAdapters.BuyingHomeAdapter;
import hoodadtech.com.sepita.model.Date;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.Service;
import hoodadtech.com.sepita.model.ServiceDetail;
import hoodadtech.com.sepita.model.ServiceDetails;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.DateAndTimeResult.DateAndTimeResult;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.visitingTimes.VisitingTimesResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.MyClickListener;
import hoodadtech.com.sepita.utils.NetworkUtil;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import hoodadtech.com.sepita.view.activity.OrderDetailsActivity;
import io.blackbox_vision.wheelview.view.WheelView;

public class FrgBuyingHome extends Fragment implements BuyingHomeItem.CheckChangedListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener ,MyClickListener{

    LinearLayout btn_submit;
    TextView time, price;
    int firstCounter = 0;
    int Ttime = 0;
    int Pprice = 0;
    Server server;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    private MultiRecyclerAdapter multiRecyclerAdapter;
    private List<MultiAdapterItem> homeItems;
    Service service;
    View view;
    List<ServiceDetails> serviceDetailsList;
    int servieQuantity;
    boolean dataLoaded = false;
    FileUtility fileUtility;
    PostOrderObject postOrderObject;
    ProgressBar progressBar;
    Dialog dialog;
    IranSansTextView btn_date,tv_service_title;
    String selectedtime = null;
    int selectedTimePos = 3;
    String selectedDatee = null;
    int SelectedDatePos = 3;
    LinearLayout date_layout,linearBox;
    List<Date> dateList;
    List<String> dateArrayList, timeArrayList;
    String[] dateArray, foo_array;
    String selectedDateTime = "";
    private List<ServiceDetail> list;
    private BuyingHomeAdapter adapter;
    private SwipeRefreshLayout swipeContainer;
    private AlertDialog show;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (view != null) {
            //TextView title = getActivity().findViewById(R.id.toolbar_text);
            //title.setText(service.getTitle());
            return view;
        }

        view = inflater.inflate(R.layout.frg_buying_home, container, false);

        serviceDetailsList = new ArrayList<>();
        fileUtility = new FileUtility(getContext());
        postOrderObject = fileUtility.getPostOrderObject();
        service = new Gson().fromJson(getArguments().getString("service"), Service.class);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        //TextView title = getActivity().findViewById(R.id.toolbar_text);
        //title.setText(service.getTitle());
        server = Server.getInstance(getContext());
        getDateANDTimes();
        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });
        initView(view);
        return view;
    }

    private void getDateANDTimes() {
        dateArrayList = new ArrayList<>();
        timeArrayList = new ArrayList<>();
        dateList = new ArrayList<>();
        Log.i("kk",service.getId());
        server.getVisitingTimesNewNewNew(service.getId(), new FutureCallback<Response<DateAndTimeResult>>() {
            @Override
            public void onCompleted(Exception e, Response<DateAndTimeResult> result) {

                if (result != null){
                    if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().getIsSuccess()) {
                            timeArrayList = result.getResult().getResult().getHoures();
                            dateList = result.getResult().getResult().getDates();
                            for (int i = 0; i < result.getResult().getResult().getDates().size(); i++) {
                                dateArrayList.add(result.getResult().getResult().getDates().get(i).getShowDate());

                            }
                            setData(server, false);
                        }
                    }
                }
            }
        });
    }

    private void initView(View view) {
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue1), android.graphics.PorterDuff.Mode.SRC_ATOP);

        btn_date = view.findViewById(R.id.btn_date);
        tv_service_title = view.findViewById(R.id.tv_service_title);
        linearBox = view.findViewById(R.id.linearBox);
        //btn_date.setOnClickListener(this);
        date_layout = view.findViewById(R.id.date_layout);
        date_layout.setOnClickListener(this);
        list = new ArrayList<>();
        IranSansTextView title = view.findViewById(R.id.header_title);
        title.setText(service.getTitle());
        btn_submit = view.findViewById(R.id.btn_submit);
        time = view.findViewById(R.id.time);
        price = view.findViewById(R.id.price);
        swipeContainer = view.findViewById(R.id.swiperefresh);
        swipeContainer.setOnRefreshListener(this);
        swipeContainer.setColorSchemeResources(R.color.app_blue1);
        time.setText(StringUtility.numberEn2Fa(String.valueOf(firstCounter)));
        price.setText(StringUtility.numberEn2Fa(String.valueOf(firstCounter)));
        btn_submit.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getContext());
        multiRecyclerAdapter = new MultiRecyclerAdapter(getContext());
        adapter = new BuyingHomeAdapter(list,getActivity());
        adapter.listener = this;
        //recyclerView.setAdapter(multiRecyclerAdapter);
        recyclerView.setAdapter(adapter);
;
        recyclerView.setLayoutManager(layoutManager);



    }

    private void setData(Server server,boolean isRefresh) {
        if (!isRefresh) {
            progressBar.setVisibility(View.VISIBLE);
        }

        server.getServiceDetails(service.getId(), (e, result) -> {
            if (isAdded()) {
                if (result != null) {
                    if (result.getHeaders().code() == 401) {
                        getToken(isRefresh);
                    } else if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().getStatusCode() == 16){
                            getToken(isRefresh);
                        }else if (result.getResult().getStatus().isSuccess()){
                            dataLoaded = true;
                            Ttime = result.getResult().getResult().getBaseDuration();
                            Pprice = result.getResult().getResult().getBaseAmount();
                            homeItems = new ArrayList<>();
                            List<ServiceDetail> hhomeItems = result.getResult().getResult().getServiceDetails();

                            time.setText(StringUtility.numberEn2Fa(String.valueOf(result.getResult().getResult().getBaseDuration())));
                            price.setText(StringUtility.numberEn2Fa(String.valueOf(result.getResult().getResult().getBaseAmount())));
                            tv_service_title.setText(result.getResult().getResult().getServiceTitle());

                            list.clear();
                            for (int i = 0; i < hhomeItems.size(); i++) {
                                ServiceDetail serviceDetail = hhomeItems.get(i);
                                serviceDetail.setSelected(false);
                                list.add(serviceDetail);

                            }
                            adapter.notifyDataSetChanged();
                            date_layout.setVisibility(View.VISIBLE);
                            multiRecyclerAdapter.setItems(homeItems);
                        }else {
                            Toast.makeText(getContext(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), getString(R.string.checkConnection), Toast.LENGTH_SHORT).show();
                }
                swipeContainer.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                linearBox.setVisibility(View.VISIBLE);

            }


        });
    }

    void showDialog() {
        //dialog = new Dialog(getContext());
        //dialog.setCancelable(false);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setContentView(R.layout.dialog_date_time);

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_date_time, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(rootView);
        builder.setCancelable(false);
        builder.create();
        show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final WheelView wheelView = rootView.findViewById(R.id.loop_view);
        final WheelView time_loop_view = rootView.findViewById(R.id.time_loop_view);
        wheelView.setInitialPosition(SelectedDatePos);
        wheelView.setIsLoopEnabled(false);


        wheelView.addOnLoopScrollListener((item, position) -> {
            selectedDatee = item.toString();
            SelectedDatePos = position;
            postOrderObject.setVisitDate(dateList.get(SelectedDatePos).getDate());
            selectedDateTime = selectedDatee + " " + selectedtime;

        });
        if (dateArrayList.size() > 0)
            wheelView.setItems(dateArrayList);

        time_loop_view.setInitialPosition(selectedTimePos);
        time_loop_view.setIsLoopEnabled(false);
        time_loop_view.addOnLoopScrollListener((item, position) -> {
            selectedtime = item.toString();
            selectedTimePos = position;
            postOrderObject.setVisitTime(StringUtility.numberFa2En(selectedtime));
            selectedDateTime = selectedDatee + " " + selectedtime;


        });
        if (timeArrayList.size() > 0)
            time_loop_view.setItems(timeArrayList);


        IransansButton cancelBtn = rootView.findViewById(R.id.canel_Btn);
        IransansButton confirmBtn = rootView.findViewById(R.id.confirmBtn);


//
        cancelBtn.setOnClickListener(view -> show.dismiss());

        confirmBtn.setOnClickListener(view -> {
            if (selectedDatee == null)
                selectedDatee = dateList.get(SelectedDatePos).getShowDate();
            if (selectedtime == null)
                selectedtime = timeArrayList.get(selectedTimePos);
            postOrderObject.setVisitDate(dateList.get(SelectedDatePos).getDate());
            postOrderObject.setVisitTime(StringUtility.numberFa2En(selectedtime));
            selectedDateTime = selectedDatee + " " + selectedtime;
            btn_date.setText(StringUtility.numberEn2Fa(selectedDateTime.toString()));
            show.dismiss();
        });

        //dialog.show();
    }

    private void getToken1() {
        progressBar.setVisibility(View.VISIBLE);
        Server server = Server.getInstance(getContext());
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getContext()), UserObject.class);
        //server.accountLoginUser(userObject.getCellNumber(), userObject.getActivationCode(), (e, result) -> {
        //    progressBar.setVisibility(View.INVISIBLE);
        //    linearBox.setVisibility(View.VISIBLE);
        //    if (e != null) {
        //        CustomSnackbar.showSnackBar(view);
        //    } else if (OperationResult.isOk(result, e)) {
        //        SharedPreference sharedPreference = new SharedPreference();
        //        sharedPreference.saveLoginToken(getContext(), result.getResult().getTokenId());
        //        Server server1 = Server.getInstance(getContext(), sharedPreference.getLoginTokenValue(getContext()));
        //        server1.getServiceDetails(service.getId(), (e1, result1) -> {
        //            if (isAdded()) {
        //                progressBar.setVisibility(View.GONE);
        //                if (e1 != null) {
        //                    CustomSnackbar.showSnackBar(view);
        //                } else if (OperationResult.isOk(result1.getResult(), e1)) {
        //                    dataLoaded = true;
        //                    Ttime = result1.getResult().getResult().getBaseDuration();
        //                    Pprice = result1.getResult().getResult().getBaseAmount();
        //                    homeItems = new ArrayList<>();
        //                    List<ServiceDetail> hhomeItems = result1.getResult().getResult().getServiceDetails();
//
        //                    time.setText(StringUtility.numberEn2Fa(String.valueOf(result1.getResult().getResult().getBaseDuration())));
        //                    price.setText(StringUtility.numberEn2Fa(String.valueOf(result1.getResult().getResult().getBaseAmount())));
        //                    list.clear();
        //                    for (int i = 0; i < hhomeItems.size(); i++) {
        //                        ServiceDetail serviceDetail = hhomeItems.get(i);
        //                        serviceDetail.setSelected(false);
        //                        list.add(serviceDetail);
        //                    }
//
        //                    adapter.notifyDataSetChanged();
        //                } else if (result != null && result.getStatus() != null) {
//
        //                    Toast.makeText(getContext(), result.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
        //                } else {
        //                    Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();
//
        //                }
        //            }
//
//
        //        });
//
//
        //    } else {
        //        Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();
//
        //    }
        //});

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_submit:
                if (!NetworkUtil.isOnline(getContext())) {
                    CustomSnackbar.showSnackBar(btn_submit);
                    //} else if (Ttime == 0)
                    //    Toast.makeText(getContext(), "موردی انتخاب نشده است", Toast.LENGTH_SHORT).show();
                }else if (!checkList()) {
                    Toast.makeText(getContext(), "موردی انتخاب نشده است", Toast.LENGTH_SHORT).show();
                } else if (selectedDatee == null || selectedtime == null) {
                    if (selectedDatee == null)
                        Toast.makeText(getContext(), "لطفا تاریخ مورد نظر خود را مشخص کنید", Toast.LENGTH_SHORT).show();

                    else if (selectedtime == null)
                        Toast.makeText(getContext(), "لطفا ساعت مورد نظر خود را مشخص کنید", Toast.LENGTH_SHORT).show();
                    return;

                } else if (dataLoaded) {
                    try {
                        serviceDetailsList.clear();
                        for (int i = 0; i < list.size(); i++) {
                            ServiceDetail serviceDetail = list.get(i);
                            if (serviceDetail.isSelected()){
                                ServiceDetails serviceDetails = new ServiceDetails();
                                serviceDetails.setQuantity("1");
                                serviceDetails.setServiceDetailId(serviceDetail.getId());
                                serviceDetailsList.add(serviceDetails);
                            }


                        }
                        postOrderObject.setServiceDetails(serviceDetailsList);
                        postOrderObject.setDuration(Ttime);
                        postOrderObject.setNetAmount(String.valueOf(Pprice));
                        postOrderObject.setCarwashServiceId("");
                        fileUtility.writePostOrderObject(postOrderObject);

                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        String frgTag = getActivity().getClass().getSimpleName();
                        FrgAddress fragment = new FrgAddress();
                        Bundle bundle = new Bundle();
                        bundle.putInt("time", Ttime);
                        bundle.putInt("price", Pprice);
                        bundle.putString("serviceCode",service.getServiceTypeCode());
                        fragment.setArguments(bundle);
                        fragmentTransaction.replace(R.id.fragment_container, fragment, frgTag);
                        fragmentTransaction.addToBackStack(frgTag);
                        fragmentTransaction.commit();
                    } catch (Exception e) {
                    }

                }

                break;
            case R.id.btn_date:
                showDialog();
                break;

            case R.id.date_layout:
                showDialog();
                break;
//

        }

    }

    @Override
    public void onRefresh() {
        getDateANDTimes();
        //setData(true);
    }

    @Override
    public void onItemClick(ServiceDetail item, View view, boolean b) {
        if (item.isSelected()) {
            ServiceDetails serviceDetails = new ServiceDetails();
            serviceDetails.setQuantity("1");
            serviceDetails.setServiceDetailId(item.getId());
            serviceDetailsList.add(serviceDetails);
            Ttime = Ttime + item.getDuration();
            Pprice = Pprice + item.getAmount();
            time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
            price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
        } else {

            List<ServiceDetails> temp = serviceDetailsList;
            for (int i = 0; i < temp.size(); i++) {
                if (item.getId().equals(temp.get(i).getServiceDetailId())) {
                    serviceDetailsList.remove(temp.get(i));
                }
            }

            Ttime = Ttime - item.getDuration();
            Pprice = Pprice - item.getAmount();
            time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
            price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
        }
    }


    @Override
    public void itemClicked(int position) {

        List<ServiceDetail> holder = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            ServiceDetail serviceDetail1 = list.get(i);

            if (position == i){
                if (serviceDetail1.isSelected()){
                    serviceDetail1.setSelected(false);
                    //Ttime = Ttime - serviceDetail1.getDuration();
                    Pprice = Pprice - serviceDetail1.getAmount();
                    //time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
                    price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
                }else {
                    serviceDetail1.setSelected(true);
                    //Ttime = Ttime + serviceDetail1.getDuration();
                    Pprice = Pprice + serviceDetail1.getAmount();
                    //time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
                    price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
                }
            }
            holder.add(serviceDetail1);
        }

        list.clear();

        for (int i = 0; i < holder.size(); i++) {
            list.add(holder.get(i));
        }
        holder.clear();
        adapter.notifyDataSetChanged();
    }

    public boolean checkList(){
        for (int i = 0; i < list.size(); i++) {
            ServiceDetail serviceDetail = list.get(i);
            if (serviceDetail.isSelected()){
                return true;
            }
        }
        return false;
    }

    public void getToken(boolean is){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getContext()), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(getActivity(), result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(getActivity(), sharedPreference.getLoginTokenValue(getActivity()));
                        setData(server1,is);

                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        //isSendRequest = false;
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //isSendRequest = false;
                }
            }
        });
    }
}
