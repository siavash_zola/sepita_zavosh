package hoodadtech.com.sepita.view.activity;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.newAdapters.HistoryAdapter;
import hoodadtech.com.sepita.model.AddressNew;
import hoodadtech.com.sepita.model.Item_history;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.orderListResult.OrderListResult;
import hoodadtech.com.sepita.model.serverResult.orderListResult.Result;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.SharedPreference;

import static java.security.AccessController.getContext;

public class HistoryActivity extends AppCompatActivity {

    private List<Item_history> list;
    private RecyclerView recyclerView;
    private HistoryAdapter adapter;
    private LinearLayout btn_back;
    private ProgressBar loader;
    private SwipeRefreshLayout srl;
    private Server server;
    private boolean isSendRequest = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        reference();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData(server);
    }

    @Override
    public void onBackPressed() {
        if (!isSendRequest) {
            super.onBackPressed();
        }
    }

    private void reference() {
        list = new ArrayList<>();
        server= Server.getInstance(HistoryActivity.this);
        srl = findViewById(R.id.srl);
        srl.setColorSchemeResources(R.color.app_blue1);
        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new HistoryAdapter(list,HistoryActivity.this);
        recyclerView.setAdapter(adapter);
        btn_back = findViewById(R.id.btn_back);
        loader = findViewById(R.id.loader);
        loader.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue1), android.graphics.PorterDuff.Mode.SRC_ATOP);
        listeners();


    }

    private void listeners() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(server);
            }
        });
    }

    private void loadData(Server server) {
        isSendRequest = true;
        server.getOrderListNew("0", new FutureCallback<Response<OrderListResult>>() {
            @Override
            public void onCompleted(Exception e, Response<OrderListResult> result) {

                list.clear();

                if (result != null) {
                    if (result.getHeaders().code() == 401) {
                        getToken();
                    } else if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().getStatusCode() == 16) {
                            getToken();
                            //Log.i("kk","null");
                        } else if (result.getResult().getStatus().getIsSuccess()) {
                            List<Result> resultList = result.getResult().getResult();
                            for (int i = 0; i < resultList.size(); i++) {
                                Result resultItem = resultList.get(i);
                                Item_history item = new Item_history();
                                item.setOrderId(resultItem.getOrderId());
                                item.setServiceTitle(resultItem.getServiceTitle());
                                item.setOrderCode(resultItem.getOrderCode());
                                item.setCityArea(resultItem.getCityArea());
                                item.setDuration(resultItem.getDuration());
                                item.setAmount(resultItem.getAmount() + "");
                                item.setPaymentType(resultItem.getOrderPaymentType());
                                item.setStatusTitle(resultItem.getStatusTitle());
                                item.setStatusCode(resultItem.getStatusCode());
                                item.setOrderDate(resultItem.getOrderDate());
                                item.setStatusColor(resultItem.getStatusColor());

                                list.add(item);
                            }
                            adapter.notifyDataSetChanged();
                            loader.setVisibility(View.INVISIBLE);
                            srl.setRefreshing(false);
                            isSendRequest = false;
                        } else {
                            Toast.makeText(HistoryActivity.this, result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                            loader.setVisibility(View.INVISIBLE);
                            srl.setRefreshing(false);
                            isSendRequest = false;
                        }
                    } else {
                        Toast.makeText(HistoryActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                        loader.setVisibility(View.INVISIBLE);
                        srl.setRefreshing(false);
                        isSendRequest = false;
                    }
                }else {
                    Toast.makeText(HistoryActivity.this, getString(R.string.plzCheckNetwork), Toast.LENGTH_SHORT).show();
                    loader.setVisibility(View.INVISIBLE);
                    srl.setRefreshing(false);
                    isSendRequest = false;
                }
            }
        });
    }

    public void getToken(){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(HistoryActivity.this), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(HistoryActivity.this, result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(HistoryActivity.this, sharedPreference.getLoginTokenValue(HistoryActivity.this));
                        loadData(server1);
                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(HistoryActivity.this, result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        loader.setVisibility(View.INVISIBLE);
                        srl.setRefreshing(false);
                        isSendRequest = false;
                    }
                }else {
                    Toast.makeText(HistoryActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    loader.setVisibility(View.INVISIBLE);
                    srl.setRefreshing(false);
                    isSendRequest = false;
                }
            }
        });
    }
}
