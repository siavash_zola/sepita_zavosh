package hoodadtech.com.sepita.model;

public class DrawerItem {
    int id;
    String icon;
    int iconResource;
    String title;
    Boolean selected;
    public DrawerItem(String title, int iconResource){
        setTitle(title);
        setIconResource(iconResource);
    }
    public DrawerItem(String title){
        setTitle(title);
    }
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIconResource() {
        return iconResource;
    }

    public void setIconResource(int iconResource) {
        this.iconResource = iconResource;
    }
}
