package hoodadtech.com.sepita.network;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.koushikdutta.ion.builder.Builders;

import java.util.List;

import hoodadtech.com.sepita.BuildConfig;
import hoodadtech.com.sepita.model.AddressNew;
import hoodadtech.com.sepita.model.CalculatePriceObject;
import hoodadtech.com.sepita.model.CarModel;
import hoodadtech.com.sepita.model.PostMessageObject;
import hoodadtech.com.sepita.model.serverResult.CalculatePriceResult;
import hoodadtech.com.sepita.model.serverResult.CarWashServiceDetailsResult;
import hoodadtech.com.sepita.model.serverResult.DateAndTimeResult.DateAndTimeResult;
import hoodadtech.com.sepita.model.serverResult.DeletResult.DeleteResult;
import hoodadtech.com.sepita.model.serverResult.DiscountResult;
import hoodadtech.com.sepita.model.serverResult.EditProfileResult;
import hoodadtech.com.sepita.model.serverResult.GetPrice.GetPriceResult;
import hoodadtech.com.sepita.model.serverResult.GetPrice.GetPrice_sender;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.Order;
import hoodadtech.com.sepita.model.serverResult.OrderCommentResult;
import hoodadtech.com.sepita.model.serverResult.OrderDetailResult;
import hoodadtech.com.sepita.model.PostOrderCommentRequst;
import hoodadtech.com.sepita.model.serverResult.PostOrderResult;
import hoodadtech.com.sepita.model.ProfileRequest;
import hoodadtech.com.sepita.model.serverResult.PostOrderResultNew;
import hoodadtech.com.sepita.model.serverResult.ProfileResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.Service;
import hoodadtech.com.sepita.model.serverResult.ServiceDetailResult;
import hoodadtech.com.sepita.model.VisitingDatesTimes;
import hoodadtech.com.sepita.model.serverResult.ResultEmployeeDetail;
import hoodadtech.com.sepita.model.serverResult.Worker;
import hoodadtech.com.sepita.model.serverResult.about_us.AboutUsResult;
import hoodadtech.com.sepita.model.serverResult.addAddress.AddAddressResult;
import hoodadtech.com.sepita.model.serverResult.checkOrder.CheckOrder;
import hoodadtech.com.sepita.model.serverResult.getAddCredit.GetAddCreditResult;
import hoodadtech.com.sepita.model.serverResult.getAddress.GetAddressResult;
import hoodadtech.com.sepita.model.serverResult.getAddressOnMap.GetAddressOnMapResult;
import hoodadtech.com.sepita.model.serverResult.getCarWashOneResult.GetCarWashOneResult;
import hoodadtech.com.sepita.model.serverResult.getPaymentType.PaymentTypeResult;
import hoodadtech.com.sepita.model.serverResult.loginResult.LoginResult;
import hoodadtech.com.sepita.model.serverResult.menuInfoResult.MenuInfoResult;
import hoodadtech.com.sepita.model.serverResult.orderDetailResult.OrderDetailResultNew;
import hoodadtech.com.sepita.model.serverResult.orderListResult.OrderListResult;
import hoodadtech.com.sepita.model.serverResult.profileGet.ProfileGetResult;
import hoodadtech.com.sepita.model.serverResult.visitingTimes.VisitingTimesResult;
import hoodadtech.com.sepita.utils.SharedPreference;


public class Server {

    public boolean isSendRequest = false;

    private final String BaseUrl = "http://mobile.3pita.com/";
    private final String UrlAccountRegister = BaseUrl+"account/register";
    private final String UrlTestAccountRegister = BaseUrl+"account/register";
    private final String UrlAccountLogin = BaseUrl+"account/login";
    private final String UrlAccountLogout = BaseUrl+"account/logout";
    private final String UrlAccountActivate = BaseUrl+"account/activate";
    private final String UrlAccountResendActivationCode = BaseUrl+"account/resendcode";
    private final String UrlGetServices = BaseUrl+"service/get";
    private final String UrlGetServiceDetails = BaseUrl+"servicedetail_new/get?serviceid=";
    private final String UrlAccountForgetPassword = BaseUrl+"account/forget";
    private final String UrlDiscountCodeActivation = BaseUrl+"Discount/CheckCodeValidation";
    private final String UrlGetVisitingTimes = BaseUrl+"VisitDate/get";
    private final String UrlGetVisitingTimesNewNew = BaseUrl+"VisitDate/get?serviceId=";
    private final String UrlGetOrderList = BaseUrl+"order/list";
    private final String UrlPostOrder = BaseUrl+"order/postorder_new";
    private final String UrlGetOrderDetail = BaseUrl+"order/detail";
    private final String UrlGetOrderCancel = BaseUrl+"order/Cancel";
    private final String UrlGetProfile= BaseUrl+"profile/get";
    private final String UrlEditProfile= BaseUrl+"profile/post";
    private final String UrlGetOrderComment= BaseUrl+"OrderComment/get";
    private final String UrlPostOrderComment= BaseUrl+"OrderComment/post";
    private final String UrlPriceCalculation= BaseUrl+"order/CalculatePrice";
    private final String UrlGetCredit=BaseUrl+"credit/get";
    //employee
    private final String UrlGetEmployeeDetails= BaseUrl+"employee/detail";
    private final String UrlAddToFavoriteEmplyoee= BaseUrl+"employee/addtofavorite";
    private final String UrlRemoveFromFavoriteEmplyoee= BaseUrl+"employee/removefromfavorite";
    private final String UrlGetFavoriteEmplyoeesList= BaseUrl+"employee/getfavoritelist";

    //carwash
    private final String UrlGetCarBrands= BaseUrl+"servicedetail/get/car_new?serviceId=";
    private final String UrlGetCarMoldesByBrand= BaseUrl+"servicedetail/get/carmodel?carbrandid=";

    //about us
    private final String UrlGetAboutUsText= BaseUrl+"Home/about";
    private final String UrlGetContactNumber= BaseUrl+"Home/Contact";
    private final String UrlGetPostMessage= BaseUrl+"message/post";
    private final String UrlGetInviteText= BaseUrl+"InviteText/get";

    //address
    private final String UrlSendNewAddress= BaseUrl+"address/postaddress";
    private final String UrlGetAllAddress= BaseUrl+"adress/getaddress";
    private final String UrlGetAllAddressOnMap= BaseUrl+"adress/getonmap";
    private final String UrlPostOrderNew= BaseUrl+"order/postorder";
    private final String UrlMenuInfo= BaseUrl+"user/getInfo";
    private final String UrlGetPayment= BaseUrl+"paymentType/GetPaymentType";
    private final String UrlGetAddCredit= BaseUrl+"Payment/GetAddCredit";
    private final String UrlGetCarWash= BaseUrl+"servicedetail/get/car_new";
    private final String UrlCheckOrder= BaseUrl+"OrderEmployee/get";
    private final String UrlAboutUs= BaseUrl+"Home/AboutContact";
    private final String UrlGetPrice= BaseUrl+"ResidentialComplex/post";
    private final String UrlDeleteAddress= BaseUrl+"address/delete?id=";
    private final String UrlUpdateAddress= BaseUrl+"address/update";


    private final String charset = "UTF-8";
    private String LoginToken,tempLoginToken;
    public Context context;
    private String _version;
    private String _device;
    private String _deviceType = "2";
    private final String TAG = "Server";
    static Server server;
    private String IMEI;
    private long personId;

    private String isHuaweiUser=null;


    private String _deviceToken;

    private Server(Context context) {
        disableConnectionReuseIfNecessary();
        this.context = context;
        SharedPreference sharedPreference = new SharedPreference();
        LoginToken = sharedPreference.getLoginTokenValue(context);
        tempLoginToken = sharedPreference.getTempLoginTokenValue(context);
    }

    private Server(Context context, String loginToken) {
        disableConnectionReuseIfNecessary();
        this.context = context;
        LoginToken = loginToken;
    }

    public static Server getInstance(Context context) {
        if (server == null) {
            server = new Server(context.getApplicationContext());
        } else {
            server.context = context.getApplicationContext();
            SharedPreference sharedPreference = new SharedPreference();
            server.LoginToken = sharedPreference.getLoginTokenValue(context.getApplicationContext());
            server.tempLoginToken = sharedPreference.getTempLoginTokenValue(context.getApplicationContext());

        }
        return server;

    }

    public static Server getInstance(Context context, String loginToken) {
        if (server == null) {
            server = new Server(context.getApplicationContext(), loginToken);
        } else {
            server.context = context.getApplicationContext();
            server.LoginToken = loginToken;
        }
        return server;
    }

    public String getLoginToken() {
        return LoginToken;
    }

    public String GetDeviceString() {
        if (_device != null)
            return _device;
        if (Build.MODEL.contains(Build.MANUFACTURER)) {
            _device = Build.MODEL;
            return _device;
        }
        _device = Build.MANUFACTURER + " " + Build.MODEL;
        return _device;
    }

    public String GetVersion() {
        if (_version != null)
            return _version;
        _version = "ERROR";
        try {
            _version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return _version;
    }
    public String getOsVersion() {
        try {
            return  String.valueOf(android.os.Build.VERSION.SDK_INT);
        }catch (Exception e){
            return "";
        }

    }

    public String getPackageName() {
        return context.getPackageName();
    }

    public String getDeviceId() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getBuildNumber() {
        return String.valueOf(BuildConfig.VERSION_CODE);
    }

    private Builders.Any.U prepareIon(String url) {
        return Ion.with(context)
                .load(url)
                .setHeader("Authorization", LoginToken)
                .setBodyParameter("loginToken", LoginToken);
    }


    public void getOrderListNew(String statusCode, FutureCallback<Response<OrderListResult>> callback) {
        TypeToken<OrderListResult> types = new TypeToken<OrderListResult>() {
        };
        Ion.with(context).load(UrlGetOrderList)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("statusCode", statusCode + "")
                .as(types)
                .withResponse()
                .setCallback(callback);
    }
    public void getOrderDetailNew(String orderId, FutureCallback<Response<OrderDetailResultNew>> callback) {
        TypeToken<OrderDetailResultNew> types = new TypeToken<OrderDetailResultNew>() {
        };
        Ion.with(context).load(UrlGetOrderDetail)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("orderId", orderId + "")
                .as(types)
                .withResponse()
                .setCallback(callback);
    }
    public void postOrderNew(String postOrder, FutureCallback<Response<PostOrderResultNew>> callback) {
        TypeToken<PostOrderResultNew> types = new TypeToken<PostOrderResultNew>() {
        };
        Log.i("kk","post");
        Ion.with(context).load(UrlPostOrderNew)
                .setHeader("Authorization",LoginToken)
                .setHeader("Content-Type", "application/json")
                .setStringBody(postOrder)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }
    //public void getAllAddressOnMap( FutureCallback<GetAddressOnMapResult> callback) {
    public void getAllAddressOnMap(FutureCallback<Response<GetAddressOnMapResult>>  callback) {
        TypeToken<GetAddressOnMapResult> types = new TypeToken<GetAddressOnMapResult>() {
        };
        Ion.with(context).load(UrlGetAllAddressOnMap)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }
    public void getAllAddress( FutureCallback<Response<GetAddressResult>> callback) {
        TypeToken<GetAddressResult> types = new TypeToken<GetAddressResult>() {
        };
        Ion.with(context).load(UrlGetAllAddress)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }
    public void getMenuInfo( FutureCallback<MenuInfoResult> callback) {
        TypeToken<MenuInfoResult> types = new TypeToken<MenuInfoResult>() {
        };
        Ion.with(context).load(UrlMenuInfo)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .setCallback(callback);
    }
    public void getPaymentType( FutureCallback<Response<PaymentTypeResult>> callback) {
        TypeToken<PaymentTypeResult> types = new TypeToken<PaymentTypeResult>() {
        };
        Ion.with(context).load(UrlGetPayment)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }
    public void getAddCredit( FutureCallback<Response<GetAddCreditResult>> callback) {
        TypeToken<GetAddCreditResult> types = new TypeToken<GetAddCreditResult>() {
        };
        Ion.with(context).load(UrlGetAddCredit)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }
    public void getCarWashOne( FutureCallback<Response<GetCarWashOneResult>> callback) {
        TypeToken<GetCarWashOneResult> types = new TypeToken<GetCarWashOneResult>() {
        };
        Ion.with(context).load(UrlGetCarWash)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void checkMyOrder(String orderCode , FutureCallback<Response<CheckOrder>> callback){
        TypeToken<CheckOrder> types = new TypeToken<CheckOrder>() {
        };
        Ion.with(context).load(UrlCheckOrder)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("code",orderCode)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void addAddressToServer(AddressNew address, FutureCallback<Response<AddAddressResult>> callback) {
        TypeToken<AddAddressResult> types = new TypeToken<AddAddressResult>() {
        };
        Ion.with(context).load(UrlSendNewAddress)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("title", address.getTitle())
                .setBodyParameter("number", address.getPlaque())
                .setBodyParameter("unitNumber", address.getUnit())
                .setBodyParameter("body", address.getAddress())
                .setBodyParameter("city", address.getCity())
                .setBodyParameter("cityArea", address.getNeighborhood())
                .setBodyParameter("phone", address.getPhone())
                .setBodyParameter("lat", address.getLat())
                .setBodyParameter("long", address.getLon())
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void updateAddress(AddressNew address, FutureCallback<Response<DeleteResult>> callback) {
        TypeToken<DeleteResult> types = new TypeToken<DeleteResult>() {
        };
        Ion.with(context).load(UrlUpdateAddress)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("id", address.getId())
                .setBodyParameter("title", address.getTitle())
                .setBodyParameter("body", address.getAddress())
                .setBodyParameter("city", address.getCity())
                .setBodyParameter("cityarea", address.getNeighborhood())
                .setBodyParameter("phone", address.getPhone())
                .setBodyParameter("number", address.getPlaque())
                .setBodyParameter("unitnumber", address.getUnit())
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void accountRegisterUser(String fullName,String cellNumber,String reagentCode, FutureCallback<hoodadtech.com.sepita.model.serverResult.registerResult.RegisterResult> callback) {
        TypeToken<hoodadtech.com.sepita.model.serverResult.registerResult.RegisterResult> types = new TypeToken<hoodadtech.com.sepita.model.serverResult.registerResult.RegisterResult>() {
        };
        Ion.with(context).load(UrlTestAccountRegister)
                .setBodyParameter("fullName", fullName + "")
                .setBodyParameter("cellNumber", cellNumber + "")
                .setBodyParameter("reagentCode", reagentCode + "")
                .as(types)
                .setCallback(callback);
    }

    public void accountActivateUser(String tokenId,String activationCode,String deviceId,String deviceModel,String OsType,String OsVersion, FutureCallback<OperationResult<RegisterResult>> callback) {
        TypeToken<OperationResult<RegisterResult>> types = new TypeToken<OperationResult<RegisterResult>>() {
        };
        Ion.with(context).load(UrlAccountActivate)
                .setBodyParameter("tokenId", tokenId + "")
                .setBodyParameter("activationCode", activationCode + "")
                .setBodyParameter("deviceId", deviceId + "")
                .setBodyParameter("deviceModel", deviceModel + "")
                .setBodyParameter("OsType", OsType + "")
                .setBodyParameter("OsVersion", OsVersion + "")
                .as(types)
                .setCallback(callback);
    }

    public void accountLoginUser(String cellNumber,String password, FutureCallback<OperationResult<RegisterResult>> callback) {
        TypeToken<OperationResult<RegisterResult>> types = new TypeToken<OperationResult<RegisterResult>>() {
        };
        Ion.with(context).load(UrlAccountLogin)
                .setBodyParameter("cellNumber", cellNumber + "")
                .setBodyParameter("password", password + "")
                .as(types)
                .setCallback(callback);
    }
    public void accountLoginUser1(String cellNumber,String password, FutureCallback<Response<LoginResult>> callback) {
        TypeToken<LoginResult> types = new TypeToken<LoginResult>() {
        };
        Ion.with(context).load(UrlAccountLogin)
                .setBodyParameter("cellNumber", cellNumber + "")
                .setBodyParameter("password", password + "")
                .as(types)
                .withResponse()
                .setCallback(callback);
    }
    public void getToken(String cellNumber,String password, FutureCallback<Response<OperationResult<RegisterResult>>> callback) {
        TypeToken<OperationResult<RegisterResult>> types = new TypeToken<OperationResult<RegisterResult>>() {
        };
        Ion.with(context).load(UrlAccountLogin)
                .setBodyParameter("cellNumber", cellNumber + "")
                .setBodyParameter("password", password + "")
                .as(types)
                .withResponse()
                .setCallback(callback);
    }


    public void accountResendActivationCode(String tokenId, FutureCallback<OperationResult<RegisterResult>> callback) {
        TypeToken<OperationResult<RegisterResult>> types = new TypeToken<OperationResult<RegisterResult>>() {
        };
        Ion.with(context).load(UrlAccountResendActivationCode)
                .setBodyParameter("tokenId", tokenId + "")
                .as(types)
                .setCallback(callback);
    }

    public void getServices(FutureCallback<Response<OperationResult<List<Service>>>> callback) {
        TypeToken<OperationResult<List<Service>>>types = new TypeToken<OperationResult<List<Service>>>() {
        };
        Ion.with(context).load(UrlGetServices)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }


    /////profile

    public void getProfile(FutureCallback<Response<OperationResult<ProfileResult>>> callback) {
        TypeToken<OperationResult<ProfileResult>>types = new TypeToken<OperationResult<ProfileResult>>() {
        };
        Ion.with(context).load(UrlGetProfile)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }
    public void getProfileNew(FutureCallback<Response<ProfileGetResult>> callback) {
        TypeToken<ProfileGetResult> types = new TypeToken<ProfileGetResult>() {
        };
        Ion.with(context).load(UrlGetProfile)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void editProfile(ProfileRequest profileRequestOject, FutureCallback<Response<hoodadtech.com.sepita.model.serverResult.editProfileResult.EditProfileResult>> callback) {
        TypeToken<hoodadtech.com.sepita.model.serverResult.editProfileResult.EditProfileResult> types = new TypeToken<hoodadtech.com.sepita.model.serverResult.editProfileResult.EditProfileResult>() {
        };
        String profileRequest=new Gson().toJson(profileRequestOject);
        Ion.with(context).load(UrlEditProfile)
                .setHeader("Authorization",LoginToken)
                .setHeader("Content-Type", "application/json")
                .setStringBody(profileRequest)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void accountLogout( FutureCallback<Response<OperationResult<String>>> callback) {
        TypeToken<OperationResult<String>> types = new TypeToken<OperationResult<String>>() {
        };

        Ion.with(context).load(UrlAccountLogout)
                .setHeader("Authorization",LoginToken)
                .setHeader("Content-Type", "application/json")
                .setStringBody("")
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void getCredit(FutureCallback<OperationResult<String>>callback){
        TypeToken<OperationResult<String>> types = new TypeToken<OperationResult<String>>() {
        };

        Ion.with(context).load(UrlGetCredit)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .setCallback(callback);

    }

    public void getPrice(GetPrice_sender sender , FutureCallback<Response<GetPriceResult>> callback){
        TypeToken<GetPriceResult> types = new TypeToken<GetPriceResult>() {
        };
        String senderN=new Gson().toJson(sender);
        Ion.with(context).load(UrlGetPrice)
                .setHeader("Authorization",LoginToken)
                .setHeader("Content-Type", "application/json")
                .setStringBody(senderN)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }










    public void getOrderComment(FutureCallback<Response<OperationResult<OrderCommentResult>>> callback) {
        TypeToken<OperationResult<OrderCommentResult>>types = new TypeToken<OperationResult<OrderCommentResult>>() {
        };
        Ion.with(context).load(UrlGetOrderComment)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void postOrderCommetn(PostOrderCommentRequst postOrderCommentRequst, FutureCallback<OperationResult<String>> callback) {
        TypeToken<OperationResult<String>> types = new TypeToken<OperationResult<String>>() {
        };
        String profileRequest=new Gson().toJson(postOrderCommentRequst);
        Ion.with(context).load(UrlPostOrderComment)
                .setHeader("Authorization",LoginToken)
                .setHeader("Content-Type", "application/json")
                .setStringBody(profileRequest)
                .as(types)
                .setCallback(callback);
    }

    public void calculatePrice(CalculatePriceObject calculatePriceObject, FutureCallback<Response<OperationResult<CalculatePriceResult>>> callback) {
        TypeToken<OperationResult<CalculatePriceResult>> types = new TypeToken<OperationResult<CalculatePriceResult>>() {
        };
        String profileRequest=new Gson().toJson(calculatePriceObject);
        Ion.with(context).load(UrlPriceCalculation)
                .setHeader("Authorization",LoginToken)
                .setHeader("Content-Type", "application/json")
                .setStringBody(profileRequest)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }


    public void getServiceDetails(String serviceId, FutureCallback<Response<OperationResult<ServiceDetailResult>>> callback) {
        TypeToken<OperationResult<ServiceDetailResult>> types = new TypeToken<OperationResult<ServiceDetailResult>>() {
        };
        Ion.with(context).load(UrlGetServiceDetails+serviceId)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void discountCodeValidation(String code, FutureCallback<Response<OperationResult<DiscountResult>>> callback) {
        TypeToken<OperationResult<DiscountResult>> types = new TypeToken<OperationResult<DiscountResult>>() {
        };
        Ion.with(context).load(UrlDiscountCodeActivation)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("code", code + "")
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void postOrder(String postOrder, FutureCallback<OperationResult<PostOrderResult>> callback) {
        TypeToken<OperationResult<PostOrderResult>> types = new TypeToken<OperationResult<PostOrderResult>>() {
        };
        Log.i("kk","post1");
        Ion.with(context).load(UrlPostOrder)
                .setHeader("Authorization",LoginToken)
                .setHeader("Content-Type", "application/json")
                .setStringBody(postOrder)
                .as(types)
                .setCallback(callback);
    }

    public void getOrderList(String statusCode, FutureCallback<OperationResult<List<Order>>> callback) {
        TypeToken<OperationResult<List<Order>>> types = new TypeToken<OperationResult<List<Order>>>() {
        };
        Ion.with(context).load(UrlGetOrderList)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("statusCode", statusCode + "")
                .as(types)
                .setCallback(callback);
    }

    public void getOrderDetail(String orderId, FutureCallback<OperationResult<OrderDetailResult>> callback) {
        TypeToken<OperationResult<OrderDetailResult>> types = new TypeToken<OperationResult<OrderDetailResult>>() {
        };
        Ion.with(context).load(UrlGetOrderDetail)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("orderId", orderId + "")
                .as(types)
                .setCallback(callback);
    }

    public void cancelOrder(String orderId, FutureCallback<OperationResult<String>> callback) {
        TypeToken<OperationResult<String>> types = new TypeToken<OperationResult<String>>() {
        };
        Ion.with(context).load(UrlGetOrderCancel)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("orderId", orderId + "")
                .as(types)
                .setCallback(callback);
    }




    public void getVisitingTimes(FutureCallback<VisitingDatesTimes> callback) {
        TypeToken<VisitingDatesTimes> types = new TypeToken<VisitingDatesTimes>() {
        };
        Ion.with(context).load(UrlGetVisitingTimes)
                .as(types)
                .setCallback(callback);
    }



    public void getVisitingTimesNewNewNew(String service ,FutureCallback<Response<DateAndTimeResult>> callback) {
        TypeToken<DateAndTimeResult> types = new TypeToken<DateAndTimeResult>() {
        };
        Ion.with(context).load(UrlGetVisitingTimesNewNew+service)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }





    public void accountForgetPassword(String cellNumber,String deviceId,String deviceModel,String OsType,String OsVersion,
                                    FutureCallback<OperationResult<RegisterResult>> callback) {
        TypeToken<OperationResult<RegisterResult>> types = new TypeToken<OperationResult<RegisterResult>>() {
        };
        Ion.with(context).load(UrlAccountForgetPassword)
                .setBodyParameter("cellNumber", cellNumber + "")
                .setBodyParameter("deviceId", deviceId + "")
                .setBodyParameter("deviceModel", deviceModel + "")
                .setBodyParameter("OsType", OsType + "")
                .setBodyParameter("OsVersion", OsVersion + "")
                .as(types)
                .setCallback(callback);
        //Log.i("kk",cellNumber);
        //Log.i("kk",deviceId);
        //Log.i("kk",deviceModel);
        //Log.i("kk",OsType);
        //Log.i("kk",OsVersion);
    }


    //employee
    public void getEmplyeeDetailsById(String employeeId, FutureCallback<OperationResult<ResultEmployeeDetail>> callback) {
        TypeToken<OperationResult<ResultEmployeeDetail>> types = new TypeToken<OperationResult<ResultEmployeeDetail>>() {
        };
        Ion.with(context).load(UrlGetEmployeeDetails)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("employeeId", employeeId + "")
                .as(types)
                .setCallback(callback);
    }


    public void addEmplyeeToFavorites(String employeeId, FutureCallback<OperationResult<Boolean>> callback) {
        TypeToken<OperationResult<Boolean>> types = new TypeToken<OperationResult<Boolean>>() {
        };
        Ion.with(context).load(UrlAddToFavoriteEmplyoee)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("employeeId", employeeId + "")
                .as(types)
                .setCallback(callback);
    }

    public void removeEmplyeeFromFavorites(String employeeId, FutureCallback<OperationResult<Boolean>> callback) {
        TypeToken<OperationResult<Boolean>> types = new TypeToken<OperationResult<Boolean>>() {
        };
        Ion.with(context).load(UrlRemoveFromFavoriteEmplyoee)
                .setHeader("Authorization",LoginToken)
                .setBodyParameter("employeeId", employeeId + "")
                .as(types)
                .setCallback(callback);
    }


    public void getFavoriteEmplyoeesList(FutureCallback<Response<OperationResult<List<Worker>>>> callback) {
        TypeToken<OperationResult<List<Worker>>>types = new TypeToken<OperationResult<List<Worker>>>() {
        };
        Ion.with(context).load(UrlGetFavoriteEmplyoeesList)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }



    //carwash methods

    public void getCarwashServicesWithBrands(String serviceId,FutureCallback<Response<OperationResult<CarWashServiceDetailsResult>>> callback) {
        TypeToken<OperationResult<CarWashServiceDetailsResult>>types = new TypeToken<OperationResult<CarWashServiceDetailsResult>>() {
        };
        Ion.with(context).load(UrlGetCarBrands+serviceId)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }


    public void getCarModelByBrand(String brandId,FutureCallback<Response<OperationResult<List<CarModel>>>> callback) {
        TypeToken<OperationResult<List<CarModel>>>types = new TypeToken<OperationResult<List<CarModel>>>() {
        };
        Ion.with(context).load(UrlGetCarMoldesByBrand+brandId)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void deleteAddress(String id,FutureCallback<Response<DeleteResult>> callback) {
        TypeToken<DeleteResult>types = new TypeToken<DeleteResult>() {
        };
        Ion.with(context).load(UrlDeleteAddress+id)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }



    public void getInviteText(FutureCallback<Response<OperationResult<String>>> callback) {
        TypeToken<OperationResult<String>> types = new TypeToken<OperationResult<String>>() {
        };
        Ion.with(context).load(UrlGetInviteText)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }


    public void getAboutUs(FutureCallback<Response<AboutUsResult>> callback){
        TypeToken<AboutUsResult> types = new TypeToken<AboutUsResult>(){
        };
        Ion.with(context).load(UrlAboutUs)
                .setHeader("Authorization",LoginToken)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }

    public void postMessage(PostMessageObject postOrderCommentRequst, FutureCallback<Response<OperationResult<String>>> callback) {
        TypeToken<OperationResult<String>> types = new TypeToken<OperationResult<String>>() {
        };
        String profileRequest=new Gson().toJson(postOrderCommentRequst);
        Ion.with(context).load(UrlGetPostMessage)
                .setHeader("Authorization",LoginToken)
                .setHeader("Content-Type", "application/json")
                .setStringBody(profileRequest)
                .as(types)
                .withResponse()
                .setCallback(callback);
    }


    private void disableConnectionReuseIfNecessary() {
        // Work around pre-Froyo bugs in HTTP connection reuse.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");
        }
    }



    public boolean isOnDataConnection(Context ctx) {
        ConnectivityManager connec = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connec.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connec.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        // Check if wifi or mobile network is available or not. If any of them is
        // available or connected then it will return true, otherwise false;
        boolean isWifi = wifi.isConnected();
        boolean isMobile = mobile.isConnected();
        return isMobile;
    }



}