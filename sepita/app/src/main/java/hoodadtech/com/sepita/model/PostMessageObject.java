package hoodadtech.com.sepita.model;

public class PostMessageObject {

    private String subject;
    private String Body;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }
}
