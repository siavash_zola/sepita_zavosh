package hoodadtech.com.sepita.model;

import com.google.android.gms.maps.model.LatLng;

public class AddressSaved {
    private String title;
    private LatLng latLng;
    private String id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
