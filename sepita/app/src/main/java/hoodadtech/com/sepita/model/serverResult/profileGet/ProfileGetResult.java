package hoodadtech.com.sepita.model.serverResult.profileGet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.serverResult.ProfileResult;
import hoodadtech.com.sepita.model.serverResult.addAddress.Status;

public class ProfileGetResult {
    @SerializedName("result")
    @Expose
    public ProfileResult result;
    @SerializedName("status")
    @Expose
    public Status status;

    public ProfileResult getProfileResult() {
        return result;
    }

    public void setProfileResult(ProfileResult profileResult) {
        this.result = profileResult;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
