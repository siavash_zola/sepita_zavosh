package hoodadtech.com.sepita.model.serverResult.getAddCredit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import hoodadtech.com.sepita.model.Bank;

public class Result {
    @SerializedName("banks")
    @Expose
    private List<Bank> banks = null;
    @SerializedName("credit")
    @Expose
    private List<Credit> credit = null;
    @SerializedName("remainAmount")
    @Expose
    private String remainAmount;

    public List<Bank> getBanks() {
        return banks;
    }

    public void setBanks(List<Bank> banks) {
        this.banks = banks;
    }

    public List<Credit> getCredit() {
        return credit;
    }

    public void setCredit(List<Credit> credit) {
        this.credit = credit;
    }

    public String getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(String remainAmount) {
        this.remainAmount = remainAmount;
    }
}
