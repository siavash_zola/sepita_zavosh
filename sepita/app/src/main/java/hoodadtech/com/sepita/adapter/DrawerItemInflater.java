package hoodadtech.com.sepita.adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.DrawerItem;


public class DrawerItemInflater implements IAdapterViewInflater<DrawerItem> {

    @Override
    public View inflate(BaseInflaterAdapter<DrawerItem> adapter, int pos, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.drawer_list_item, parent, false);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final DrawerItem item = adapter.getTItem(pos);
        holder.updateDisplay(item);
        return convertView;
    }



    private class ViewHolder {
        private View m_rootView;
        private TextView titleTV;
        private ImageView iconTV;


        public ViewHolder(View rootView) {
            m_rootView = rootView;
            titleTV =  m_rootView.findViewById(R.id.titleTV);
            iconTV =  m_rootView.findViewById(R.id.drawer_icon);
            rootView.setTag(this);
        }

        public void updateDisplay(DrawerItem item) {
            titleTV.setText("   " + item.getTitle());
            //String title=m_rootView.getResources().getString(R.string.Icon_Home);
            //if (item.getSelected()) {
            //    m_rootView.setBackgroundColor(m_rootView.getResources().getColor(R.color.gray_light0));
            //} else
            //    m_rootView.setBackgroundColor(m_rootView.getResources().getColor(R.color.white));
////
            //switch (item.getId()){
//
            //    case 0:
            //        iconTV.setTextColor(m_rootView.getResources().getColor(R.color.nav_black));
            //        break;
            //    case 1:
            //        iconTV.setTextColor(m_rootView.getResources().getColor(R.color.nav_yellow));
            //        break;
            //    case 2:
            //        iconTV.setTextColor(m_rootView.getResources().getColor(R.color.nav_green));
            //        break;
            //    case 3:
            //        iconTV.setTextColor(m_rootView.getResources().getColor(R.color.nav_assists));
            //        break;
            //    case 4:
            //        iconTV.setTextColor(m_rootView.getResources().getColor(R.color.nav_about));
            //        break;
            //    case 5:
            //        iconTV.setTextColor(m_rootView.getResources().getColor(R.color.nav_blue));
            //        break;
            //    case 6:
            //        iconTV.setTextColor(m_rootView.getResources().getColor(R.color.nav_purple));
            //        break;
            //    case 7:
            //        iconTV.setTextColor(m_rootView.getResources().getColor(R.color.nav_red));
            //        break;
//
            //}

            //iconTV.setText(item.getIcon());
            iconTV.setImageResource(item.getIconResource());

            //iconTV.setTypeface(Typeface.createFromAsset(m_rootView.getContext().getAssets(), "icomoon.ttf"));
            titleTV.setTypeface(Typeface.createFromAsset(m_rootView.getContext().getAssets(), "IRAN-Sans.ttf"));


        }
    }


}


