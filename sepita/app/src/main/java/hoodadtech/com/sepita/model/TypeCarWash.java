package hoodadtech.com.sepita.model;

public class TypeCarWash {
    private String title;
    private String carwashServiceId;
    private boolean isSelected;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCarwashServiceId() {
        return carwashServiceId;
    }

    public void setCarwashServiceId(String carwashServiceId) {
        this.carwashServiceId = carwashServiceId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
