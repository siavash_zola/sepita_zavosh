package hoodadtech.com.sepita.utils;

import hoodadtech.com.sepita.model.AddressNew;

public interface AddressManager {
    void delete(AddressNew addressNew);
    void edit(AddressNew addressNew);
}
