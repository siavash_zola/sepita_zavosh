package hoodadtech.com.sepita.adapter.newAdapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.Item_history;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.view.activity.OrderDetailsActivity;

public class HistoryAdapter extends RecyclerView.Adapter{

    List<Item_history> list = new ArrayList<>();
    private Activity activity;
    private final int TYPE_HEADER = 0;
    private final int TYPE_FOOTER = 1;
    private final int TYPE_ITEM = 2;

    public HistoryAdapter(List<Item_history> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER){
            View headerItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_history, parent, false);
            return new HeaderViewHolder(headerItem);
        }
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
            return new ItemViewHolder(itemView);
        }
        if (viewType == TYPE_FOOTER){
            View space = LayoutInflater.from(parent.getContext()).inflate(R.layout.space, parent, false);
            return new HeaderViewHolder(space);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int p) {
        int position = p-1;
        if (holder instanceof ItemViewHolder){

            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            Item_history item = list.get(position);
            itemViewHolder.tv_title.setText(item.getServiceTitle());
            itemViewHolder.tv_numberOrder.setText(item.getOrderCode());
            itemViewHolder.tv_date.setText(item.getOrderDate());
            itemViewHolder.tv_city.setText(item.getCityArea());
            itemViewHolder.tv_paymentType.setText(item.getPaymentType());
            itemViewHolder.tv_status.setText(item.getStatusTitle());
            itemViewHolder.tv_hour.setText(item.getDuration());
            itemViewHolder.tv_price.setText(item.getAmount());

            int radius = convertDpToPixel(15,activity);

            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(Color.parseColor("#"+item.getStatusColor()));
            gradientDrawable.setCornerRadius(radius);
            gradientDrawable.setStroke(0, Color.parseColor("#"+item.getStatusColor()));

            itemViewHolder.linearLayout_status.setBackground(gradientDrawable);
            switch (item.getStatusCode()){
                case "1":
                    //itemViewHolder.linearLayout_status.setBackgroundResource(R.drawable.bg_button_silver);
                    itemViewHolder.tv_status.setTextColor(activity.getResources().getColor(R.color.text_color));
                    break;
                case "2":
                    //itemViewHolder.linearLayout_status.setBackgroundResource(R.drawable.bg_button_green);
                    itemViewHolder.tv_status.setTextColor(activity.getResources().getColor(R.color.white));
                    break;
                case "3":
                    //itemViewHolder.linearLayout_status.setBackgroundResource(R.drawable.bg_button_green);
                    itemViewHolder.tv_status.setTextColor(activity.getResources().getColor(R.color.white));
                    break;
                case "4":
                    //itemViewHolder.linearLayout_status.setBackgroundResource(R.drawable.bg_button_red);
                    itemViewHolder.tv_status.setTextColor(activity.getResources().getColor(R.color.white));
                    break;
            }



        }
    }

    @Override
    public int getItemCount() {
        return list.size()+2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }else if (position == list.size()+1){
            return TYPE_FOOTER;
        }else  {
            return TYPE_ITEM;
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        IranSansTextView tv_title,tv_numberOrder,tv_date,tv_city,tv_hour,tv_price,tv_paymentType,tv_status;
        LinearLayout linearLayout_status,linearLayout_showDetail;
        public ItemViewHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_numberOrder = itemView.findViewById(R.id.tv_numberOrder);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_city = itemView.findViewById(R.id.tv_city);
            tv_hour = itemView.findViewById(R.id.tv_hour);
            tv_price = itemView.findViewById(R.id.tv_price);
            tv_paymentType = itemView.findViewById(R.id.tv_paymentType);
            tv_status = itemView.findViewById(R.id.tv_status);
            linearLayout_status = itemView.findViewById(R.id.linearLayout_status);
            linearLayout_showDetail = itemView.findViewById(R.id.linearLayout_showDetail);

            linearLayout_showDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Log.i("kk",getAdapterPosition()-1+"");
                    Intent intent = new Intent(activity, OrderDetailsActivity.class);
                    Item_history item_history = list.get(getAdapterPosition() - 1);
                    intent.putExtra("status",item_history.getStatusCode());
                    intent.putExtra("id",item_history.getOrderId());
                    intent.putExtra("from","history");
                    activity.startActivity(intent);


                }
            });
        }
    }
    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static int convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }
}
