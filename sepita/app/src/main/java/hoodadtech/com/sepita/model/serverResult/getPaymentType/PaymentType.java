package hoodadtech.com.sepita.model.serverResult.getPaymentType;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentType {
    @SerializedName("paymetTypeId")
    @Expose
    private String paymetTypeId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("isActive")
    @Expose
    private Boolean isActive;

    public String getPaymetTypeId() {
        return paymetTypeId;
    }

    public void setPaymetTypeId(String paymetTypeId) {
        this.paymetTypeId = paymetTypeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

}

