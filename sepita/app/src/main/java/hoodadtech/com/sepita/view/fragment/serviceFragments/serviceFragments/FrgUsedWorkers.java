package hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.MultiRecyclerAdapter;
import hoodadtech.com.sepita.adapter.listItems.DetailsItem;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.Service;
import hoodadtech.com.sepita.model.ServiceDetail;
import hoodadtech.com.sepita.model.ServiceDetails;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.NetworkUtil;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;

public class FrgUsedWorkers extends Fragment implements DetailsItem.ItemClickListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    Button btn_submit;
    TextView time, price;
    int firstCounter = 0;
    int Ttime = 0;
    int Pprice = 0;
    Server server;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    private MultiRecyclerAdapter multiRecyclerAdapter;
    private List<MultiAdapterItem> homeItems;
    Service service;
    View view;
    List<ServiceDetails> serviceDetailsList;
    int servieQuantity;
    boolean dataLoaded = false;
    FileUtility fileUtility;
    PostOrderObject postOrderObject;
    ProgressBar progressBar;

    private SwipeRefreshLayout swipeContainer;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        if (view != null)
            return view;

        view = inflater.inflate(R.layout.frg_details_first, container, false);
        serviceDetailsList = new ArrayList<>();
        fileUtility = new FileUtility(getContext());
        postOrderObject = fileUtility.getPostOrderObject();
        service = new Gson().fromJson(getArguments().getString("service"), Service.class);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        //TextView title = getActivity().findViewById(R.id.toolbar_text);
        //title.setText(service.getTitle());

        server = Server.getInstance(getContext());


        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });
        initView(view);
        return view;
    }

    private void initView(View view) {
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.accent), android.graphics.PorterDuff.Mode.SRC_ATOP);


        IranSansTextView title = view.findViewById(R.id.header_title);
        title.setText(service.getTitle());
        btn_submit = view.findViewById(R.id.btn_submit);
        time = view.findViewById(R.id.time);
        price = view.findViewById(R.id.price);
        swipeContainer = view.findViewById(R.id.swiperefresh);
        swipeContainer.setOnRefreshListener(this);
        time.setText(StringUtility.numberEn2Fa(String.valueOf(firstCounter)));
        price.setText(StringUtility.numberEn2Fa(String.valueOf(firstCounter)));
        btn_submit.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getContext());
        multiRecyclerAdapter = new MultiRecyclerAdapter(getContext());
        recyclerView.setAdapter(multiRecyclerAdapter);
        recyclerView.setLayoutManager(layoutManager);
        setData(false);


    }

    private void setData(boolean isRefresh) {
        if (!isRefresh)
            progressBar.setVisibility(View.VISIBLE);
        server.getServiceDetails(service.getId(), (e, result) -> {
            if (isAdded()) {
                swipeContainer.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                if (e != null) {
                    CustomSnackbar.showSnackBar(view);

                } else if (OperationResult.isOk(result.getResult(), e)) {
                    dataLoaded = true;
                    Ttime = result.getResult().getResult().getBaseDuration();
                    Pprice = result.getResult().getResult().getBaseAmount();
                    homeItems = new ArrayList<>();
                    List<ServiceDetail> hhomeItems = result.getResult().getResult().getServiceDetails();

                    time.setText(StringUtility.numberEn2Fa(String.valueOf(result.getResult().getResult().getBaseDuration())));
                    price.setText(StringUtility.numberEn2Fa(String.valueOf(result.getResult().getResult().getBaseAmount())));
                    for (int i = 0; i < hhomeItems.size(); i++) {
                        ServiceDetails serviceDetails = new ServiceDetails();
                        serviceDetails.setServiceDetailId(hhomeItems.get(i).getId());
                        serviceDetails.setQuantity(String.valueOf(hhomeItems.get(i).getSeedValue()));
                        serviceDetailsList.add(serviceDetails);
                        DetailsItem listItem = new DetailsItem();
                        listItem.setItem(hhomeItems.get(i));
                        listItem.setItemClickListener(this);
                        homeItems.add(listItem);
                    }
                    postOrderObject.setServiceDetails(serviceDetailsList);
                    multiRecyclerAdapter.setItems(homeItems);
                } else if (result.getHeaders().code() == 401) {
                    getToken();

                } else if (result.getResult() != null && result.getResult().getStatus() != null) {

                    Toast.makeText(getContext(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();

                }
            }


        });
    }


    private void getToken() {
        progressBar.setVisibility(View.VISIBLE);
        Server server = Server.getInstance(getContext());
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getContext()), UserObject.class);
        server.accountLoginUser(userObject.getCellNumber(), userObject.getActivationCode(), (e, result) -> {
            progressBar.setVisibility(View.INVISIBLE);
            if (e != null) {
                CustomSnackbar.showSnackBar(view);
            } else if (OperationResult.isOk(result, e)) {
                SharedPreference sharedPreference = new SharedPreference();
                sharedPreference.saveLoginToken(getContext(), result.getResult().getTokenId());
                Server server1 = Server.getInstance(getContext(), sharedPreference.getLoginTokenValue(getContext()));
                server1.getServiceDetails(service.getId(), (e1, result1) -> {
                    if (isAdded()) {
                        progressBar.setVisibility(View.GONE);
                        if (e1 != null) {
                            CustomSnackbar.showSnackBar(view);
                        } else if (OperationResult.isOk(result1.getResult(), e1)) {
                            dataLoaded = true;
                            Ttime = result1.getResult().getResult().getBaseDuration();
                            Pprice = result1.getResult().getResult().getBaseAmount();
                            homeItems = new ArrayList<>();
                            List<ServiceDetail> hhomeItems = result1.getResult().getResult().getServiceDetails();

                            time.setText(StringUtility.numberEn2Fa(String.valueOf(result1.getResult().getResult().getBaseDuration())));
                            price.setText(StringUtility.numberEn2Fa(String.valueOf(result1.getResult().getResult().getBaseAmount())));
                            for (int i = 0; i < hhomeItems.size(); i++) {
                                ServiceDetails serviceDetails = new ServiceDetails();
                                serviceDetails.setServiceDetailId(hhomeItems.get(i).getId());
                                serviceDetails.setQuantity(String.valueOf(hhomeItems.get(i).getSeedValue()));
                                serviceDetailsList.add(serviceDetails);
                                DetailsItem listItem = new DetailsItem();
                                listItem.setItem(hhomeItems.get(i));
                                listItem.setItemClickListener(this);
                                homeItems.add(listItem);
                            }
                            postOrderObject.setServiceDetails(serviceDetailsList);
                            multiRecyclerAdapter.setItems(homeItems);
                        } else if (result != null && result.getStatus() != null) {

                            Toast.makeText(getContext(), result.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();

                        }
                    }


                });


            } else {
                Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_submit:
                if (!NetworkUtil.isOnline(getContext())) {
                    CustomSnackbar.showSnackBar(btn_submit);
                } else if (Ttime == 0)
                    Toast.makeText(getContext(), "موردی انتخاب نشده است", Toast.LENGTH_SHORT).show();
                else if (dataLoaded) {
                    try {
                        postOrderObject.setServiceDetails(serviceDetailsList);
                        postOrderObject.setDuration(Ttime);
                        postOrderObject.setNetAmount(String.valueOf(Pprice));
                        fileUtility.writePostOrderObject(postOrderObject);

                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        String frgTag = getActivity().getClass().getSimpleName();
                        FrgSecondDetail fragment = new FrgSecondDetail();
                        Bundle bundle = new Bundle();
                        bundle.putInt("time", Ttime);
                        bundle.putInt("price", Pprice);
                        fragment.setArguments(bundle);
                        fragmentTransaction.replace(R.id.fragment_container, fragment, frgTag);
                        fragmentTransaction.addToBackStack(frgTag);
                        fragmentTransaction.commit();
                    } catch (Exception e) {
                    }

                }

                break;

//

        }

    }

    @Override
    public void onClick(ServiceDetail item, View view, int timedd, int pricedd, int quantity) {
        switch (view.getId()) {
            case R.id.minuse_one:
                if (Ttime - timedd >= 4)
                    Ttime = Ttime - timedd;
                if (Pprice - pricedd >= 50000)
                    Pprice = Pprice - pricedd;
                time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
                price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
                for (int i = 0; i < serviceDetailsList.size(); i++) {
                    if (serviceDetailsList.get(i).getServiceDetailId() == item.getId()) {
                        serviceDetailsList.get(i).setQuantity(String.valueOf(quantity));
                    }
                }
                break;
            case R.id.plus_one:
                Ttime = Ttime + timedd;
                Pprice = Pprice + pricedd;
                time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
                price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));

                for (int i = 0; i < serviceDetailsList.size(); i++) {
                    if (serviceDetailsList.get(i).getServiceDetailId() == item.getId()) {
                        serviceDetailsList.get(i).setQuantity(String.valueOf(quantity));
                    }
                }
                break;
        }
    }

    @Override
    public void onRefresh() {
        setData(true);
    }
}
