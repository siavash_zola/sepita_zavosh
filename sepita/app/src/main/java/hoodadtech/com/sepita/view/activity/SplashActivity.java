package hoodadtech.com.sepita.view.activity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.Timer;
import java.util.TimerTask;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.MutedVideoView;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {
    protected boolean active = true;
    protected int splashTime = 10000;
    //ImageView imageView;
    Animation slideUpAnimation, slideDownAnimation, fadeIn;
    //ImageView titleTV;
    Timer timer;
    IranSansTextView TV_retry;
    RelativeLayout no_internet_layout;



    VideoView videoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        reference();
        delay();

        //videoView = findViewById(R.id.video);
        //Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.splash);
        //videoView.setVideoURI(uri);
        //videoView.start();
        //videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
        //    public void onCompletion(MediaPlayer mp) {
        //        //startNextActivity();
        //        //Toast.makeText(SplashActivity.this,"end",Toast.LENGTH_SHORT).show();
        //        if (!isOnline()){
        //            View parentLayout = findViewById(android.R.id.content);
        //            CustomSnackbar.showSnackBar(parentLayout);
        //            TV_retry.setVisibility(View.VISIBLE);
//
        //        }else {
        //            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        //            finish();
        //        }
        //    }
        //});

        MutedVideoView vView = (MutedVideoView) findViewById(R.id.video_view);
        Uri video = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.splash);

        if (vView != null) {
            vView.setVideoURI(video);
            vView.setZOrderOnTop(true);
            vView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    if (!isOnline()){
                        View parentLayout = findViewById(android.R.id.content);
                        CustomSnackbar.showSnackBar(parentLayout);
                        TV_retry.setVisibility(View.VISIBLE);

                    }else {
                        jump();
                    }

                }
            });


            vView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                    if (!isOnline()){
                        View parentLayout = findViewById(android.R.id.content);
                        CustomSnackbar.showSnackBar(parentLayout);
                        TV_retry.setVisibility(View.VISIBLE);

                    }else {
                        jump();
                    }
                    return false;
                }
            });

            vView.start();

        }else{

            if (!isOnline()){
                View parentLayout = findViewById(android.R.id.content);
                CustomSnackbar.showSnackBar(parentLayout);
                TV_retry.setVisibility(View.VISIBLE);

            }else {
                jump();
            }
        }
    }

    private void jump() {

        // Jump to your Next Activity or MainActivity
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);

        finish();

    }

    private void delay() {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            //imageView.startAnimation(slideUpAnimation);
            //titleTV.setVisibility(View.VISIBLE);
            //titleTV.startAnimation(fadeIn);
            if (!isOnline()){
                View parentLayout = findViewById(android.R.id.content);
                CustomSnackbar.showSnackBar(parentLayout);
                TV_retry.setVisibility(View.VISIBLE);
                timer.cancel();

            }
        }, 1500);

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, 4000);
    }

    private void reference() {
        //imageView = view.findViewById(R.id.img);
        //titleTV = view.findViewById(R.id.titleTV);
        TV_retry = findViewById(R.id.TV_retry);
        no_internet_layout = findViewById(R.id.no_internet_layout);
        TV_retry.setOnClickListener(this);

        //slideUpAnimation = AnimationUtils.loadAnimation(getContext(),
        //        R.anim.slide_up_animation);
//
        //slideDownAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down_animation);
        //fadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
    }
    @Override
    public void onPause() {
        super.onPause();
        if (timer != null)
            timer.cancel();
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.TV_retry:
                if(isOnline()){
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                    TV_retry.setVisibility(View.GONE);
                }
                else CustomSnackbar.showSnackBar(view);


                break;
        }
    }
}
