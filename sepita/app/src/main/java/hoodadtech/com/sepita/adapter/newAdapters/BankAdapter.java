package hoodadtech.com.sepita.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.koushikdutta.ion.Ion;

import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.AddressSaved;
import hoodadtech.com.sepita.model.Bank;
import hoodadtech.com.sepita.utils.MyClickListener;
import hoodadtech.com.sepita.utils.OnItemLatLonClickListener;

public class BankAdapter extends RecyclerView.Adapter{
    private List<Bank> list;
    private Activity activity;
    private final int TYPE_HEADER = 0;
    private final int TYPE_FOOTER = 1;
    private final int TYPE_ITEM = 2;
    public MyClickListener listener;

    public BankAdapter(List<Bank> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bank, parent, false);
            return new ItemViewHolder(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        Bank bank = list.get(position);
        if (bank.isSelected()){
            itemViewHolder.selector.setBackgroundResource(R.drawable.bg_button_silver);
        }else {
            itemViewHolder.selector.setBackgroundColor(activity.getResources().getColor(android.R.color.transparent));
        }
        Ion.with(activity).load(bank.getLogoUrl()).intoImageView(itemViewHolder.iv_logo);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_logo;
        LinearLayout selector;
        public ItemViewHolder(View itemView) {
            super(itemView);
            iv_logo = itemView.findViewById(R.id.iv_logo);
            selector = itemView.findViewById(R.id.selector);

            selector.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.itemClicked(getAdapterPosition());
                }
            });
        }
    }
}
