package hoodadtech.com.sepita.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.newAdapters.BankAdapter;
import hoodadtech.com.sepita.model.AddressNew;
import hoodadtech.com.sepita.model.Bank;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.getAddCredit.Credit;
import hoodadtech.com.sepita.model.serverResult.getAddCredit.GetAddCreditResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansEditText;
import hoodadtech.com.sepita.utils.MyClickListener;
import hoodadtech.com.sepita.utils.PublicMethod;
import hoodadtech.com.sepita.utils.SharedPreference;

public class IncreaseCreditActivity extends AppCompatActivity implements MyClickListener{
    private List<Bank> banks ;
    private LinearLayout linearLayout_back,linearLayout_payment,price0,price1,price2,price3,price4;
    private IranSansTextView tv_price0,tv_price1,tv_price2,tv_price3,tv_price4,tv_toman0,tv_toman1,tv_toman2,tv_toman3,tv_toman4,myCredit;
    private IransansEditText ev_price;
    private BankAdapter adapter;
    private Bank bankSelected;
    private List<Credit> credits;
    private Credit creditSelected = null;
    private ProgressBar loader;
    private LinearLayout linearBox;
    private Server server;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_increase_credit);
        reference();

    }

    private void reference() {

        server = Server.getInstance(this);
        loadData(server);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        banks = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.recyclerView_bank);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new BankAdapter(banks,this);
        adapter.listener = this;
        recyclerView.setAdapter(adapter);

        linearLayout_back = findViewById(R.id.linearLayout_back);
        linearLayout_payment = findViewById(R.id.linearLayout_payment);
        myCredit = findViewById(R.id.tv_credit);
        price0 = findViewById(R.id.price0);
        price1 = findViewById(R.id.price1);
        price2 = findViewById(R.id.price2);
        price3 = findViewById(R.id.price3);
        price4 = findViewById(R.id.price4);
        tv_price0 = findViewById(R.id.tv_price0);
        tv_price1 = findViewById(R.id.tv_price1);
        tv_price2 = findViewById(R.id.tv_price2);
        tv_price3 = findViewById(R.id.tv_price3);
        tv_price4 = findViewById(R.id.tv_price4);
        tv_toman0 = findViewById(R.id.tv_toman0);
        tv_toman1 = findViewById(R.id.tv_toman1);
        tv_toman2 = findViewById(R.id.tv_toman2);
        tv_toman3 = findViewById(R.id.tv_toman3);
        tv_toman4 = findViewById(R.id.tv_toman4);
        ev_price = findViewById(R.id.ev_price);
        loader = findViewById(R.id.loader);
        linearBox = findViewById(R.id.linearBox);
        loader.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.white), android.graphics.PorterDuff.Mode.SRC_ATOP);

        listener();
    }

    private void listener() {
        linearLayout_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        linearLayout_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (creditSelected != null){
                    //az credit begir bede
                }else {
                    //az edtexto check kon
                }
            }
        });
        price0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                creditSelected = credits.get(0);
                ev_price.setText(PublicMethod.convert(creditSelected.getAmount()));

                price0.setBackgroundResource(R.drawable.bg_button_gray);
                price1.setBackgroundResource(R.drawable.bg_button_silver);
                price2.setBackgroundResource(R.drawable.bg_button_silver);
                price3.setBackgroundResource(R.drawable.bg_button_silver);
                price4.setBackgroundResource(R.drawable.bg_button_silver);

                tv_price0.setTextColor(getResources().getColor(R.color.white));
                tv_price1.setTextColor(getResources().getColor(R.color.text_color));
                tv_price2.setTextColor(getResources().getColor(R.color.text_color));
                tv_price3.setTextColor(getResources().getColor(R.color.text_color));
                tv_price4.setTextColor(getResources().getColor(R.color.text_color));

                tv_toman0.setTextColor(getResources().getColor(R.color.white));
                tv_toman1.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman2.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman3.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman4.setTextColor(getResources().getColor(R.color.text_color));
            }
        });
        price1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                creditSelected = credits.get(1);
                ev_price.setText(PublicMethod.convert(creditSelected.getAmount()));

                price0.setBackgroundResource(R.drawable.bg_button_silver);
                price1.setBackgroundResource(R.drawable.bg_button_gray);
                price2.setBackgroundResource(R.drawable.bg_button_silver);
                price3.setBackgroundResource(R.drawable.bg_button_silver);
                price4.setBackgroundResource(R.drawable.bg_button_silver);

                tv_price0.setTextColor(getResources().getColor(R.color.text_color));
                tv_price1.setTextColor(getResources().getColor(R.color.white));
                tv_price2.setTextColor(getResources().getColor(R.color.text_color));
                tv_price3.setTextColor(getResources().getColor(R.color.text_color));
                tv_price4.setTextColor(getResources().getColor(R.color.text_color));

                tv_toman0.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman1.setTextColor(getResources().getColor(R.color.white));
                tv_toman2.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman3.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman4.setTextColor(getResources().getColor(R.color.text_color));
            }
        });
        price2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                creditSelected = credits.get(2);
                ev_price.setText(PublicMethod.convert(creditSelected.getAmount()));

                price0.setBackgroundResource(R.drawable.bg_button_silver);
                price1.setBackgroundResource(R.drawable.bg_button_silver);
                price2.setBackgroundResource(R.drawable.bg_button_gray);
                price3.setBackgroundResource(R.drawable.bg_button_silver);
                price4.setBackgroundResource(R.drawable.bg_button_silver);

                tv_price0.setTextColor(getResources().getColor(R.color.text_color));
                tv_price1.setTextColor(getResources().getColor(R.color.text_color));
                tv_price2.setTextColor(getResources().getColor(R.color.white));
                tv_price3.setTextColor(getResources().getColor(R.color.text_color));
                tv_price4.setTextColor(getResources().getColor(R.color.text_color));

                tv_toman0.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman1.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman2.setTextColor(getResources().getColor(R.color.white));
                tv_toman3.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman4.setTextColor(getResources().getColor(R.color.text_color));
            }
        });
        price3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                creditSelected = credits.get(3);
                ev_price.setText(PublicMethod.convert(creditSelected.getAmount()));

                price0.setBackgroundResource(R.drawable.bg_button_silver);
                price1.setBackgroundResource(R.drawable.bg_button_silver);
                price2.setBackgroundResource(R.drawable.bg_button_silver);
                price3.setBackgroundResource(R.drawable.bg_button_gray);
                price4.setBackgroundResource(R.drawable.bg_button_silver);

                tv_price0.setTextColor(getResources().getColor(R.color.text_color));
                tv_price1.setTextColor(getResources().getColor(R.color.text_color));
                tv_price2.setTextColor(getResources().getColor(R.color.text_color));
                tv_price3.setTextColor(getResources().getColor(R.color.white));
                tv_price4.setTextColor(getResources().getColor(R.color.text_color));

                tv_toman0.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman1.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman2.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman3.setTextColor(getResources().getColor(R.color.white));
                tv_toman4.setTextColor(getResources().getColor(R.color.text_color));
            }
        });
        price4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                creditSelected = credits.get(4);
                ev_price.setText(PublicMethod.convert(creditSelected.getAmount()));

                price0.setBackgroundResource(R.drawable.bg_button_silver);
                price1.setBackgroundResource(R.drawable.bg_button_silver);
                price2.setBackgroundResource(R.drawable.bg_button_silver);
                price3.setBackgroundResource(R.drawable.bg_button_silver);
                price4.setBackgroundResource(R.drawable.bg_button_gray);

                tv_price0.setTextColor(getResources().getColor(R.color.text_color));
                tv_price1.setTextColor(getResources().getColor(R.color.text_color));
                tv_price2.setTextColor(getResources().getColor(R.color.text_color));
                tv_price3.setTextColor(getResources().getColor(R.color.text_color));
                tv_price4.setTextColor(getResources().getColor(R.color.white));

                tv_toman0.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman1.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman2.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman3.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman4.setTextColor(getResources().getColor(R.color.white));
            }
        });
        ev_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                creditSelected = null;
                price0.setBackgroundResource(R.drawable.bg_button_silver);
                price1.setBackgroundResource(R.drawable.bg_button_silver);
                price2.setBackgroundResource(R.drawable.bg_button_silver);
                price3.setBackgroundResource(R.drawable.bg_button_silver);
                price4.setBackgroundResource(R.drawable.bg_button_silver);

                tv_price0.setTextColor(getResources().getColor(R.color.text_color));
                tv_price1.setTextColor(getResources().getColor(R.color.text_color));
                tv_price2.setTextColor(getResources().getColor(R.color.text_color));
                tv_price3.setTextColor(getResources().getColor(R.color.text_color));
                tv_price4.setTextColor(getResources().getColor(R.color.text_color));

                tv_toman0.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman1.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman2.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman3.setTextColor(getResources().getColor(R.color.text_color));
                tv_toman4.setTextColor(getResources().getColor(R.color.text_color));
            }
        });


    }

    @Override
    public void itemClicked(int position) {
        for (int i = 0; i < banks.size(); i++) {
            Bank bank = banks.get(i);
            if (i == position){
                bank.setSelected(true);
                bankSelected = bank;

            }else {
                bank.setSelected(false);
            }
        }

        adapter.notifyDataSetChanged();
    }

    public void loadData(Server server){
        server.getAddCredit(new FutureCallback<Response<GetAddCreditResult>>() {
            @Override
            public void onCompleted(Exception e, Response<GetAddCreditResult> result) {
                if (result != null){
                    if (result.getHeaders().code() == 401){
                        getToken();
                    }else if (result.getHeaders().code() == 200){
                        if (result.getResult().getStatus().getStatusCode() == 16){
                            getToken();
                        }else if (result.getResult().getStatus().getIsSuccess()){
                            loader.setVisibility(View.INVISIBLE);
                            linearBox.setVisibility(View.VISIBLE);
                            credits = result.getResult().getResult().getCredit();
                            tv_price0.setText(PublicMethod.convert(credits.get(0).getAmount()));
                            tv_price1.setText(PublicMethod.convert(credits.get(1).getAmount()));
                            tv_price2.setText(PublicMethod.convert(credits.get(2).getAmount()));
                            tv_price3.setText(PublicMethod.convert(credits.get(3).getAmount()));
                            tv_price4.setText(PublicMethod.convert(credits.get(4).getAmount()));

                            banks.clear();
                            for (int i = 0; i < result.getResult().getResult().getBanks().size(); i++) {
                                banks.add(result.getResult().getResult().getBanks().get(i));
                            }
                            adapter.notifyDataSetChanged();
                            myCredit.setText(PublicMethod.convert(result.getResult().getResult().getRemainAmount()));
                        }else {
                            Toast.makeText(IncreaseCreditActivity.this,result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(IncreaseCreditActivity.this,getString(R.string.error),Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(IncreaseCreditActivity.this,getString(R.string.checkConnection),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void getToken(){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(IncreaseCreditActivity.this), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(IncreaseCreditActivity.this, result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(IncreaseCreditActivity.this, sharedPreference.getLoginTokenValue(IncreaseCreditActivity.this));

                        loadData(server1);
                    }else {
                        Toast.makeText(IncreaseCreditActivity.this, result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(IncreaseCreditActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //isSendRequest = false;
                }
            }
        });
    }
}
