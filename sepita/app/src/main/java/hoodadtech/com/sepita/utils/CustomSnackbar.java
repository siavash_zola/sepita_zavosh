package hoodadtech.com.sepita.utils;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import hoodadtech.com.sepita.R;

/**
 * Created by Milad on 2/20/2018.
 */

public class CustomSnackbar {
    View view;
    public static void showSnackBar(View view){
        TSnackbar snackbar = TSnackbar
                .make(view, "", TSnackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);

       snackbar.setIconRight(R.drawable.icondelete, 48); //Resize to bigger dp

        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#29bdbe"));
        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setText(  " عدم دسترسی به اینترنت"+" \n" +"اتصال اینترنت خود را بررسی کنید" );
        textView.setPadding(0,-300,0,-300);


        textView.setTextSize(15);
        Typeface face= Typeface.createFromAsset(view.getContext().getAssets(), "IRAN-Sans.ttf");
        textView.setTypeface(face);

        try {
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) snackbarView.getLayoutParams();
            params.setMargins(params.leftMargin + 0,
                    params.topMargin+100,
                    params.rightMargin + 0,
                    params.bottomMargin + 0);

            snackbarView.setLayoutParams(params);
        }catch (Exception e){

        }


        snackbar.show();
    }
}
