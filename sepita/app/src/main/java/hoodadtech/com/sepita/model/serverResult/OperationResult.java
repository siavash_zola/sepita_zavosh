package hoodadtech.com.sepita.model.serverResult;

public class OperationResult<ResultType> {
    private OperationStatus status;
    public ResultType result;

    public ResultType getResult() {
        return result;
    }

    public void setResult(ResultType result) {
        this.result = result;
    }

    public OperationResult(Exception ex) {
        status = new OperationStatus(ex);
    }

    public OperationResult() {
    }

    public static boolean isOk(OperationResult r,Exception e) {

        return e==null && r != null && r.getStatus()!=null && r.getStatus().isSuccess() && r.getResult() != null;
    }

    public static boolean isNotValid(OperationResult r,Exception e)
    {
        if (e==null &&r!=null&& r.getStatus() !=null)
            if (r.getStatus().getStatusCode() == 3 ||r.getStatus().getStatusCode() == 4)
                return true;
        return false;
    }


    public void setStatus(OperationStatus status) {
        this.status = status;
    }

    public OperationStatus getStatus() {
        return this.status;
    }
}
