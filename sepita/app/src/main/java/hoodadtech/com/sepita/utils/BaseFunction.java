package hoodadtech.com.sepita.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by pcc on 11/9/2015.
 */
public class BaseFunction {

    public String readJson(Context context, String filePath)
    {
        StringBuffer sb = new StringBuffer();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(context.getAssets().open(filePath)));
            String temp;
            while ((temp = br.readLine()) != null)
                sb.append(temp);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close(); // stop reading
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }




    public  boolean assetExists(Context context, String path) {
        boolean bAssetOk = false;
        try {
            InputStream stream = context.getAssets().open(path);
            stream.close();
            bAssetOk = true;
        } catch (FileNotFoundException e) {
            Log.w("IOUtilities", "assetExists failed: "+e.toString());
        } catch (IOException e) {
            Log.w("IOUtilities", "assetExists failed: "+e.toString());
        }
        return bAssetOk;
    }


    public  void setToolbarColor(Toolbar toolbar, int actionBarColor, int statusBarColor, Activity activity)
    {
        toolbar.setBackgroundColor(actionBarColor);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(statusBarColor);
        }
    }
}
