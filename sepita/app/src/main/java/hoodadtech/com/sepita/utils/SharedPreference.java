package hoodadtech.com.sepita.utils;

/**
 * Created by milad on 8/24/2015.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreference {

    public static final String PREFS_NAME = "MYPREF";
    public static final String FULL_NAME = "FULL_NAME";
    public static final String IMAGE_PROFILE = "IMAGE_PROFILE";
    public static final String CREDIT = "CREDIT";
    public static final String MESSAGE = "MESSAGE";
    public static final String TITLE_MESSAGE = "TITLE_MESSAGE";

    public static final String LoginToken = "LoginToken";
    public static final String TempLoginToken = "TempLoginToken";
    public static final String UserObject="UserObject";
    public static final String CodeTekhfif="CodeTekhfif";
    public static final String IsFirstUse="IsFirstUse";
    public static final String noMiniplayer = "noMiniplayer";
    public static final String RadioAutoPaly="RadioAutoPaly";
    public static final String firstAppLaunch="AppLaunch";
    public static final String FirstAppLunch_PREFS_NAME = "FirstAppLunch_PREFS_NAME";
    public static final String HuaweiUser_PREFS_NAME = "HuaweiUser_PREFS_NAME";



    public SharedPreference() {
        super();
    }

    public void saveLoginToken(Context context, String text) {
        SharedPreferences sharedPreferences;
        Editor editor;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(LoginToken, text);
        editor.apply();
    }

    public void saveTempLoginToken(Context context, String text) {
        SharedPreferences sharedPreferences;
        Editor editor;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(TempLoginToken, text);
        editor.apply();
    }

    public void saveCodeTakhfif(Context context, String text) {
        SharedPreferences sharedPreferences;
        Editor editor;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(CodeTekhfif, text);
        editor.apply();
    }

    public void saveCredit(Context context, String credit) {
        SharedPreferences sharedPreferences;
        Editor editor;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(CREDIT, credit);
        editor.apply();
    }

    public void saveFullName(Context context, String fullName){
        SharedPreferences sharedPreferences;
        Editor editor;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(FULL_NAME, fullName);
        editor.apply();
    }

    public void saveImageProfile(Context context, String img){
        SharedPreferences sharedPreferences;
        Editor editor;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(IMAGE_PROFILE, img);
        editor.apply();
    }

    public String getCodeTekhfif(Context context) {
        SharedPreferences sharedPreferences;
        String text;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = sharedPreferences.getString(CodeTekhfif, "0");
        return text;
    }

    public void saveIsFirstUse(Context context, boolean text) {
        SharedPreferences sharedPreferences;
        Editor editor;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putBoolean(IsFirstUse, text);
        editor.apply();
    }

    public boolean getIsFirstUse(Context context) {
        SharedPreferences sharedPreferences;
        boolean text;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = sharedPreferences.getBoolean(IsFirstUse,false);
        return text;
    }

    public String getLoginTokenValue(Context context) {
        SharedPreferences sharedPreferences;
        String text;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = sharedPreferences.getString(LoginToken, null);
        return text;
    }

    public String getFullName (Context context){
        SharedPreferences sharedPreferences;
        String text;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = sharedPreferences.getString(FULL_NAME, null);
        return text;
    }

    public String getCredit (Context context){
        SharedPreferences sharedPreferences;
        String text;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = sharedPreferences.getString(CREDIT, null);
        return text;
    }

    public String getImageProfile (Context context){
        SharedPreferences sharedPreferences;
        String text;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = sharedPreferences.getString(IMAGE_PROFILE, null);
        return text;
    }

    public void saveUserObject(Context context, String text) {
        SharedPreferences sharedPreferences;
        Editor editor;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(UserObject, text);
        editor.apply();
    }

    public String getUserObject(Context context) {
        SharedPreferences sharedPreferences;
        String text;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = sharedPreferences.getString(UserObject, null);
        return text;
    }

    public String getTempLoginTokenValue(Context context) {
        SharedPreferences sharedPreferences;
        String text;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = sharedPreferences.getString(TempLoginToken, null);
        return text;
    }

    public void clearSharedPreference(Context context) {
        SharedPreferences sharedPreferences;
        Editor editor;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public void removeValue(Context context) {
        SharedPreferences sharedPreferences;
        Editor editor;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove(LoginToken);
        editor.apply();
    }

    public void saveMessage(Context context, String message) {
        SharedPreferences sharedPreferences;
        Editor editor;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(MESSAGE, message);
        editor.apply();
    }
    public String getMessage (Context context){
        SharedPreferences sharedPreferences;
        String text;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = sharedPreferences.getString(MESSAGE, "");
        return text;
    }

    public void saveTitleMessage(Context context, String title) {
        SharedPreferences sharedPreferences;
        Editor editor;
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(TITLE_MESSAGE, title);
        editor.apply();
    }
    public String getTitleMessage (Context context){
        SharedPreferences sharedPreferences;
        String text;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = sharedPreferences.getString(TITLE_MESSAGE, "");
        return text;
    }

}
