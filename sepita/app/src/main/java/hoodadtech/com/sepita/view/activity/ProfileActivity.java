package hoodadtech.com.sepita.view.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.AddressNew;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.ProfileRequest;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.EditProfileResult;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.ProfileResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.profileGet.ProfileGetResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.IransansEditText;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import io.blackbox_vision.wheelview.view.WheelView;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout btn_submit,choosePic,btn_back,linearLayout_date;
    LinearLayout linearBox;
    IransansEditText ET_input_name, et_PhoneNumber, et_email, Et_phoneNumber;
    TextView TV_gender,tv_day,tv_month,tv_year,tv_slash0,tv_slash1;
    PostOrderObject postOrderObject;
    FileUtility fileUtility;
    Server server;
    Toolbar toolbar;
    TextView toolBar_title;
    Spinner spinner;
    String gender = "not selected";
    ImageView iv_profile;
    private List<String> dayList,monthList,yearList;

    int selectedYearPos = 0;
    int selectedMonthPos = 0;
    int selectedDayPos = 0;
    private int yearPos;
    private int dayPos;
    private int monthPos;
    private ProgressBar loader,senderLoader;
    int GALLERY = 100;
    final int PIC_CROP = 101;
    private Uri uri = null;
    private Bitmap thumbnail = null;
    private SwipeRefreshLayout refresh;
    private UserObject user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        fileUtility = new FileUtility(this);
        server = Server.getInstance(getApplicationContext(), new SharedPreference().getLoginTokenValue(this));
        postOrderObject = fileUtility.getPostOrderObject();
        initView();
    }

    private void convertDate(String date) {
        String [] dateParts = date.split("/");

        tv_day.setText(dateParts[2]);
        tv_month.setText(dateParts[1]);
        tv_year.setText(dateParts[0]);

        for (int i = 0; i < dayList.size(); i++) {
            if (dateParts[2].equals(dayList.get(i))){
                selectedDayPos = i;
                break;
            }
        }
        for (int i = 0; i < monthList.size(); i++) {
            if (dateParts[1].equals(monthList.get(i))){
                selectedMonthPos = i;
                break;
            }
        }
        for (int i = 0; i < yearList.size(); i++) {
            if (dateParts[0].equals(yearList.get(i))){
                selectedYearPos = i;
                break;
            }
        }
    }

    private void initView() {
        createList();
        loader = findViewById(R.id.loader);
        refresh = findViewById(R.id.profileRefresh);
        refresh.setColorSchemeResources(R.color.app_blue2);
        refresh.setOnClickListener(this);
        senderLoader = findViewById(R.id.senderLoader);
        loader.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);
        senderLoader.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);
        spinner = findViewById(R.id.Spinner_category);
        iv_profile = findViewById(R.id.iv_profile);
        linearLayout_date = findViewById(R.id.linearLayout_date);
        choosePic = findViewById(R.id.choosePic);
        btn_back = findViewById(R.id.btn_back);
        tv_day = findViewById(R.id.tv_day);
        tv_month = findViewById(R.id.tv_month);
        tv_year = findViewById(R.id.tv_year);
        tv_slash0 = findViewById(R.id.tv_slash0);
        tv_slash1 = findViewById(R.id.tv_slash1);
        choosePic.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        linearLayout_date.setOnClickListener(this);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categories, R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

        spinner.setPrompt(getResources().getString(R.string.gender));
        spinner.setAdapter(adapter);
        user = new Gson().fromJson(new SharedPreference().getUserObject(getApplicationContext()), UserObject.class);
        if(user!=null&&user.getGender()!=null){
            if(user.getGender().equals("male")){

                spinner.setSelection(1);
                gender="male";
            }else if(user.getGender()!=null&&user.getGender().equals("female")){
                spinner.setSelection(0);
                gender="female";
            }
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    gender = "female";
                } else if (i == 1) {
                    gender = "male";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                    gender="not selected";
            }
        });



        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //toolBar_title = findViewById(R.id.toolbar_text);
        //toolBar_title.setText(getString(R.string.title_activity_profile));

        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(this), UserObject.class);

        btn_submit = findViewById(R.id.btn_save);
        //TV_gender = findViewById(R.id.TV_gender);
        ET_input_name = findViewById(R.id.ET_input_name);
        et_PhoneNumber = findViewById(R.id.et_PhoneNumber);
        et_email = findViewById(R.id.et_email);
        //mainLayout = findViewById(R.id.mainLayout);
        linearBox = findViewById(R.id.linearBox);
        btn_submit.setOnClickListener(this);
        //mainLayout.setOnClickListener(this);
        ET_input_name.setText(userObject.getFullName());
        et_PhoneNumber.setText(StringUtility.numberEn2Fa(userObject.getCellNumber()));
        if (userObject.getEmail() != null)
            et_email.setText(userObject.getEmail());
        //server.getProfile(new FutureCallback<Response<OperationResult<ProfileResult>>>() {
        //    @Override
        //    public void onCompleted(Exception e, Response<OperationResult<ProfileResult>> result) {
        //        if (e != null) {
        //            Log.i("kk","khorooji==> e");
        //            if (OperationResult.isOk(result.getResult(), e)) {
        //                Log.i("kk","khorooji==> ok");
        //                ET_input_name.setText(result.getResult().getResult().getFullName());
        //                et_PhoneNumber.setText(StringUtility.numberEn2Fa(result.getResult().getResult().getCellNum()));
        //                et_email.setText(result.getResult().getResult().getEmail());
        //                Log.i("kk","khorooji==>");
        //                //if (result.getResult().result.getBirthDate() != null){
        //                //    convertDate(result.getResult().result.getBirthDate());
        //                //    Log.i("kk","khorooji==>"+result.getResult().result.getBirthDate());
        //                //}else {
        //                //    Log.i("kk","khorooji==>"+result.getResult().result.getBirthDate());
        //                //    tv_slash0.setVisibility(View.GONE);
        //                //    tv_slash1.setVisibility(View.GONE);
        //                //}
        //                if(result.getResult().getResult().getGender()!=null&&result.getResult().getResult().getGender().equals("male")){
        //                    spinner.setSelection(1);
        //                    gender="male";
        //                }else {
        //                    if (user != null && user.getGender()!=null&&result.getResult().getResult().getGender() != null &&user.getGender()!=null&& user.getGender().equals("female")) {
        //                        spinner.setSelection(0);
        //                        gender = "female";
        //                    }
        //                }
//
//
//
        //                UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getApplicationContext()), UserObject.class);
        //                userObject.setFullName(result.getResult().getResult().getFullName());
        //                userObject.setCellNumber(result.getResult().getResult().getCellNum());
        //                userObject.setEmail(result.getResult().getResult().getEmail());
        //                userObject.setGender(result.getResult().getResult().getGender());
//
//
        //                new SharedPreference().saveUserObject(getApplicationContext(), new Gson().toJson(userObject));
//
//
        //            }
        //        } else {
//
        //        }
        //    }
        //});
        getProfile(server);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProfile(server);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                hideKeyboard();
                if (ET_input_name.getText().toString().length() == 0){
                    Toast.makeText(this, "لطفا نام خود را وارد کنید.", Toast.LENGTH_LONG).show();
                    return;
                }
                //if (et_email.getMyText().length() > 0 && !StringUtility.isValidEmail(et_email.getMyText())) {
                //    Toast.makeText(this, "ایمیل وارد شده معتبر نمی باشد", Toast.LENGTH_LONG).show();
                //    return;
                //}
                //if(et_email.getMyText().length()==0){
                //    Toast.makeText(this, "لطفا ایمیل خود را وارد کنید", Toast.LENGTH_LONG).show();
                //    return;
                //}

                //loader.setVisibility(View.VISIBLE);
                btn_submit.setVisibility(View.GONE);
                senderLoader.setVisibility(View.VISIBLE);
                ProfileRequest profileRequest = new ProfileRequest();
                if (thumbnail != null) {
                    String encodedImage = Base64.encodeToString(getFileDataFromDrawable(thumbnail), Base64.DEFAULT);
                    //Log.i("kk",encodedImage);
                    profileRequest.setImageUrl(encodedImage);
                }else {
                    profileRequest.setImageUrl("");
                }
                if (tv_year.getText().toString().length() == 0){
                    profileRequest.setBirthDate("");
                }else {
                    profileRequest.setBirthDate(tv_year.getText().toString() + "/" + tv_month.getText().toString() + "/" + tv_day.getText().toString());
                }
                //Log.i("kk",tv_year.getText().toString()+"/"+tv_month.getText().toString()+"/"+tv_day.getText().toString());

                profileRequest.setFullName(ET_input_name.getMyText());
                profileRequest.setEmail(et_email.getMyText());
                profileRequest.setGender(gender);
                profileRequest.setIntroducerCode("");
                editProfile(server,profileRequest);

                break;
            case R.id.mainLayout:
                hideKeyboard();
                break;

            case R.id.choosePic:
                showChooser();
                break;

            case R.id. btn_back:
                onBackPressed();
                break;
            case R.id.linearLayout_date:
                showDialogForDate();
                break;
        }
    }

    void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    private void showChooser() {
        // Use the GET_CONTENT intent from the utility class
        //Intent target = FileUtils.createGetContentIntent();
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Create the chooser Intent
        //Intent intent = Intent.createChooser(target, "chooser_title");
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},2);
                //ActivityCompat.requestPermissions(getActivity(),
                //        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                //        2);
            }else{
                startActivityForResult(intent, GALLERY);
            }

        } catch (ActivityNotFoundException e) {
            // The reason for the existence of aFileChooser
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 2 && grantResults.length > 0&& grantResults[0] == PackageManager.PERMISSION_GRANTED){
            //browser.showChooserNow();
            Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, GALLERY);
        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY && resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                //Log.i("kk",getRealPathFromURI(uri));
                //Ion.with(this).load(getRealPathFromURI(uri)).intoImageView(iv_profile);
                performCrop(uri);
            }
        }
        if (requestCode == PIC_CROP) {
            if (data != null) {
                //// get the returned data
                //Bundle extras = data.getExtras();
                //// get the cropped bitmap
                //Bitmap selectedBitmap = extras.getParcelable("data");
                //iv_profile.setImageBitmap(selectedBitmap);

                String filePath = Environment.getExternalStorageDirectory()+ "/temporary_holder.jpg";
                thumbnail = BitmapFactory.decodeFile(filePath);
                iv_profile.setImageBitmap(thumbnail);

            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Audio.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void createList(){
        String[] dayArray = getResources().getStringArray(R.array.day);
        String[] monthArray = getResources().getStringArray(R.array.month);
        String[] yearArray = getResources().getStringArray(R.array.year);

        dayList = new ArrayList();
        monthList = new ArrayList();
        yearList = new ArrayList();
        for (int i = 0; i < dayArray.length; i++) {
            dayList.add(dayArray[i]);
        }
        for (int i = 0; i < monthArray.length; i++) {
            monthList.add(monthArray[i]);
        }
        for (int i = 0; i < yearArray.length; i++) {
            yearList.add(yearArray[i]);
        }
    }

    private void showDialogForDate() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_select_date, null);
        builder.setView(rootView);
        builder.create();
        AlertDialog show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        WheelView day_loop = rootView.findViewById(R.id.day_loop);
        WheelView month_loop = rootView.findViewById(R.id.month_loop);
        WheelView year_loop = rootView.findViewById(R.id.year_loop);
        day_loop.setIsLoopEnabled(false);
        month_loop.setIsLoopEnabled(false);
        year_loop.setIsLoopEnabled(false);

        IransansButton confirmBtn = rootView.findViewById(R.id.confirmBtn);
        IransansButton cancel_Btn = rootView.findViewById(R.id.cancel_Btn);

        cancel_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_day.setText(dayList.get(dayPos));
                tv_month.setText(monthList.get(monthPos));
                tv_year.setText(yearList.get(yearPos));

                selectedDayPos = dayPos;
                selectedYearPos = yearPos;
                selectedMonthPos = monthPos;
                tv_slash0.setVisibility(View.VISIBLE);
                tv_slash1.setVisibility(View.VISIBLE);
                show.dismiss();
            }
        });


        day_loop.setItems(dayList);
        month_loop.setItems(monthList);
        year_loop.setItems(yearList);
        dayPos = selectedDayPos;
        monthPos = selectedMonthPos;
        yearPos = selectedYearPos;
        day_loop.setInitialPosition(selectedDayPos);
        month_loop.setInitialPosition(selectedMonthPos);
        year_loop.setInitialPosition(selectedYearPos);

        day_loop.setOnLoopScrollListener(new WheelView.OnLoopScrollListener() {
            @Override
            public void onLoopScrollFinish(@NonNull Object item, int position) {
                dayPos = position;
            }
        });

        month_loop.setOnLoopScrollListener(new WheelView.OnLoopScrollListener() {
            @Override
            public void onLoopScrollFinish(@NonNull Object item, int position) {
                monthPos = position;
                if (position<6){
                    if (dayList.size() == 30) {
                        dayList.add("31");
                        day_loop.setItems(dayList);
                    }

                }else {
                    if (dayList.size() == 31) {
                        dayList.remove(dayList.size() - 1);
                        day_loop.setItems(dayList);

                        if (dayPos == 30){
                            dayPos = 29;
                        }
                    }
                }

            }
        });

        year_loop.setOnLoopScrollListener(new WheelView.OnLoopScrollListener() {
            @Override
            public void onLoopScrollFinish(@NonNull Object item, int position) {
                yearPos = position;
            }
        });
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void performCrop(Uri uri) {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(uri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 300);
            cropIntent.putExtra("outputY", 300);

            File f = new File(Environment.getExternalStorageDirectory(),"/temporary_holder.jpg");
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Log.e("kk", ex.getMessage());
            }

            uri = Uri.fromFile(f);

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

            startActivityForResult(cropIntent, PIC_CROP);

        } //respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void getProfile(Server server){
        server.getProfileNew(new FutureCallback<Response<ProfileGetResult>>() {
            @Override
            public void onCompleted(Exception e, Response<ProfileGetResult> result) {
                refresh.setRefreshing(false);
                if (result != null){
                    if (result.getHeaders().code() == 401){
                        getToken(1,null);
                    }else if (result.getHeaders().code() == 200){
                        if (result.getResult().getStatus().getStatusCode() == 16){
                            getToken(1,null);
                        }else if (result.getResult().getStatus().getIsSuccess()){
                            linearBox.setVisibility(View.VISIBLE);
                            loader.setVisibility(View.INVISIBLE);
                            //Log.i("kk",getProfileResult().getFullName())
                            ET_input_name.setText(result.getResult().getProfileResult().getFullName());
                            et_PhoneNumber.setText(StringUtility.numberEn2Fa(result.getResult().getProfileResult().getCellNum()));
                            et_email.setText(result.getResult().getProfileResult().getEmail());
                            if (result.getResult().getProfileResult().getBirthDate() != null){
                                convertDate(result.getResult().getProfileResult().getBirthDate());
                                //Log.i("kk","khorooji==>"+result.getProfileResult().getBirthDate());
                            }else {
                                //Log.i("kk","khorooji==>"+result.getProfileResult().getBirthDate());
                                tv_slash0.setVisibility(View.GONE);
                                tv_slash1.setVisibility(View.GONE);
                            }
                            if(result.getResult().getProfileResult().getGender()!=null&&result.getResult().getProfileResult().getGender().equals("male")){
                                spinner.setSelection(1);
                                gender="male";
                            }else {
                                if (user != null && user.getGender()!=null&&result.getResult().getProfileResult().getGender() != null &&user.getGender()!=null&& user.getGender().equals("female")) {
                                    spinner.setSelection(0);
                                    gender = "female";
                                }
                            }
                            //Ion.with(ProfileActivity.this).load(result.getProfileResult().getImageUrl()).intoImageView(iv_profile);
                            if (result.getResult().getProfileResult().getImageUrl() == null || result.getResult().getProfileResult().getImageUrl().length() == 0){
                                iv_profile.setImageResource(R.drawable.user);
                            }else {
                                Ion.with(ProfileActivity.this).load(result.getResult().getProfileResult().getImageUrl()).intoImageView(iv_profile);
                            }
                            UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getApplicationContext()), UserObject.class);
                            userObject.setFullName(result.getResult().getProfileResult().getFullName());
                            userObject.setCellNumber(result.getResult().getProfileResult().getCellNum());
                            userObject.setEmail(result.getResult().getProfileResult().getEmail());
                            userObject.setGender(result.getResult().getProfileResult().getGender());
                            userObject.setBirthDate(result.getResult().getProfileResult().getBirthDate());

                            new SharedPreference().saveUserObject(getApplicationContext(), new Gson().toJson(userObject));
                        }else {
                            Toast.makeText(ProfileActivity.this,result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(ProfileActivity.this,getString(R.string.error),Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(ProfileActivity.this,getString(R.string.checkConnection),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void editProfile(Server server,ProfileRequest profileRequest) {
        server.editProfile(profileRequest, new FutureCallback<Response<hoodadtech.com.sepita.model.serverResult.editProfileResult.EditProfileResult>>() {
            @Override
            public void onCompleted(Exception e, Response<hoodadtech.com.sepita.model.serverResult.editProfileResult.EditProfileResult> result) {
                //loader.setVisibility(View.GONE);


                if (result != null){
                    if (result.getHeaders().code() == 401){
                        getToken(2,profileRequest);
                    }else if (result.getHeaders().code() == 200){
                        if (result.getResult().getStatus().getStatusCode() == 16){
                            getToken(2,profileRequest);
                        }else if (result.getResult().getStatus().getIsSuccess()){
                            Toast.makeText(ProfileActivity.this,result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                            btn_submit.setVisibility(View.VISIBLE);
                            senderLoader.setVisibility(View.GONE);
                        }else {
                            Toast.makeText(ProfileActivity.this,result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                            btn_submit.setVisibility(View.VISIBLE);
                            senderLoader.setVisibility(View.GONE);
                        }
                    }else {
                        Toast.makeText(ProfileActivity.this,getString(R.string.error),Toast.LENGTH_SHORT).show();
                        btn_submit.setVisibility(View.VISIBLE);
                        senderLoader.setVisibility(View.GONE);
                    }
                }else {
                    Toast.makeText(ProfileActivity.this,getString(R.string.checkConnection),Toast.LENGTH_SHORT).show();
                    btn_submit.setVisibility(View.VISIBLE);
                    senderLoader.setVisibility(View.GONE);
                }
            }
        });

    }

    public void getToken(int b,ProfileRequest profileRequest){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(ProfileActivity.this), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(ProfileActivity.this, result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(ProfileActivity.this, sharedPreference.getLoginTokenValue(ProfileActivity.this));
                        if (b == 1) {
                            getProfile(server1);
                        }else {
                            editProfile(server1,profileRequest);
                        }

                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(ProfileActivity.this, result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(ProfileActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //isSendRequest = false;
                }
            }
        });
    }
}
