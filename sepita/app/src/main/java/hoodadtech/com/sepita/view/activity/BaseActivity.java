package hoodadtech.com.sepita.view.activity;

import android.annotation.TargetApi;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.lang.reflect.Field;
import java.util.Date;
import java.util.Objects;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.PublicMethod;
import hoodadtech.com.sepita.utils.StringUtility;
import hoodadtech.com.sepita.view.fragment.DrawerFragment;


public abstract class BaseActivity extends AppCompatActivity {


    /**
     * **********************
     * NAVIGATION DRAWER
     * **********************
     */
    public DrawerLayout mDrawerLayout;
    protected LinearLayout mDrawerWrapper;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private Toolbar toolbar;
    private TextView mainbadge;
    private FrameLayout mainbadgef;
    private DrawerFragment drawerFragment;
    private FragmentManager fragmentManager;
    private static final String TAG = "BaseActivity";
    public static boolean isHome = false;


    public void filldraweritems() {
        if (drawerFragment != null)
            drawerFragment.fillNavigationDrawerItems();
    }

    void initNavigationDrawer() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                toolbar,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        )

        {
            public void onDrawerClosed(View view) {
//                if (getSupportActionBar() != null)
//                    getSupportActionBar().setTitle(mTitle);

               // invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
//                if (getSupportActionBar() != null)
//                    getSupportActionBar().setTitle(mDrawerTitle);
              //  invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);


        try {
            Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setElevation(0.f);
            getSupportActionBar().setTitle(null);
        }catch (Exception e){

        }


        mDrawerToggle.syncState();
    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(mTitle);
            getSupportActionBar().setDisplayUseLogoEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(false);
        }
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout =  findViewById(R.id.drawer_layout);
        mDrawerWrapper =  findViewById(R.id.left_drawer);
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initNavigationDrawer();

        Objects.requireNonNull(getSupportActionBar()).setTitle(null);


        if (drawerFragment != null) {
//            drawerFragment.setCurrentActivity(this);
            drawerFragment.setmDrawerLayout(mDrawerLayout);
            drawerFragment.setmDrawerWrapper(mDrawerWrapper);
        }
        //
        Log.d(TAG, "onPostCreate: " + new Date().getTime());
    }

    private void setupFragments() {
        fragmentManager = getSupportFragmentManager();

        drawerFragment = (DrawerFragment) fragmentManager.findFragmentByTag("drawer");
        if (drawerFragment == null) {
            drawerFragment = new DrawerFragment();
        }


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.left_drawer, drawerFragment, "drawer");
                    fragmentTransaction.commit();
                } catch (Exception ex) {
                }

            }
        }).start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        forceRTLIfSupported();

    }



    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerWrapper);
        return super.onPrepareOptionsMenu(menu);
    }//end onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    /*
     * MINI PLAYER STUFF
     */

    @Override
    protected void onStart() {
        super.onStart();
        setupFragments();

    }


    @Override
    protected void onStop() {
        super.onStop();
        drawerFragment = null;
        System.gc();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: " + new Date().getTime());


    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        drawerFragment.fillNavigationDrawerItems();

    }


    @Override
    protected void onPause() {
        super.onPause();
        // Unbind from the service
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isTaskRoot()) {
            drawerFragment = null;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void forceRTLIfSupported() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mDrawerWrapper)) {
            mDrawerLayout.closeDrawer(mDrawerWrapper);
        }else if (isHome){
            AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View rootView = inflater.inflate(R.layout.dialog_confirmation, null);
            builder.setView(rootView);
            builder.create();
            AlertDialog show = builder.show();
            show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


            IransansButton yesBtn = rootView.findViewById(R.id.yesBtn);
            IranSansTextView textView = rootView.findViewById(R.id.text);
            IransansButton noBtn = rootView.findViewById(R.id.noBtn);
            textView.setText(R.string.areYouWantToExit);
            yesBtn.setOnClickListener(view -> {

                show.dismiss();
                super.onBackPressed();
                finish();

            });
            noBtn.setOnClickListener(view -> {
                        show.dismiss();
                    }
            );
        }else {
            super.onBackPressed();
        }

    }

    public TextView getActionBarTextView() {
        TextView titleTextView = null;

        try {
            Field f = toolbar.getClass().getDeclaredField("mTitleTextView");
            f.setAccessible(true);
            titleTextView = (TextView) f.get(toolbar);

        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {


        }
        return titleTextView;
    }

}
