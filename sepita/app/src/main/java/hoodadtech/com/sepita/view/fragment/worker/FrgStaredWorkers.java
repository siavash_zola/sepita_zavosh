package hoodadtech.com.sepita.view.fragment.worker;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.MultiRecyclerAdapter;
import hoodadtech.com.sepita.adapter.listItems.DetailsItem;
import hoodadtech.com.sepita.adapter.listItems.ItemWorkerListItem;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.Service;
import hoodadtech.com.sepita.model.ServiceDetail;
import hoodadtech.com.sepita.model.ServiceDetails;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.Worker;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.NetworkUtil;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments.FrgSecondDetail;

public class FrgStaredWorkers extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ItemWorkerListItem.ItemClickListener {


    Server server;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    private MultiRecyclerAdapter multiRecyclerAdapter;
    private List<MultiAdapterItem> homeItems;
    TextView txt_no_selected;

    View view;


    boolean dataLoaded = false;

    ProgressBar progressBar;

    private SwipeRefreshLayout swipeContainer;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        if (view != null)
            return view;

        view = inflater.inflate(R.layout.frg_stared_workers, container, false);

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        //TextView title = getActivity().findViewById(R.id.toolbar_text);


        server = Server.getInstance(getContext());


        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });
        initView(view);
        return view;
    }

    private void initView(View view) {
        progressBar = view.findViewById(R.id.progressBar);
        txt_no_selected = view.findViewById(R.id.txt_no_selected);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.accent), android.graphics.PorterDuff.Mode.SRC_ATOP);


        IranSansTextView title = view.findViewById(R.id.header_title);
        title.setText(getResources().getString(R.string.worker_list));

        swipeContainer = view.findViewById(R.id.swiperefresh);
        swipeContainer.setOnRefreshListener(this);
        swipeContainer.setColorSchemeResources(R.color.main_blue);


        recyclerView = view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getContext());
        multiRecyclerAdapter = new MultiRecyclerAdapter(getContext());
        recyclerView.setAdapter(multiRecyclerAdapter);
        recyclerView.setLayoutManager(layoutManager);
        setData(false);


    }

    private void setData(boolean isRefresh) {
        if (!isRefresh)
            progressBar.setVisibility(View.VISIBLE);
        server.getFavoriteEmplyoeesList((e, result) -> {
            if (isAdded()) {
                swipeContainer.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                if (e != null) {
                    CustomSnackbar.showSnackBar(view);

                }
                else if (OperationResult.isOk(result.getResult(), e)) {
                    dataLoaded = true;

                    homeItems = new ArrayList<>();
                    List<Worker> hhomeItems = result.getResult().getResult();

                    if (hhomeItems != null && hhomeItems.size() < 1) {
                        txt_no_selected.setVisibility(View.VISIBLE);

                    } else {
                        txt_no_selected.setVisibility(View.GONE);
                        for (int i = 0; i < hhomeItems.size(); i++) {
                            ItemWorkerListItem workerListItem = new ItemWorkerListItem();
                            workerListItem.setItem(hhomeItems.get(i));
                            workerListItem.setItemClickListener(this);
                            homeItems.add(workerListItem);
                        }

                        multiRecyclerAdapter.setItems(homeItems);
                    }


                } else if (result.getHeaders().code() == 401) {
                    getToken();

                } else if (result.getResult() != null && result.getResult().getStatus() != null) {

                    Toast.makeText(getContext(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();

                }
            }


        });
    }


    private void getToken() {
        progressBar.setVisibility(View.VISIBLE);
        Server server = Server.getInstance(getContext());
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getContext()), UserObject.class);
        server.accountLoginUser(userObject.getCellNumber(), userObject.getActivationCode(), (e, result) -> {
            progressBar.setVisibility(View.INVISIBLE);
            if (e != null) {
                CustomSnackbar.showSnackBar(view);
            } else if (OperationResult.isOk(result, e)) {
                SharedPreference sharedPreference = new SharedPreference();
                sharedPreference.saveLoginToken(getContext(), result.getResult().getTokenId());
                Server server1 = Server.getInstance(getContext(), sharedPreference.getLoginTokenValue(getContext()));
                getStaredEmployeesList(server, e, result);


            } else {
                Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getStaredEmployeesList(Server server, Exception e, OperationResult<RegisterResult> result) {
        server.getFavoriteEmplyoeesList((e1, result1) -> {
            if (isAdded()) {
                swipeContainer.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                if (e != null) {
                    CustomSnackbar.showSnackBar(view);

                } else if (OperationResult.isOk(result1.getResult(), e)) {
                    dataLoaded = true;

                    homeItems = new ArrayList<>();
                    List<Worker> hhomeItems = result1.getResult().getResult();


                    for (int i = 0; i < hhomeItems.size(); i++) {
                        ItemWorkerListItem workerListItem = new ItemWorkerListItem();
                        workerListItem.setItem(hhomeItems.get(i));
                        workerListItem.setItemClickListener(this);
                        homeItems.add(workerListItem);
                    }

                    multiRecyclerAdapter.setItems(homeItems);
                } else if (result1.getHeaders().code() == 401) {
                    getToken();

                } else if (result.getResult() != null && result1.getResult().getStatus() != null) {

                    Toast.makeText(getContext(), result1.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();

                }
            }


        });
    }


    @Override
    public void onRefresh() {
        setData(true);
    }

    @Override
    public void onClick(Worker item, View view) {
        switch (view.getId()) {
            case R.id.layout_favorites:
                break;
            case R.id.layout_main:
                FragmentHandler fragmentHandler = new FragmentHandler(getActivity(), getFragmentManager());
                Bundle bundle = new Bundle();
                bundle.putString("id", item.getId());

                break;
        }
    }
}

