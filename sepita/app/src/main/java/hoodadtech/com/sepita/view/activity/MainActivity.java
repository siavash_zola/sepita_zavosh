package hoodadtech.com.sepita.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.FragmentHandler;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.view.fragment.entrancefragments.FrgEntrance;

public class MainActivity extends AppCompatActivity{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment();
    }

    private void loadFragment() {
        FragmentHandler fragmentHandler = new FragmentHandler(this, getSupportFragmentManager());


        if (new SharedPreference().getLoginTokenValue(MainActivity.this) != null) {
            startActivity(new Intent(MainActivity.this,HomeActivity.class));
            finish();

        } else
            fragmentHandler.loadFragment(new FrgEntrance(), false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

            //finish();
    }


}
