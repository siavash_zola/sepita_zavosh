package hoodadtech.com.sepita.model.serverResult.getCarWashOneResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {
    @SerializedName("carwashServices")
    @Expose
    private List<CarwashService> carwashServices = null;
    @SerializedName("priceItems")
    @Expose
    private List<PriceItem> priceItems = null;

    public List<CarwashService> getCarwashServices() {
        return carwashServices;
    }

    public void setCarwashServices(List<CarwashService> carwashServices) {
        this.carwashServices = carwashServices;
    }

    public List<PriceItem> getPriceItems() {
        return priceItems;
    }

    public void setPriceItems(List<PriceItem> priceItems) {
        this.priceItems = priceItems;
    }
}
