package hoodadtech.com.sepita.model.serverResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.serverResult.addAddress.Status;

public class PostOrderResultNew {
    @SerializedName("result")
    @Expose
    private PostOrderResult result;
    @SerializedName("status")
    @Expose
    private Status status;

    public PostOrderResult getResult() {
        return result;
    }

    public void setResult(PostOrderResult result) {
        this.result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
