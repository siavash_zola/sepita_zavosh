package hoodadtech.com.sepita.model.serverResult.orderDetailResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {
    @SerializedName("serviceTitle")
    @Expose
    private String serviceTitle;
    @SerializedName("orderCode")
    @Expose
    private String orderCode;
    @SerializedName("employeeId")
    @Expose
    private String employeeId;
    @SerializedName("employeeTitle")
    @Expose
    private String employeeTitle;
    @SerializedName("employeeStar")
    @Expose
    private Integer employeeStar;
    @SerializedName("employeeImageUrl")
    @Expose
    private String employeeImageUrl;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("finalAmount")
    @Expose
    private Integer finalAmount;
    @SerializedName("paymentTypeTitle")
    @Expose
    private String paymentTypeTitle;
    @SerializedName("visitDate")
    @Expose
    private String visitDate;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("employeeGender")
    @Expose
    private String employeeGender;
    @SerializedName("visitDateTime")
    @Expose
    private String visitDateTime;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("cityArea")
    @Expose
    private String cityArea;
    @SerializedName("employeePhone")
    @Expose
    private String employeePhone;
    @SerializedName("serviceDetails")
    @Expose
    private List<ServiceDetail> serviceDetails = null;
    @SerializedName("washType")
    @Expose
    private String washType;
    @SerializedName("serviceTypeCode")
    @Expose
    private String serviceTypeCode;



    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeTitle() {
        return employeeTitle;
    }

    public void setEmployeeTitle(String employeeTitle) {
        this.employeeTitle = employeeTitle;
    }

    public Integer getEmployeeStar() {
        return employeeStar;
    }

    public void setEmployeeStar(Integer employeeStar) {
        this.employeeStar = employeeStar;
    }

    public String getEmployeeImageUrl() {
        return employeeImageUrl;
    }

    public void setEmployeeImageUrl(String employeeImageUrl) {
        this.employeeImageUrl = employeeImageUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(Integer finalAmount) {
        this.finalAmount = finalAmount;
    }

    public String getPaymentTypeTitle() {
        return paymentTypeTitle;
    }

    public void setPaymentTypeTitle(String paymentTypeTitle) {
        this.paymentTypeTitle = paymentTypeTitle;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getEmployeeGender() {
        return employeeGender;
    }

    public void setEmployeeGender(String employeeGender) {
        this.employeeGender = employeeGender;
    }

    public String getVisitDateTime() {
        return visitDateTime;
    }

    public void setVisitDateTime(String visitDateTime) {
        this.visitDateTime = visitDateTime;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCityArea() {
        return cityArea;
    }

    public void setCityArea(String cityArea) {
        this.cityArea = cityArea;
    }

    public String getEmployeePhone() {
        return employeePhone;
    }

    public void setEmployeePhone(String employeePhone) {
        this.employeePhone = employeePhone;
    }

    public List<ServiceDetail> getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(List<ServiceDetail> serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    public String getWashType() {
        return washType;
    }

    public void setWashType(String washType) {
        this.washType = washType;
    }

    public String getServiceTypeCode() {
        return serviceTypeCode;
    }

    public void setServiceTypeCode(String serviceTypeCode) {
        this.serviceTypeCode = serviceTypeCode;
    }
}
