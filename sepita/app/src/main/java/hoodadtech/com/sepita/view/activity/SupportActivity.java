package hoodadtech.com.sepita.view.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.PostMessageObject;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.IransansEditText;
import hoodadtech.com.sepita.utils.SeterInfo;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;

import static java.security.AccessController.getContext;

public class SupportActivity extends AppCompatActivity implements View.OnClickListener {

    IransansEditText ET_body,ET_title;
    Server server;
    IransansButton btn_submit;
    LinearLayout main_layout,btn_back;
    RelativeLayout main_layout1;

    public static SeterInfo seterInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        reference();
        listener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreference sharedPreference = new SharedPreference();
        sharedPreference.saveMessage(SupportActivity.this,ET_body.getMyText());
        sharedPreference.saveTitleMessage(SupportActivity.this,ET_title.getMyText());
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreference sharedPreference = new SharedPreference();
        ET_body.setText(sharedPreference.getMessage(SupportActivity.this));
        ET_title.setText(sharedPreference.getTitleMessage(SupportActivity.this));
    }

    private void reference() {
        main_layout = findViewById(R.id.main_layout);
        main_layout1 = findViewById(R.id.main_layout1);
        //toolBar_title = findViewById(R.id.toolbar_text);
        ET_body = findViewById(R.id.ET_body);
        ET_title = findViewById(R.id.ET_title);
        btn_submit = findViewById(R.id.btn_submit);
        btn_back = findViewById(R.id.btn_back);
        //toolBar_title.setText(getString(R.string.support));
        btn_submit.setOnClickListener(this);
        main_layout.setOnClickListener(this);
        main_layout1.setOnClickListener(this);
        server = Server.getInstance(this);

    }

    private void listener(){
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void postMessage(Server server) {
        if(ET_title.getText().toString().length()<1){
            Toast.makeText(this,  "لطفا عنوان پیام را وارد نمایید" ,Toast.LENGTH_SHORT).show();
            return;
        }
        else if(ET_body.getText().toString().length()<2){
            Toast.makeText(this,  "لطفا متن پیام را وارد نمایید" ,Toast.LENGTH_SHORT).show();
            return;
        }

        PostMessageObject postMessageObject=new PostMessageObject();

        postMessageObject.setSubject(ET_title.getText().toString());
        postMessageObject.setBody(ET_body.getText().toString());
        server.postMessage(postMessageObject, (e, result) -> {
            btn_submit.setEnabled(true);
            if (result != null){
                if (result.getHeaders().code() == 401){
                    getToken();
                }else if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().getStatusCode() == 16){
                        getToken();
                    }else if (result.getResult().getStatus().isSuccess()){
                        Toast.makeText(this,result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                        finish();
                    }else {
                        Toast.makeText(this,result.getResult().getStatus().getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(this,getString(R.string.error),Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(this,getString(R.string.checkConnection),Toast.LENGTH_SHORT).show();
            }

        });
    }

    void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.main_layout:
                hideKeyboard();
                break;
            case R.id.main_layout1:
                hideKeyboard();
                break;
            case R.id.btn_submit:
                btn_submit.setEnabled(false);
                postMessage(server);
                break;
        }
    }

    public void getToken(){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(SupportActivity.this), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(SupportActivity.this, result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(SupportActivity.this, sharedPreference.getLoginTokenValue(SupportActivity.this));

                        postMessage(server1);

                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(SupportActivity.this, result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(SupportActivity.this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //isSendRequest = false;
                }
            }
        });
    }


}
