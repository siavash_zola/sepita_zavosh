package hoodadtech.com.sepita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceDetailInfoItem {
    @SerializedName("total")
    @Expose
    private int total;

    @SerializedName("amount")
    @Expose
    private int amount;

    @SerializedName("duration")
    @Expose
    private int duration;


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
