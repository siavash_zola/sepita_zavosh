package hoodadtech.com.sepita.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Milad on 12/15/2017.
 */

public class IransansEditText extends android.support.v7.widget.AppCompatEditText {
    public IransansEditText(Context context) {
        super(context);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "IRANSansMobile(FaNum)_Light.ttf");
        this.setTypeface(face);
    }

    public IransansEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "IRANSansMobile(FaNum)_Light.ttf");
        this.setTypeface(face);
    }

    public IransansEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "IRANSansMobile(FaNum)_Light.ttf");
        this.setTypeface(face);
    }

    public String getMyText(){
        return filtered();
    }

    public String filtered(){
        if (getText() != null) {
            return getText().toString()
                    .replace("۰","0")
                    .replace("۱","1")
                    .replace("۲","2")
                    .replace("۳","3")
                    .replace("۴","4")
                    .replace("۵","5")
                    .replace("۶","6")
                    .replace("۷","7")
                    .replace("۸","8")
                    .replace("۹","9")
                    .trim();
        }else{
            return "";
        }
    }


}
