
package hoodadtech.com.sepita.model.serverResult;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResultEmployeeDetail {

    @SerializedName("employeeId")
    @Expose
    private String employeeId;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("employeeImage")
    @Expose
    private Object employeeImage;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("avrageStarNumber")
    @Expose
    private String avrageStarNumber;
    @SerializedName("employeeDate")
    @Expose
    private String employeeDate;
    @SerializedName("workNumber")
    @Expose
    private String workNumber;
    @SerializedName("cellPhone")
    @Expose
    private String cellPhone;
    @SerializedName("isInList")
    @Expose
    private Boolean isInList;
    @SerializedName("employeeCommentItems")
    @Expose
    private List<EmployeeCommentItem> employeeCommentItems = null;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Object getEmployeeImage() {
        return employeeImage;
    }

    public void setEmployeeImage(Object employeeImage) {
        this.employeeImage = employeeImage;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAvrageStarNumber() {
        return avrageStarNumber;
    }

    public void setAvrageStarNumber(String avrageStarNumber) {
        this.avrageStarNumber = avrageStarNumber;
    }

    public String getEmployeeDate() {
        return employeeDate;
    }

    public void setEmployeeDate(String employeeDate) {
        this.employeeDate = employeeDate;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public Boolean getIsInList() {
        return isInList;
    }

    public void setIsInList(Boolean isInList) {
        this.isInList = isInList;
    }

    public List<EmployeeCommentItem> getEmployeeCommentItems() {
        return employeeCommentItems;
    }

    public void setEmployeeCommentItems(List<EmployeeCommentItem> employeeCommentItems) {
        this.employeeCommentItems = employeeCommentItems;
    }

}
