package hoodadtech.com.sepita.utils;

public interface OnItemLatLonClickListener {
    void itemLatLonClicked(int position);
}
