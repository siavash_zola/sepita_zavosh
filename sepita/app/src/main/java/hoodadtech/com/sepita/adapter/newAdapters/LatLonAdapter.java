package hoodadtech.com.sepita.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.AddressSaved;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.OnItemLatLonClickListener;

public class LatLonAdapter extends RecyclerView.Adapter {
    private List<AddressSaved> list;
    private Activity activity;
    private final int TYPE_HEADER = 0;
    private final int TYPE_FOOTER = 1;
    private final int TYPE_ITEM = 2;
    public OnItemLatLonClickListener listener;

    public LatLonAdapter(List<AddressSaved> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lat_lon, parent, false);
            return new ItemViewHolder(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        AddressSaved addressSaved = list.get(position);
        itemViewHolder.address_title.setText(addressSaved.getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public IranSansTextView address_title;
        public LinearLayout item_latLon;
        public ItemViewHolder(View itemView) {
            super(itemView);
            address_title = itemView.findViewById(R.id.address_title);
            item_latLon = itemView.findViewById(R.id.item_latLon);

            item_latLon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.itemLatLonClicked(getAdapterPosition());
                }
            });
        }
    }
}
