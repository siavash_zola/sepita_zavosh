
package hoodadtech.com.sepita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalculatePriceObject {

    @SerializedName("baseDuration")
    @Expose
    private String baseDuration;
    @SerializedName("basePrice")
    @Expose
    private String basePrice;
    @SerializedName("currentDuration")
    @Expose
    private String currentDuration;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("isVip")
    @Expose
    private String isVip;

    @SerializedName("number")
    @Expose
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getBaseDuration() {
        return baseDuration;
    }

    public void setBaseDuration(String baseDuration) {
        this.baseDuration = baseDuration;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getCurrentDuration() {
        return currentDuration;
    }

    public void setCurrentDuration(String currentDuration) {
        this.currentDuration = currentDuration;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIsVip() {
        return isVip;
    }

    public void setIsVip(String isVip) {
        this.isVip = isVip;
    }

}
