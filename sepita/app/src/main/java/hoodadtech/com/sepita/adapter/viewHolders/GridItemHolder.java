package hoodadtech.com.sepita.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.security.PublicKey;

import hoodadtech.com.sepita.R;

/**
 * Created by Milad on 1/5/2018.
 */

public class GridItemHolder extends RecyclerView.ViewHolder  {
    public ImageView imageView;
    public TextView title;

    public GridItemHolder(View itemView) {
        super(itemView);
        imageView=itemView.findViewById(R.id.img);
        title=itemView.findViewById(R.id.title);
    }
}
