
package hoodadtech.com.sepita.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostOrderObject {

    @SerializedName("serviceId")
    @Expose
    private String serviceId;
    @SerializedName("visitDate")
    @Expose
    private String visitDate;
    @SerializedName("carModelId")
    @Expose
    private String carModelId="";
    @SerializedName("visitTime")
    @Expose
    private String visitTime;
    @SerializedName("employeeGender")
    @Expose
    private String employeeGender="notimportant";
    @SerializedName("isVip")
    @Expose
    private String isVip="false";
    @SerializedName("isCredit")
    @Expose
    private String isCredit;
    @SerializedName("netAmount")
    @Expose
    private String netAmount;
    @SerializedName("finalAmunt")
    @Expose
    private String finalAmunt;
    @SerializedName("serviceDetails")
    @Expose
    private List<ServiceDetails> serviceDetails = null;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("number")
    @Expose
    private Integer number=1;

    @SerializedName("discountCodeId")
    @Expose
    private String discountCodeId;
    @SerializedName("isUseCredit")
    @Expose
    private String isUseCredit;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("carwashServiceId")
    @Expose
    private String carwashServiceId;
    @SerializedName("serviceTypeCodeId")
    @Expose
    private String serviceTypeCodeId;




    public String getIsUseCredit() {
        return isUseCredit;
    }

    public void setIsUseCredit(String isUseCredit) {
        this.isUseCredit = isUseCredit;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(String visitTime) {
        this.visitTime = visitTime;
    }

    public String getEmployeeGender() {
        return employeeGender;
    }

    public void setEmployeeGender(String employeeGender) {
        this.employeeGender = employeeGender;
    }

    public String getIsVip() {
        return isVip;
    }

    public void setIsVip(String isVip) {
        this.isVip = isVip;
    }

    public String getIsCredit() {
        return isCredit;
    }

    public void setIsCredit(String isCredit) {
        this.isCredit = isCredit;
    }

    public String getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(String netAmount) {
        this.netAmount = netAmount;
    }

    public String getFinalAmunt() {
        return finalAmunt;
    }

    public void setFinalAmunt(String finalAmunt) {
        this.finalAmunt = finalAmunt;
    }

    public List<ServiceDetails> getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(List<ServiceDetails> serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getDiscountCodeId() {
        return discountCodeId;
    }

    public void setDiscountCodeId(String discountCodeId) {
        this.discountCodeId = discountCodeId;
    }

    public String getCarModelId() {
        return carModelId;
    }

    public void setCarModelId(String carModelId) {
        this.carModelId = carModelId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCarwashServiceId() {
        return carwashServiceId;
    }

    public void setCarwashServiceId(String carwashServiceId) {
        this.carwashServiceId = carwashServiceId;
    }

    public String getServiceTypeCodeId() {
        return serviceTypeCodeId;
    }

    public void setServiceTypeCodeId(String serviceTypeCodeId) {
        this.serviceTypeCodeId = serviceTypeCodeId;
    }

    @Override
    public String toString() {
        return "PostOrderObject{" +
                "serviceId='" + serviceId + '\'' +
                ", visitDate='" + visitDate + '\'' +
                ", carModelId='" + carModelId + '\'' +
                ", visitTime='" + visitTime + '\'' +
                ", employeeGender='" + employeeGender + '\'' +
                ", isVip='" + isVip + '\'' +
                ", isCredit='" + isCredit + '\'' +
                ", netAmount='" + netAmount + '\'' +
                ", finalAmunt='" + finalAmunt + '\'' +
                ", serviceDetails=" + serviceDetails +
                ", address=" + address.toString() +
                ", phone='" + phone + '\'' +
                ", duration=" + duration +
                ", number=" + number +
                ", discountCodeId='" + discountCodeId + '\'' +
                ", isUseCredit='" + isUseCredit + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
