package hoodadtech.com.sepita.model.serverResult.checkOrder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("hasEmploye")
    @Expose
    private Boolean hasEmploye;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("cellNumber")
    @Expose
    private String cellNumber;

    public Boolean getHasEmploye() {
        return hasEmploye;
    }

    public void setHasEmploye(Boolean hasEmploye) {
        this.hasEmploye = hasEmploye;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }
}
