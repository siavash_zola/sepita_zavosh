package hoodadtech.com.sepita.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hoodadtech.com.sepita.R;

/**
 * Created by Milad on 1/5/2018.
 */

public class VerticalItemHolder extends RecyclerView.ViewHolder  {
    //public ImageView imageView;
    public TextView title;
    public LinearLayout linearBox_service;
    public TextView bigTitle,english_title;
    public ImageView img;

    public VerticalItemHolder(View itemView) {
        super(itemView);
        //imageView=itemView.findViewById(R.id.img);
        title=itemView.findViewById(R.id.title);
        bigTitle=itemView.findViewById(R.id.bigTitle);
        linearBox_service=itemView.findViewById(R.id.linearBox_service);
        img=itemView.findViewById(R.id.img);
        english_title=itemView.findViewById(R.id.english_title);
    }
}
