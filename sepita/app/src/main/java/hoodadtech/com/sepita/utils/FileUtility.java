package hoodadtech.com.sepita.utils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;

import hoodadtech.com.sepita.model.PostOrderObject;

/**
 * Created by Milad on 2/6/2018.
 */

public class FileUtility {
    Context context;
    public FileUtility(Context context){
        this.context=context;
    }

    public static File getUserDataDirectory(Context context) {
        File projectDir = new File(context.getFilesDir().getPath());
        boolean success = true;
        if (!projectDir.exists())
            success = projectDir.mkdirs();
        if (success) {
            return projectDir;
        } else {
            return null;
        }
    }

    public static File getUserProfileJsonFile(Context context) {
        return new File(FileUtility.getUserDataDirectory(context).getAbsolutePath(), "/PostOrder.json");
    }

    public void writePostOrderObject(PostOrderObject user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(FileUtility.getUserProfileJsonFile(context)));
            outputStreamWriter.write(json);
            outputStreamWriter.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public PostOrderObject getPostOrderObject() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(FileUtility.getUserProfileJsonFile(context))));
            StringBuilder json = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                json.append(line);
            }
            Gson gson = new Gson();
            JsonReader jsonReader=new JsonReader(new StringReader(json.toString().trim()));
            jsonReader.setLenient(true);
            return gson.fromJson(jsonReader, PostOrderObject.class);
        } catch (EOFException e) {
            return null;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
