package hoodadtech.com.sepita.model.serverResult.DateAndTimeResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import hoodadtech.com.sepita.model.Date;

public class ResultDT {
    @SerializedName("dates")
    @Expose
    private List<Date> dates = null;
    @SerializedName("houres")
    @Expose
    private List<String> houres = null;
    @SerializedName("extraHourPrice")
    @Expose
    private String extraHourPrice;
    @SerializedName("maxHourAvailable")
    @Expose
    private Integer maxHourAvailable;
    @SerializedName("maxHourMessageText")
    @Expose
    private String maxHourMessageText;
    @SerializedName("femalePrice")
    @Expose
    private String femalePrice;

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    public List<String> getHoures() {
        return houres;
    }

    public void setHoures(List<String> houres) {
        this.houres = houres;
    }

    public String getExtraHourPrice() {
        return extraHourPrice;
    }

    public void setExtraHourPrice(String extraHourPrice) {
        this.extraHourPrice = extraHourPrice;
    }

    public Integer getMaxHourAvailable() {
        return maxHourAvailable;
    }

    public void setMaxHourAvailable(Integer maxHourAvailable) {
        this.maxHourAvailable = maxHourAvailable;
    }

    public String getMaxHourMessageText() {
        return maxHourMessageText;
    }

    public void setMaxHourMessageText(String maxHourMessageText) {
        this.maxHourMessageText = maxHourMessageText;
    }

    public String getFemalePrice() {
        return femalePrice;
    }

    public void setFemalePrice(String femalePrice) {
        this.femalePrice = femalePrice;
    }
}
