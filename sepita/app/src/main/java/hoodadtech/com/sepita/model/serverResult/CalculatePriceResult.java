
package hoodadtech.com.sepita.model.serverResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalculatePriceResult {

    @SerializedName("amount")
    @Expose
    private String amount;


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
