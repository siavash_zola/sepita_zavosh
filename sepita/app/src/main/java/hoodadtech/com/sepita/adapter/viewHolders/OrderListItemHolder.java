package hoodadtech.com.sepita.adapter.viewHolders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.FontIconTextView;
import hoodadtech.com.sepita.utils.IranSansTextView;

/**
 * Created by Milad on 1/30/2018.
 */

public class OrderListItemHolder extends RecyclerView.ViewHolder {
    public IranSansTextView TV_phone, TV_date, TV_address, TV_status,TV_Order_Code,txt_service_title;
    public FontIconTextView Icon_Status, Icon_cancel;
    public LinearLayout layout_details;
    public CardView layout_status;

    public OrderListItemHolder(View itemView) {
        super(itemView);
        TV_phone = itemView.findViewById(R.id.TV_phone);
        txt_service_title = itemView.findViewById(R.id.txt_service_title);
        TV_date = itemView.findViewById(R.id.TV_date);
        TV_address = itemView.findViewById(R.id.TV_address);
        Icon_Status = itemView.findViewById(R.id.Icon_Status);
        TV_status = itemView.findViewById(R.id.TV_status);
        layout_details = itemView.findViewById(R.id.layout_details);
        Icon_cancel = itemView.findViewById(R.id.Icon_cancel);
        TV_Order_Code = itemView.findViewById(R.id.TV_Order_Code);
        layout_status = itemView.findViewById(R.id.layout_status);

    }
}
