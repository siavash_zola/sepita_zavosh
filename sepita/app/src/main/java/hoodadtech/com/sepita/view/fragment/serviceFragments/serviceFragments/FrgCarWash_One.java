package hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.newAdapters.CarWashTypeAdapter;
import hoodadtech.com.sepita.model.AddressNew;
import hoodadtech.com.sepita.model.Date;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.Service;
import hoodadtech.com.sepita.model.ServiceDetails;
import hoodadtech.com.sepita.model.TypeCarWash;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.DateAndTimeResult.DateAndTimeResult;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.getCarWashOneResult.CarwashService;
import hoodadtech.com.sepita.model.serverResult.getCarWashOneResult.GetCarWashOneResult;
import hoodadtech.com.sepita.model.serverResult.getCarWashOneResult.PriceItem;
import hoodadtech.com.sepita.model.serverResult.visitingTimes.VisitingTimesResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.IransansButton;
import hoodadtech.com.sepita.utils.MyClickListener;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import io.blackbox_vision.wheelview.view.WheelView;

/**
 * A simple {@link Fragment} subclass.
 */
public class FrgCarWash_One extends Fragment implements MyClickListener{
    private IranSansTextView tv_service_title,btn_date,price;
    private List<TypeCarWash> list ;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private CarWashTypeAdapter adapter;
    private TypeCarWash washSelected = null;
    private LinearLayout date_layout,btn_submit;
    List<String> dateArrayList, timeArrayList;
    List<Date> dateList;
    String vipText;
    int maxHourAvailable = 10;
    String maxHourMessageText="";
    private Dialog dialog;
    String selectedDatee = null;
    int SelectedDatePos = 3;
    FileUtility fileUtility;
    PostOrderObject postOrderObject;
    String selectedDateTime = "";
    String selectedtime = null;
    int selectedTimePos = 3;
    private ProgressBar loader;
    private List<PriceItem> priceItems;
    int amount = 0;
    public boolean isLoaded = false;
    private SwipeRefreshLayout refresh;
    private AlertDialog show;
    private Server server;
    private Service service;
    private String serviceId;


    public FrgCarWash_One() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //service = new Gson().fromJson(getArguments().getString("service"), Service.class);

        return inflater.inflate(R.layout.fragment_frg_car_wash__one, container, false);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDateANDTimes();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        reference(view);
    }

    private void reference(View view) {
        fileUtility = new FileUtility(getContext());
        postOrderObject = fileUtility.getPostOrderObject();
        //List<ServiceDetails> details = new ArrayList<>();
        //for (int i = 0; i < postOrderObject.getServiceDetails().size(); i++) {
        //    ServiceDetails serviceDetails = postOrderObject.getServiceDetails().get(i);
        //    if (Integer.parseInt(serviceDetails.getQuantity())>0){
        //        details.add(serviceDetails);
        //    }
        //}

        //postOrderObject.setServiceDetails(details);

        server = Server.getInstance(getActivity());
        tv_service_title = view.findViewById(R.id.tv_service_title);
        refresh = view.findViewById(R.id.refresh);
        refresh.setColorSchemeResources(R.color.app_blue1);
        tv_service_title.setText(getArguments().getString("title"));

        loader = view.findViewById(R.id.loader);
        loader.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue1), android.graphics.PorterDuff.Mode.SRC_ATOP);

        list = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new CarWashTypeAdapter(list,getActivity());
        adapter.listener = this;
        recyclerView.setAdapter(adapter);
        date_layout = view.findViewById(R.id.date_layout);
        btn_submit = view.findViewById(R.id.btn_submit);
        btn_date = view.findViewById(R.id.btn_date);
        price = view.findViewById(R.id.price);

        postOrderObject.setCarModelId(getArguments().getString("id"));


        listener();
    }

    private void loadData(Server server) {

        server.getCarWashOne(new FutureCallback<Response<GetCarWashOneResult>>() {
            @Override
            public void onCompleted(Exception e, Response<GetCarWashOneResult> result) {
                loader.setVisibility(View.GONE);
                refresh.setRefreshing(false);

                if (result != null) {
                    if (result.getHeaders().code() == 401) {
                        getToken();
                    } else if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().getStatusCode() == 16) {
                            getToken();
                        } else if (result.getResult().getStatus().getIsSuccess()) {
                            isLoaded = true;
                            list.clear();
                            for (int i = 0; i < result.getResult().getResult().getCarwashServices().size(); i++) {
                                CarwashService carwashService = result.getResult().getResult().getCarwashServices().get(i);
                                TypeCarWash wash = new TypeCarWash();
                                wash.setTitle(carwashService.getCarwashServiceTitle());
                                wash.setCarwashServiceId(carwashService.getCarwashServiceId());
                                if (carwashService.getCarwashServiceId().equals(postOrderObject.getCarwashServiceId())) {
                                    wash.setSelected(true);
                                } else {
                                    wash.setSelected(false);
                                }
                                //Log.i("kk",postOrderObject.getCarwashServiceId());
                                list.add(wash);
                            }
                            adapter.notifyDataSetChanged();
                            date_layout.setVisibility(View.VISIBLE);
                            //------------
                            priceItems = result.getResult().getResult().getPriceItems();
                        } else {
                            Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getActivity().getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.checkConnection), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void listener() {
        date_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLoaded) {
                    if (selectedDatee != null || selectedtime != null) {
                        if (washSelected != null) {
                            postOrderObject.setDuration(1);
                            postOrderObject.setNetAmount(amount + "");
                            postOrderObject.setCarwashServiceId(washSelected.getCarwashServiceId());
                            fileUtility.writePostOrderObject(postOrderObject);

                            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                            String frgTag = getActivity().getClass().getSimpleName();
                            FrgAddress fragment = new FrgAddress();
                            fragmentTransaction.replace(R.id.fragment_container, fragment, frgTag);
                            fragmentTransaction.addToBackStack(frgTag);
                            fragmentTransaction.commit();
                        } else {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.selectPlz), Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(getContext(), getActivity().getString(R.string.selectDate), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isLoaded = false;
                list.clear();
                getDateANDTimes();
            }
        });
    }

    private void getDateANDTimes() {
        Server server = Server.getInstance(getContext());
        dateArrayList = new ArrayList<>();
        timeArrayList = new ArrayList<>();
        dateList = new ArrayList<>();

        serviceId = getArguments().getString("id");
        Log.i("kk",serviceId);
        server.getVisitingTimesNewNewNew(serviceId, new FutureCallback<Response<DateAndTimeResult>>() {
            @Override
            public void onCompleted(Exception e, Response<DateAndTimeResult> result) {
                if (result!=null) {
                    if (result.getHeaders().code() == 200) {
                        timeArrayList = result.getResult().getResult().getHoures();
                        dateList = result.getResult().getResult().getDates();
                        for (int i = 0; i < result.getResult().getResult().getDates().size(); i++) {
                            dateArrayList.add(result.getResult().getResult().getDates().get(i).getShowDate());
                            //vipText = result.getResult().getVipText();
                            maxHourAvailable = Integer.valueOf(result.getResult().getResult().getMaxHourAvailable());
                            maxHourMessageText = result.getResult().getResult().getMaxHourMessageText();

                        }
                        loadData(server);
                    }
                }
            }
        });
    }

    @Override
    public void itemClicked(int position) {
        for (int i = 0; i < list.size(); i++) {
            TypeCarWash wash = list.get(i);
            if (i == position){
                wash.setSelected(true);
                washSelected = wash;
            }else {
                wash.setSelected(false);
            }
        }
        adapter.notifyDataSetChanged();


        amount = 0;
        for (int i = 0; i < postOrderObject.getServiceDetails().size(); i++) {
            ServiceDetails serviceDetails = postOrderObject.getServiceDetails().get(i);
            for (int j = 0; j < priceItems.size(); j++) {
                if (serviceDetails.getServiceDetailId().equals(priceItems.get(j).getServiceDetailId()) && washSelected.getCarwashServiceId().equals(priceItems.get(j).getCarwashServiceId())){
                    amount = amount + (Integer.parseInt(serviceDetails.getQuantity()) * Integer.parseInt(priceItems.get(j).getAmount()));
                    break;
                }
            }
        }

        price.setText(amount+"");
    }

    void showDialog() {
        //dialog = new Dialog(getContext());
        //dialog.setCancelable(false);
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.setContentView(R.layout.dialog_date_time);

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.dialog_date_time, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(rootView);
        builder.setCancelable(false);
        builder.create();
        show = builder.show();
        show.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final WheelView wheelView = rootView.findViewById(R.id.loop_view);
        final WheelView time_loop_view = rootView.findViewById(R.id.time_loop_view);
        wheelView.setInitialPosition(SelectedDatePos);
        wheelView.setIsLoopEnabled(false);


        wheelView.addOnLoopScrollListener((item, position) -> {
            selectedDatee = item.toString();
            SelectedDatePos = position;
            postOrderObject.setVisitDate(dateList.get(SelectedDatePos).getDate());
            selectedDateTime = selectedDatee + " " + selectedtime;

        });
        if (dateArrayList.size() > 0)
            wheelView.setItems(dateArrayList);

        time_loop_view.setInitialPosition(selectedTimePos);
        time_loop_view.setIsLoopEnabled(false);
        time_loop_view.addOnLoopScrollListener((item, position) -> {
            selectedtime = item.toString();
            selectedTimePos = position;
            postOrderObject.setVisitTime(StringUtility.numberFa2En(selectedtime));
            selectedDateTime = selectedDatee + " " + selectedtime;


        });
        if (timeArrayList.size() > 0)
            time_loop_view.setItems(timeArrayList);


        IransansButton cancelBtn = rootView.findViewById(R.id.canel_Btn);
        IransansButton confirmBtn = rootView.findViewById(R.id.confirmBtn);


//
        cancelBtn.setOnClickListener(view -> show.dismiss());

        confirmBtn.setOnClickListener(view -> {
            if (selectedDatee == null)
                selectedDatee = dateList.get(SelectedDatePos).getShowDate();
            if (selectedtime == null)
                selectedtime = timeArrayList.get(selectedTimePos);
            postOrderObject.setVisitDate(dateList.get(SelectedDatePos).getDate());
            postOrderObject.setVisitTime(StringUtility.numberFa2En(selectedtime));
            selectedDateTime = selectedDatee + " " + selectedtime;
            btn_date.setText(StringUtility.numberEn2Fa(selectedDateTime.toString()));
            show.dismiss();
        });

        //dialog.show();
    }
    @Override
    public void onResume() {
        super.onResume();
        if (postOrderObject.getVisitDate() != null && postOrderObject.getVisitTime() != null){
            btn_date.setText(StringUtility.numberEn2Fa(selectedDateTime.toString()));
        }
        Log.i("kk",postOrderObject.getCarwashServiceId());
    }

    public void getToken(){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getActivity()), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(getActivity(), result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(getActivity(), sharedPreference.getLoginTokenValue(getActivity()));

                        loadData(server1);
                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    //isSendRequest = false;
                }
            }
        });
    }
}
