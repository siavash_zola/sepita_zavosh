package hoodadtech.com.sepita.adapter.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.utils.IranSansTextView;

/**
 * Created by Milad on 1/30/2018.
 */

public class DetailItemHolder extends RecyclerView.ViewHolder   {
    public IranSansTextView TV_value,TV_title;
    public LinearLayout plus_one,minus_one;
    public ImageView icon;
    public DetailItemHolder(View itemView) {
        super(itemView);
        TV_title=itemView.findViewById(R.id.TV_title);
        TV_value=itemView.findViewById(R.id.TV_value);
        plus_one=itemView.findViewById(R.id.plus_one);
        minus_one=itemView.findViewById(R.id.minuse_one);
        icon=itemView.findViewById(R.id.icon);

    }
}
