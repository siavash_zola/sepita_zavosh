package hoodadtech.com.sepita.view.fragment.serviceFragments.serviceFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;

import java.util.ArrayList;
import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.adapter.MultiAdapterItem;
import hoodadtech.com.sepita.adapter.MultiRecyclerAdapter;
import hoodadtech.com.sepita.adapter.listItems.DetailsItem;
import hoodadtech.com.sepita.model.ServiceDetailInfoItem;
import hoodadtech.com.sepita.model.serverResult.OperationResult;
import hoodadtech.com.sepita.model.PostOrderObject;
import hoodadtech.com.sepita.model.Service;
import hoodadtech.com.sepita.model.ServiceDetail;
import hoodadtech.com.sepita.model.ServiceDetails;
import hoodadtech.com.sepita.model.UserObject;
import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.network.Server;
import hoodadtech.com.sepita.utils.CustomSnackbar;
import hoodadtech.com.sepita.utils.FileUtility;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.NetworkUtil;
import hoodadtech.com.sepita.utils.ServiceType;
import hoodadtech.com.sepita.utils.SharedPreference;
import hoodadtech.com.sepita.utils.StringUtility;
import hoodadtech.com.sepita.view.activity.MapsActivity;

public class FrgFirstDetail extends Fragment implements DetailsItem.ItemClickListener, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    LinearLayout btn_submit;
    TextView time, price,tv_service_title;
    int firstCounter = 0;
    int Ttime = 0;
    int Pprice = 0;
    Server server;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    private MultiRecyclerAdapter multiRecyclerAdapter;
    private List<MultiAdapterItem> homeItems;
    Service service;
    View view;
    List<ServiceDetails> serviceDetailsList;
    int servieQuantity;
    boolean dataLoaded = false;
    FileUtility fileUtility;
    PostOrderObject postOrderObject;
    ProgressBar progressBar;

    private SwipeRefreshLayout swipeContainer;
    private String serviceString;
    private boolean isSendRequest = false;
    private int baseDuration;
    private int baseAmount;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        if (view != null)
            return view;

        view = inflater.inflate(R.layout.frg_details_first, container, false);
        serviceDetailsList = new ArrayList<>();
        fileUtility = new FileUtility(getContext());
        postOrderObject = fileUtility.getPostOrderObject();
        serviceString = getArguments().getString("service");
        this.service = new Gson().fromJson(serviceString, Service.class);
        LinearLayout linear_price = view.findViewById(R.id.linear_price);


        server = Server.getInstance(getContext());


        if (this.service.getServiceTypeCode().equals(ServiceType.CARWASH_code) || service.getServiceTypeCode().equals(ServiceType.COMPLEX_code)){
            linear_price.setVisibility(View.INVISIBLE);
        }

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

            }
        });
        initView(view);
        return view;
    }

    private void initView(View view) {
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.app_blue2), android.graphics.PorterDuff.Mode.SRC_ATOP);

        IranSansTextView title = view.findViewById(R.id.header_title);
        title.setText(service.getTitle());
        btn_submit = view.findViewById(R.id.btn_submit);
        time = view.findViewById(R.id.time);
        price = view.findViewById(R.id.price);
        tv_service_title = view.findViewById(R.id.tv_service_title);
        swipeContainer = view.findViewById(R.id.swiperefresh);
        swipeContainer.setOnRefreshListener(this);
        swipeContainer.setColorSchemeResources(R.color.app_blue1);
        time.setText(StringUtility.numberEn2Fa(String.valueOf(firstCounter)));
        price.setText(StringUtility.numberEn2Fa(String.valueOf(firstCounter)));
        btn_submit.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getContext());
        multiRecyclerAdapter = new MultiRecyclerAdapter(getContext());
        recyclerView.setAdapter(multiRecyclerAdapter);
        recyclerView.setLayoutManager(layoutManager);
        tv_service_title.setText(service.getTitle());
        setData(false,server);


    }

    private void setData(boolean isRefresh,Server server) {
        if (!isRefresh)
            progressBar.setVisibility(View.VISIBLE);
        server.getServiceDetails(service.getId(), (e, result) -> {
            if (isAdded()) {
                isSendRequest = true;
                swipeContainer.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                if (result != null) {
                    if (e != null) {
                        CustomSnackbar.showSnackBar(view);
                        isSendRequest = false;
                    } else if (result.getHeaders().code() == 200) {
                        if (result.getResult().getStatus().isSuccess()) {
                            baseDuration = result.getResult().getResult().getBaseDuration();
                            baseAmount = result.getResult().getResult().getBaseAmount();
                            dataLoaded = true;
                            Ttime = result.getResult().getResult().getBaseDuration();
                            Pprice = result.getResult().getResult().getBaseAmount();
                            homeItems = new ArrayList<>();
                            List<ServiceDetail> hhomeItems = result.getResult().getResult().getServiceDetails();

                            time.setText(StringUtility.numberEn2Fa(String.valueOf(result.getResult().getResult().getBaseDuration())));
                            price.setText(StringUtility.numberEn2Fa(String.valueOf(result.getResult().getResult().getBaseAmount())));
                            for (int i = 0; i < hhomeItems.size(); i++) {
                                //Log.i("kk",hhomeItems.get(i).getIconUrl());
                                ServiceDetails serviceDetails = new ServiceDetails();
                                serviceDetails.setServiceDetailId(hhomeItems.get(i).getId());
                                serviceDetails.setQuantity(String.valueOf(hhomeItems.get(i).getSeedValue()));
                                serviceDetailsList.add(serviceDetails);
                                DetailsItem listItem = new DetailsItem();
                                listItem.setItem(hhomeItems.get(i));
                                listItem.setItemClickListener(this);
                                homeItems.add(listItem);
                            }
                            postOrderObject.setServiceDetails(serviceDetailsList);
                            multiRecyclerAdapter.setItems(homeItems);
                            isSendRequest = false;
                        } else if (result.getResult().getStatus().getStatusCode() == 16) {
                            getToken(isRefresh);
                        } else {
                            Toast.makeText(getContext(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                            isSendRequest = false;
                        }
                    } else if (result.getHeaders().code() == 401) {
                        getToken(isRefresh);

                    } else if (result.getResult() != null && result.getResult().getStatus() != null) {
                        isSendRequest = false;

                        Toast.makeText(getContext(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                        isSendRequest = false;
                    } else {
                        Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                        isSendRequest = false;
                    }
                }else {
                    Toast.makeText(getContext(), getString(R.string.checkConnection), Toast.LENGTH_SHORT).show();
                    isSendRequest = false;
                }
            }


        });

    }

    //private void getToken() {
    //    progressBar.setVisibility(View.VISIBLE);
    //    Server server = Server.getInstance(getContext());
    //    UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getContext()), UserObject.class);
    //    server.accountLoginUser(userObject.getCellNumber(), userObject.getActivationCode(), (e, result) -> {
    //        progressBar.setVisibility(View.INVISIBLE);
    //        if (e != null) {
    //            CustomSnackbar.showSnackBar(view);
    //        } else if (OperationResult.isOk(result, e)) {
    //            SharedPreference sharedPreference = new SharedPreference();
    //            sharedPreference.saveLoginToken(getContext(), result.getResult().getTokenId());
    //            Server server1 = Server.getInstance(getContext(), sharedPreference.getLoginTokenValue(getContext()));
    //            server1.getServiceDetails(service.getId(), (e1, result1) -> {
    //                if (isAdded()) {
    //                    progressBar.setVisibility(View.GONE);
    //                    if (e1 != null) {
    //                        CustomSnackbar.showSnackBar(view);
    //                    } else if (OperationResult.isOk(result1.getResult(), e1)) {
    //                        dataLoaded = true;
    //                        Ttime = result1.getResult().getResult().getBaseDuration();
    //                        Pprice = result1.getResult().getResult().getBaseAmount();
    //                        homeItems = new ArrayList<>();
    //                        List<ServiceDetail> hhomeItems = result1.getResult().getResult().getServiceDetails();
//
    //                        time.setText(StringUtility.numberEn2Fa(String.valueOf(result1.getResult().getResult().getBaseDuration())));
    //                        price.setText(StringUtility.numberEn2Fa(String.valueOf(result1.getResult().getResult().getBaseAmount())));
    //                        for (int i = 0; i < hhomeItems.size(); i++) {
    //                            ServiceDetails serviceDetails = new ServiceDetails();
    //                            serviceDetails.setServiceDetailId(hhomeItems.get(i).getId());
    //                            serviceDetails.setQuantity(String.valueOf(hhomeItems.get(i).getSeedValue()));
    //                            serviceDetailsList.add(serviceDetails);
    //                            DetailsItem listItem = new DetailsItem();
//
    //                            listItem.setItem(hhomeItems.get(i));
    //                            listItem.setItemClickListener(this);
    //                            homeItems.add(listItem);
    //                        }
    //                        postOrderObject.setServiceDetails(serviceDetailsList);
    //                        multiRecyclerAdapter.setItems(homeItems);
    //                    } else if (result != null && result.getStatus() != null) {
//
    //                        Toast.makeText(getContext(), result.getStatus().getMessage(), Toast.LENGTH_SHORT).show();
    //                    } else {
    //                        Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();
//
    //                    }
    //                }
//
//
    //            });
//
//
    //        } else {
    //            Toast.makeText(getContext(), "لطفا وضعیت ارتباط خود را بررسی کنید", Toast.LENGTH_SHORT).show();
//
    //        }
    //    });
//
    //}

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                if (!isSendRequest) {
                    if (Ttime == 0)
                        Toast.makeText(getContext(), getActivity().getString(R.string.selectPlz), Toast.LENGTH_SHORT).show();
                    else if (dataLoaded) {
                        int sum = 0;
                        for (int i = 0; i < serviceDetailsList.size(); i++) {
                            ServiceDetails serviceDetails = serviceDetailsList.get(i);
                            sum = sum + Integer.parseInt(serviceDetails.getQuantity());
                        }
                        if (sum > 0) {
                            try {
                                Log.i("kk","ok?");
                                postOrderObject.setNumber(1);
                                postOrderObject.setCarwashServiceId("");
                                postOrderObject.setServiceDetails(serviceDetailsList);
                                postOrderObject.setDuration(Ttime);
                                postOrderObject.setNetAmount(String.valueOf(Pprice));
                                fileUtility.writePostOrderObject(postOrderObject);

                                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                                String frgTag = getActivity().getClass().getSimpleName();
                                Fragment fragment = null;

                                switch (service.getServiceTypeCode()) {
                                    case ServiceType.HAUSE_CLEANING_code:
                                        fragment = new FrgSecondDetail();
                                        break;
                                    case ServiceType.CARWASH_code:
                                        //Log.i("kk","create new fragment");
                                        fragment = new FrgCarWash_One();
                                        break;
                                    case ServiceType.HOUSEBUYING_code:
                                        break;
                                    case ServiceType.COMPLEX_code:
                                        fragment = new FrgSecondDetail();
                                        break;

                                }
                                Bundle bundle = new Bundle();
                                bundle.putString("id", service.getId());
                                bundle.putString("title", service.getTitle());
                                bundle.putInt("time", Ttime);
                                bundle.putInt("price", Pprice);
                                bundle.putString("service", serviceString);
                                bundle.putString("serviceTypeCode",service.getServiceTypeCode());
                                fragment.setArguments(bundle);
                                fragmentTransaction.replace(R.id.fragment_container, fragment, frgTag);
                                fragmentTransaction.addToBackStack(frgTag);
                                fragmentTransaction.commit();
                            } catch (Exception e) {
                            }
                        } else {
                            Toast.makeText(getContext(), getActivity().getString(R.string.selectPlz), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                break;

//

        }

    }

    @Override
    public void onClick(ServiceDetail item, View view, int timedd, int pricedd, int quantity) {
        switch (view.getId()) {
            case R.id.minuse_one:
                if (quantity >= item.getSeedValue()) {
                    for (int i = 0; i < item.getServiceDetailInfo().size(); i++) {
                        ServiceDetailInfoItem serviceDetailInfoItem = item.getServiceDetailInfo().get(i);
                        if (quantity == serviceDetailInfoItem.getTotal()) {
                            Ttime = serviceDetailInfoItem.getDuration();
                            Pprice = serviceDetailInfoItem.getAmount();
                            time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
                            price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
                            for (int j = 0; j < serviceDetailsList.size(); j++) {
                                if (serviceDetailsList.get(j).getServiceDetailId() == item.getId()) {
                                    serviceDetailsList.get(j).setQuantity(String.valueOf(quantity));
                                }
                            }
                        }
                    }
                }else {
                    if (Ttime - timedd >= baseDuration)
                        Ttime = Ttime - timedd;
                    if (quantity >= item.getSeedValue()) {
                        if (Pprice - pricedd >= baseAmount)
                            Pprice = Pprice - pricedd;
                    }
                    time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
                    price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
                    for (int i = 0; i < serviceDetailsList.size(); i++) {
                        if (serviceDetailsList.get(i).getServiceDetailId() == item.getId()) {
                            serviceDetailsList.get(i).setQuantity(String.valueOf(quantity));
                        }
                    }
                }
                time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
                price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
                for (int i = 0; i < serviceDetailsList.size(); i++) {
                    if (serviceDetailsList.get(i).getServiceDetailId() == item.getId()) {
                        serviceDetailsList.get(i).setQuantity(String.valueOf(quantity));
                    }
                }
                break;
            case R.id.plus_one:
                //Toast.makeText(getActivity(),item.getSeedValue()+"",Toast.LENGTH_SHORT).show();
                if (quantity > item.getSeedValue()) {
                    if (quantity < item.getIncreamentValueLimmit()) {
                        for (int i = 0; i < item.getServiceDetailInfo().size(); i++) {
                            ServiceDetailInfoItem serviceDetailInfoItem = item.getServiceDetailInfo().get(i);
                            if (quantity == serviceDetailInfoItem.getTotal()) {
                                Ttime = serviceDetailInfoItem.getDuration();
                                Pprice = serviceDetailInfoItem.getAmount();
                                time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
                                price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));
                                for (int j = 0; j < serviceDetailsList.size(); j++) {
                                    if (serviceDetailsList.get(j).getServiceDetailId() == item.getId()) {
                                        serviceDetailsList.get(j).setQuantity(String.valueOf(quantity));
                                    }
                                }
                            }
                        }
                    } else {

                        Ttime = Ttime + timedd;
                        Pprice = Pprice + pricedd;
                        time.setText(StringUtility.numberEn2Fa(String.valueOf(Ttime)));
                        price.setText(StringUtility.numberEn2Fa(String.valueOf(Pprice)));

                        for (int i = 0; i < serviceDetailsList.size(); i++) {
                            if (serviceDetailsList.get(i).getServiceDetailId() == item.getId()) {
                                serviceDetailsList.get(i).setQuantity(String.valueOf(quantity));
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onRefresh() {
        setData(true,server);
    }

    public void getToken(boolean b){
        UserObject userObject = new Gson().fromJson(new SharedPreference().getUserObject(getActivity()), UserObject.class);
        server.getToken(userObject.getCellNumber(), userObject.getActivationCode(), new FutureCallback<Response<OperationResult<RegisterResult>>>() {
            @Override
            public void onCompleted(Exception e, Response<OperationResult<RegisterResult>> result) {
                if (result.getHeaders().code() == 200){
                    if (result.getResult().getStatus().isSuccess()){
                        SharedPreference sharedPreference = new SharedPreference();
                        sharedPreference.saveLoginToken(getActivity(), result.getResult().getResult().getTokenId());
                        Server server1 = Server.getInstance(getActivity(), sharedPreference.getLoginTokenValue(getActivity()));
                        setData(b,server1);
                        Log.i("kk","login kard");
                    }else {
                        Toast.makeText(getActivity(), result.getResult().getStatus().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                    isSendRequest = false;
                }
            }
        });
    }
}
