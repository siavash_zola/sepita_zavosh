
package hoodadtech.com.sepita.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.utils.StringUtility;

public class VisitingDatesTimes {

    @SerializedName("dates")
    @Expose
    private List<Date> dates = null;
    @SerializedName("houres")
    @Expose
    private List<String> houres = null;

    @SerializedName("vipPercent")
    @Expose
    private String vipPercent;

    @SerializedName("femalePercent")
    @Expose
    private String femalePercent;

    @SerializedName("vipText")
    @Expose
    private String vipText;

    @SerializedName("extraHourPrice")
    @Expose
    private String extraHourPrice;

    @SerializedName("maxHourAvailable")
    @Expose
    private String maxHourAvailable;

    @SerializedName("maxHourMessageText")
    @Expose
    private String maxHourMessageText;


    public String getMaxHourMessageText() {
        return maxHourMessageText;
    }

    public void setMaxHourMessageText(String maxHourMessageText) {
        this.maxHourMessageText = maxHourMessageText;
    }

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    public List<String> getHoures() {
        return houres;
    }

    public void setHoures(List<String> houres) {
        this.houres = houres;
    }

    public String getVipPercent() {
        return vipPercent;
    }

    public void setVipPercent(String vipPercent) {
        this.vipPercent = vipPercent;
    }

    public String getFemalePercent() {
        return femalePercent;
    }

    public void setFemalePercent(String femalePercent) {
        this.femalePercent = femalePercent;
    }

    public String getVipText() {
        return vipText;
    }

    public void setVipText(String vipText) {
        this.vipText = vipText;
    }

    public String getExtraHourPrice() {
        return extraHourPrice;
    }

    public void setExtraHourPrice(String extraHourPrice) {
        this.extraHourPrice = extraHourPrice;
    }

    public String getMaxHourAvailable() {
        return maxHourAvailable;
    }

    public void setMaxHourAvailable(String maxHourAvailable) {
        this.maxHourAvailable = maxHourAvailable;
    }
}
