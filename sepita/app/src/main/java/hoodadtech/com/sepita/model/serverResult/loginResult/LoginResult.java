package hoodadtech.com.sepita.model.serverResult.loginResult;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import hoodadtech.com.sepita.model.serverResult.RegisterResult;
import hoodadtech.com.sepita.model.serverResult.addAddress.Status;

public class LoginResult {

    @SerializedName("result")
    @Expose
    private RegisterResult result;
    @SerializedName("status")
    @Expose
    private Status status;

    public RegisterResult getResult() {
        return result;
    }

    public void setResult(RegisterResult result) {
        this.result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
