package hoodadtech.com.sepita.adapter.newAdapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import hoodadtech.com.sepita.R;
import hoodadtech.com.sepita.model.RateText;
import hoodadtech.com.sepita.utils.IranSansTextView;
import hoodadtech.com.sepita.utils.MyClickListener;

public class RateAdapter extends RecyclerView.Adapter{
    private List<RateText> list;
    private Activity activity;
    private final int TYPE_HEADER = 0;
    private final int TYPE_FOOTER = 1;
    private final int TYPE_ITEM = 2;
    public MyClickListener listener;

    public RateAdapter(List<RateText> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM){
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rate, parent, false);
            return new ItemViewHolder(itemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        RateText rateText = list.get(position);
        itemViewHolder.tv_title.setText(rateText.getTitle());
        if (rateText.isSelected()){
            itemViewHolder.linBg.setBackgroundResource(R.drawable.bg_button_gray);
            itemViewHolder.tv_title.setTextColor(activity.getResources().getColor(R.color.white));
        }else {
            itemViewHolder.linBg.setBackgroundResource(R.drawable.bg_button_silver);
            itemViewHolder.tv_title.setTextColor(activity.getResources().getColor(R.color.text_color));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linBg;
        IranSansTextView tv_title;
        public ItemViewHolder(View itemView) {
            super(itemView);
            linBg = itemView.findViewById(R.id.linBg);
            tv_title = itemView.findViewById(R.id.tv_title);
            linBg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.itemClicked(getAdapterPosition());
                }
            });
        }
    }
}
