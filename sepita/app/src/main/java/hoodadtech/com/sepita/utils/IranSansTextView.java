package hoodadtech.com.sepita.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by Milad on 12/15/2017.
 */

public class IranSansTextView extends android.support.v7.widget.AppCompatTextView {
    public IranSansTextView(Context context) {
        super(context);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "IRANSansMobile(FaNum)_Light.ttf");
        this.setTypeface(face);
    }

    public IranSansTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "IRANSansMobile(FaNum)_Light.ttf");
        this.setTypeface(face);
    }

    public IranSansTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face= Typeface.createFromAsset(context.getAssets(), "IRANSansMobile(FaNum)_Light.ttf");
        this.setTypeface(face);
    }

}
