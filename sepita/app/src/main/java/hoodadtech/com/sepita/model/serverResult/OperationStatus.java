package hoodadtech.com.sepita.model.serverResult;

import android.util.Log;

import hoodadtech.com.sepita.BuildConfig;
import hoodadtech.com.sepita.utils.StringUtility;


public class OperationStatus {
    public static final int NOT_FOUND=1;
    public static final int SERVER_ERROR=2;
	int statusCode;
	String message;
	boolean isSuccess;
	int accountType;
	int networkType;
	public static final String EXCEPTION_OCCURED_MSG = "خطا";

    public OperationStatus(String message, boolean success){
        this.message=message;
        this.isSuccess=success;
    }

    public OperationStatus(String message){
        statusCode = -1;
		isSuccess = false;
        if (BuildConfig.DEBUG && StringUtility.isNotBlank(message)){
            this.message = message;
            Log.d("ex", message);
        }
        else
            this.message = EXCEPTION_OCCURED_MSG;
    }
	public OperationStatus(Exception ex) {
		statusCode = -1;
		isSuccess = false;
		if (BuildConfig.DEBUG && ex!=null && StringUtility.isNotBlank(ex.getMessage())){
			message = ex.getMessage();
			Log.d("ex", message);
		}
		else
			message = EXCEPTION_OCCURED_MSG;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean success) {
		this.isSuccess = success;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public int getNetworkType() {
		return networkType;
	}

	public void setNetworkType(int networkType) {
		this.networkType = networkType;
	}
}
