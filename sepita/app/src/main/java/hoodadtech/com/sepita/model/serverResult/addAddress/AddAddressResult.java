package hoodadtech.com.sepita.model.serverResult.addAddress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddAddressResult {
    @SerializedName("result")
    @Expose
    private Object result;
    @SerializedName("status")
    @Expose
    private Status status;

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
